import 'package:flutter/material.dart';
import 'package:Laybull/API/BaseUrl.dart';
import 'package:Laybull/myCustomProgressDialog.dart';
import 'package:progress_dialog/progress_dialog.dart';

import 'API/API.dart';
import 'API/GloabalVariable.dart';
import 'constant.dart';
import 'dilogbar.dart';


class search extends StatefulWidget {
  @override
  _searchState createState() => _searchState();
}

class _searchState extends State<search> {
  TextEditingController name = new TextEditingController();
  var _formKey = GlobalKey<FormState>();
  @override
  void initState() {
    super.initState();
    searchs.clear();
  }

  MyProgressDialog pr;
  @override
  Widget build(BuildContext context) {
    pr = MyProgressDialog(context);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body:  SafeArea(
        child: Form(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.only(left:13.0,top:10,),
            child: Container(
              child: ListView(
                children: [
                  SizedBox(height: 10,),
                  InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Row(
                        children: [
                          Container(
                              height: 20,
                              width: 20,
                              child: Image.asset('assets/ARROW.png',)),
                          SizedBox(width: 10,),
                          Text('Search'.toUpperCase(),style: headingStyle,),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 10,),
                  Container(
                    height: MediaQuery.of(context).size.height*.06,
                    width: MediaQuery.of(context).size.width,
                    margin: EdgeInsets.only(left: 2,right: 20),
                    decoration: BoxDecoration(
                      color: Color(0xffF4F4F4),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 10.0),
                      child: TextField(
                        controller: name,
                        decoration: InputDecoration(
                          hintStyle: TextStyle(fontSize: 14),
                          hintText: 'Search',alignLabelWithHint: true,
                          prefixIcon: Padding(
                            padding: const EdgeInsets.only(top:5.0),
                            child: Icon(Icons.search,color: Colors.black,size: 18,),
                          ),
                          border: InputBorder.none,
                          //contentPadding: EdgeInsets.all(20),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 5,),
                  searchs.length != 0 ?
                      productGridView(product: searchs,onFavoutireButtonTapped: (){setState(() {});},) :
                  Container(
                    margin: EdgeInsets.only(top: MediaQuery.of(context).size.height*.2),
                      child: Center(child: Text('No Product Found', style: TextStyle(color: Colors.black),)),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
      bottomNavigationBar:
      Container(
        height: MediaQuery.of(context).size.height*.14,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            GestureDetector(
              onTap: ()async{
                setState(() {
                  name.clear();
                });
                await showModalBottomSheet(
                  context: context,
                  isScrollControlled: true,
                  builder: (context) => Container(
                    // padding: EdgeInsets.only(
                    //     bottom: MediaQuery.of(context).viewInsets.bottom),
                    child: BottomSheetExample(callBack: (){setState(() {});},),
                  ),
                );
                print("ListLength = ${searchs.length}");
                setState(() {

                });
              },
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 20,vertical: 10),
                decoration: BoxDecoration(
                    // color: Colors.grey[300],
                    borderRadius: BorderRadius.circular(5)
                ),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('FILTERS',style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black,fontSize: 10),),
                    SizedBox(width: 5,),
                    Container(
                      height: 10,
                      width: 10,
                      child: Column(
                        children: [
                          Image.asset('assets/filter2.png'),
                          Image.asset('assets/filter1.png'),
                          Image.asset('assets/filter2.png'),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(height: MediaQuery.of(context).size.height*.003,),
            InkWell(
              onTap: ()async{
                if(name.text.trim().isEmpty){
                  return;
                }
                if(_formKey.currentState.validate()) {
                  print(name.text);
                  pr.show();
                  try{
                  await API.Search(name.text.toString());
                  if(API.complete == 'true'){
                    pr.hide();
                    setState(() {

                    });
                    FocusScope.of(context).unfocus();
                }
                  }catch(e){
                    print(e);
                    pr.hide();
                  }
                }
              },
              child: Container(
                margin: EdgeInsets.only(left: 20,right: 20,bottom: 15),
                height: MediaQuery.of(context).size.height*.05,
                // width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  color: Colors.black,
                  borderRadius: BorderRadius.circular(6),
                ),
                child: Center(
                    child: Text('SEARCH',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 12),
                    )),
              ),
            ),
          ],
        ),
      ),
    );
  }

}
