import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';

class MyProgressDialog {
  ProgressDialog progressDialog;
  MyProgressDialog(BuildContext context) {
    progressDialog = ProgressDialog(context, isDismissible: false,type: ProgressDialogType.Normal,);
    progressDialog.style(
      borderRadius: 10.0,
      message: "LOADING...",
      messageTextStyle: TextStyle(fontSize:14,color:  Colors.black,fontWeight: FontWeight.bold,letterSpacing: 2,fontFamily: "MetropolisExtraBold",),
      backgroundColor: Colors.white,
      child: Image.asset(
        "assets/Larg-Size.gif",
        height: 125.0,
        width: 125.0,
      ),
      elevation: 10.0,
      insetAnimCurve: Curves.easeInOut,
    );
  }
  show() {
    progressDialog.show();
  }
  hide() {
    progressDialog.hide();
  }
}
