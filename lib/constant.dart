import 'dart:io';
import 'package:Laybull/API/API.dart';
import 'package:Laybull/myCustomProgressDialog.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter/material.dart';
import 'package:Laybull/API/BaseUrl.dart';
import 'package:Laybull/API/GloabalVariable.dart';
import 'package:Laybull/Addtochart.dart';
import 'package:Laybull/Model/Product.dart';
import 'package:Laybull/Model/favourite.dart';
import 'package:Laybull/main.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';

String FCM_NotificationToken = "";

const Textprimary =  TextStyle(fontSize: 14,fontWeight: FontWeight.bold,color: Colors.black,letterSpacing: 2,fontFamily: "MetropolisExtraBold");
const headingStyle =  TextStyle(
    letterSpacing: 2.5,
    fontSize: 21,
    fontFamily: "MetropolisBold",
    fontWeight: FontWeight.bold,
    color: Colors.black
);
const Textsearch =  TextStyle(fontSize: 10,color: Colors.black,letterSpacing: 1.5);
const LiteText =  TextStyle(fontSize: 12,color: Colors.grey,fontWeight: FontWeight.w300,letterSpacing: 1.5);
const Greycolor = Color(0xffF3F3F3);
const listtext1 =TextStyle(fontSize: 14,color: Colors.black,fontWeight: FontWeight.w500,letterSpacing: 0.5,fontFamily: "MetropolisBold");
const listtext2 =TextStyle(fontSize: 14,color: Colors.black,fontWeight: FontWeight.w700,letterSpacing: 0.5,fontFamily: "MetropolisBold");
const listtext3 =TextStyle(fontSize: 14,color: Colors.grey,fontWeight: FontWeight.w500,letterSpacing: 0.5,fontFamily: "MetropolisBold");

class productGridView extends StatelessWidget{
  List<Product> product;
  final VoidCallback onFavoutireButtonTapped;
  productGridView({@required this.product,this.onFavoutireButtonTapped});
  int i = 0;

  MyProgressDialog progressDialog;

  @override
  Widget build(BuildContext context) {

    return GridView.builder(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        mainAxisSpacing: 0,
        childAspectRatio: 0.6,
      ),
      itemCount: product.length,
      itemBuilder: (BuildContext ctx, index) {
        i = index;
        return
          Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Stack(
              children: [
                GestureDetector(
                  onTap: (){
                    Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => AddToCart(product: product[index],)),
                    );
                    // Navigator.push(context, MaterialPageRoute(builder: (BuildContext) => AddToCart(product: product[index],)),);
                  },
                  child: Container(
                    height: MediaQuery.of(context).size.height*.22,
                    width: MediaQuery.of(context).size.width*.45,
                    decoration: BoxDecoration(
                        color: Colors.grey[100],
                        borderRadius: BorderRadius.circular(12),
                        image: DecorationImage(
                          image:NetworkImage(
                            BaseUrl+product[index].feature_image,
                          ),
                          fit: BoxFit.cover,
                        )
                    ),
                    margin: EdgeInsets.only(right: 10,bottom: 10,left: 5),
                  ),
                ),
                GestureDetector(
                  child: Container(
                    height: 35,
                    width: 35,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        shape: BoxShape.circle,
                        boxShadow: [BoxShadow(color: Colors.black38,blurRadius: 3,),]
                    ),
                    margin: EdgeInsets.only(right: 3),
                    child: Center(
                      child: product[index].isFavourite ?
                    Image.asset(
                        'assets/finalFire.png',
                      height: 25,
                      width: 25,
                    )
                        : Image.asset(
                        'assets/Vector1.png'),
                    ),
                  ),
                  onTap: ()async{
                    progressDialog = MyProgressDialog(context);
                    progressDialog.show();
                    try {
                      product[index].isFavourite?
                      await API.unLikeProduct(product[index].id)
                    : await API.likeProduct(product[index].id);
                    }catch(e){
                      progressDialog.hide();
                    }
                    progressDialog.hide();
                    if(API.complete == 'true'&& success=="true"){
                      product[index].isFavourite = !product[index].isFavourite;
                    }else{
                      progressDialog.hide();
                    }
                    onFavoutireButtonTapped();
                    // if(product[index].isFavourite){
                    //   print("Favourite Before");
                    //   print(wishlistshow.length);
                    //   product[index].isFavourite = false;
                    //   // wishlist.removeWhere((item) => item.id == product[index].productId.toString());
                    //   // final String encodedData = Favourite.encodeMusics(wishlist);
                    //   // MyApp.sharedPreferences.setString("wishlist", encodedData);
                    //   wishlistshow.removeWhere((item) => item.id == product[index].id);
                    //   final String encodedData1 =
                    //   Product.encodeMusics(wishlistshow);
                    //   MyApp.sharedPreferences.setString("wishlistshow", encodedData1);
                    // }
                    // else {
                    //   print(product[index].condition);
                    //   // wishlist.add(Favourite(id: product[index].productId.toString(),));
                    //   // final String encodedData = Favourite.encodeMusics(wishlist);
                    //   // MyApp.sharedPreferences.setString("wishlist", encodedData);
                    //   Product prod = Product(
                    //       color: product[index].color,
                    //       brand_id: product[index].brand_id,
                    //       discount: product[index].discount,
                    //       id: product[index].id,
                    //       name: product[index].name,
                    //       price: product[index].price,
                    //       feature_image: product[index].feature_image,
                    //       popular: product[index].popular,
                    //       images: product[index].images,
                    //       isFavourite: true,
                    //       type: product[index].type,
                    //     fulldesc: product[index].fulldesc,
                    //     category_id: product[index].category_id,
                    //     size: product[index].size,
                    //     original_price: product[index].original_price,
                    //     payment: product[index].payment,
                    //     pusblish: product[index].pusblish,
                    //     short_desc: product[index].short_desc,
                    //     status: product[index].status,
                    //     tags: product[index].tags,
                    //     vendor_id: product[index].vendor_id,
                    //     warrenty: product[index].warrenty,
                    //     condition: product[index].condition,
                    //     vendorName: product[index].vendorName,
                    //   );
                    //   wishlistshow.add(prod);
                    //   final String encodedData1 = Product.encodeMusics(wishlistshow);
                    //   MyApp.sharedPreferences.setString("wishlistshow", encodedData1);
                    //   product[index].isFavourite = true;
                    // }
                  },
                ),
              ],
              alignment: Alignment.bottomRight,
            ),
            Container(
              padding: EdgeInsets.only(top: 1.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment:
                CrossAxisAlignment.start,
                children: [
                  Container(
                    child: Text(
                      product[index].name.toUpperCase(),
                      style: listtext1,
                      // maxLines: 1,
                    ),
                  ),
                  Text(product[index].price+' AED'.toUpperCase(),
                      style: listtext2),
                  SizedBox(height: 1,),
                  Text(product[index].condition.toUpperCase(),
                      style: listtext3),
                ],
              ),
            )
          ],
        );
      },
    );
  }
}

Future<File>imageCompression(File imageToCompress)async{
  print('Size before compress ${imageToCompress.lengthSync()}');
  final dir = await getTemporaryDirectory();
  final targetPath = dir.path +
      "/${DateTime.now().millisecondsSinceEpoch.toString()}.jpg";
  final File result = await FlutterImageCompress.compressAndGetFile(
    imageToCompress.path,
    targetPath,
    quality: 70,
  );
  print('Size after compress ${result.lengthSync()}');
  return result;
}