import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:Laybull/API/API.dart';
import 'package:Laybull/ProductPages/BrandList.dart';
import 'package:Laybull/ProductPages/ColorList.dart';
import 'package:Laybull/API/BaseUrl.dart';
import 'package:Laybull/ProductPages/categories.dart';
import 'package:Laybull/ProductPages/sizelist.dart';
import 'package:Laybull/Model/CategoryModel.dart';
import 'package:Laybull/myCustomProgressDialog.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'API/GloabalVariable.dart';
import 'constant.dart';


class BottomSheetExample extends StatefulWidget {
  final VoidCallback callBack;
  BottomSheetExample({this.callBack});
  @override
  _BottomSheetExampleState createState() => _BottomSheetExampleState();
}

class _BottomSheetExampleState extends State<BottomSheetExample> {

  Category selectedCategory;
  Brand selectedBrand;
  ColorClass selectedColor;
  SlideSizeClass selectedSize;
  TextEditingController minValue = TextEditingController();
  TextEditingController maxValue = TextEditingController();

  @override
  void initState() {
    colorList.forEach((color) {
      color.isSelected = false;
    });
    brands.forEach((brand) {
      brand.isSelected = false;
    });
      slideSizesOfSneakers.forEach((size) {
          size.isSelected = false;
      });
    slideSizesExceptSneakers.forEach((size) {
      size.isSelected = false;
    });
    super.initState();
  }

  MyProgressDialog pr;
  @override
  Widget build(BuildContext context) {
    pr = MyProgressDialog(context);
    return  Container(
      height: MediaQuery.of(context).size.height/1.1,
      color: Color(0xff757575),
      child: Container(
        padding: EdgeInsets.all(20.0),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(30.0),
            topRight: Radius.circular(30.0),
          ),
        ),
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            SizedBox(height: 5,),
            Container(
              child: Divider(
                height: 1,
                thickness: 5,
                indent: 100,
                endIndent: 100,
              ),
            ),
            SizedBox(height: 15,),
            Text('SET FILTERS',style:Textprimary,textAlign: TextAlign.center,),
            SizedBox(height: 15,),
            Text('BRAND',style:Textprimary,),
            SizedBox(height: 15,),
            Container(
              height: 59,
              width: MediaQuery.of(context).size.width / 1.13,
              child: new ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: brands.length,
                  itemBuilder: (_, index) {
                    brands[index].isSelected = brands[index].isSelected??false;
                    return Padding(
                        padding: const EdgeInsets.only(
                            left: 5.0, top: 5, bottom: 6),
                        child: GestureDetector(
                          onTap: (){
                            for(int i=0;i<brands.length;i++){
                              if(i!=index){
                                brands[i].isSelected = false;
                              }
                            }
                            setState(() {
                              brands[index].isSelected = !brands[index].isSelected;
                              selectedBrand = brands[index];
                            });
                          },
                          child: Container(
                            height: 54,
                            padding: EdgeInsets.symmetric(horizontal: 10),
                            // width: MediaQuery.of(context).size.width / 2.8,
                            decoration: BoxDecoration(
                              border: Border.all(
                                  color:  brands[index].isSelected? Colors.black :Color(0xffD5D5D5),
                              ),
                              borderRadius: BorderRadius.circular(6),
                            ),
                            child: Center(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Container(
                                    child: Image.asset(brands[index].image,fit: BoxFit.cover,),
                                    margin: EdgeInsets.only(top: 5,left: 5,bottom: 5),
                                  ),
                                  SizedBox(width: 5,),
                                  // Image.asset(brands[index].image,),
                                  Text(
                                    brands[index].name.toUpperCase(),
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontWeight: FontWeight.w800,
                                      letterSpacing: 1.5,
                                      fontSize: 11,
                                      color: Colors.black
                                    ),
                                    maxLines: 1,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ));
                  }),
            ),
            SizedBox(height: 15,),
            Text('COLOR',style:Textprimary,),
            SizedBox(height: 15,),
            Container(
              height: 59,
              width: MediaQuery.of(context).size.width / 1.13,
              child: new ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount:  colorList.length,
                  itemBuilder: (_, index) {
                    return Container(
                      margin: EdgeInsets.only(right: 5),
                      child: GestureDetector(
                        onTap: (){
                          for(int i=0;i<colorList.length;i++){
                            if(i!=index){
                              colorList[i].isSelected = false;
                            }
                          }
                          setState(() {
                            colorList[index].isSelected = !colorList[index].isSelected;
                            selectedColor = colorList[index];
                          });
                        },
                        child: Stack(
                          alignment: Alignment.center,
                          children: [
                            Container(

                              decoration: BoxDecoration(
                                  border: Border.all(color: Colors.black45,width: 0.5,),shape: BoxShape.circle,
                                color: Color(colorList[index].colorValue),
                              ),
                              height: 40,
                              width: 40,
                            ),
                            colorList[index].isSelected?
                            Icon(Icons.done,color: index==0?Colors.white:Colors.black,size: 30,)
                                :SizedBox(width: 5,)
                          ],
                        ),
                      ),
                    );
                  }),
            ),
            SizedBox(height: 15,),
            Text('CATEGORY',style:Textprimary,),
            SizedBox(height: 15,),
            Container(
              padding: EdgeInsets.only(left: 10, right: 10),
              decoration: BoxDecoration(
                border: Border.all(color:Colors.grey[400]),
                borderRadius: BorderRadius.circular(5.0),),
              child: DropdownButton(
                isExpanded: true,
                hint: Text("Select Category"),
                dropdownColor: Colors.grey[100],
                icon: Icon(
                  Icons.keyboard_arrow_down_outlined,
                  size: 20,
                  color: Colors.black,
                ),
                iconSize: 12,
                underline: SizedBox(),
                style: TextStyle(color: Colors.black, fontSize: 15),
                value: selectedCategory,
                onChanged: (newValue) {
                  setState(() {
                    print("valueee= $selectedCategory");
                    selectedCategory = newValue;
                    selectedSize = null;
                  });
                },
                items: categoryList.map((valueItem) {
                  return DropdownMenuItem(
                    value: valueItem,
                    child: new Row(
                      //  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Text(valueItem.categoryName),
                      ],
                    ),
                  );
                }).toList(),
              ),
            ),
            SizedBox(height: 15,),
            Text(
              "SIZES (US)",
              style: Textprimary,
            ),
            SizedBox(
              height: 5,
            ),
            selectedCategory!=null&&selectedCategory.categoryName.toUpperCase()=="SNEAKERS"?
            Container(
              //color: Colors.grey,
              height: 50,
              width: MediaQuery.of(context).size.width / 1.13,
              child: new ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: slideSizesOfSneakers.length,
                  itemBuilder: (_, index) {
                    return Padding(
                        padding: const EdgeInsets.only(
                            top: 5, bottom: 6,right: 5),
                        child: GestureDetector(
                          onTap: (){
                            for(int i=0;i<slideSizesOfSneakers.length;i++){
                              if(i!=index){
                                slideSizesOfSneakers[i].isSelected = false;
                              }
                            }
                            setState(() {
                              slideSizesOfSneakers[index].isSelected = !slideSizesOfSneakers[index].isSelected;
                            });
                            selectedSize = slideSizesOfSneakers[index];
                          },
                          child: Container(
                            height: 54,
                            width: MediaQuery.of(context).size.width / 7.5,
                            decoration: BoxDecoration(
                              border: Border.all(color: slideSizesOfSneakers[index].isSelected?Colors.black:Color(0xffD5D5D5)),
                              borderRadius: BorderRadius.circular(6),
                            ),
                            child: Center(
                              child: Text(
                                slideSizesOfSneakers[index].size,
                                style:
                                TextStyle(fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        ));
                  }),
            )
                : Container(
              //color: Colors.grey,
              height: 50,
              width: MediaQuery.of(context).size.width / 1.13,
              child: new ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: slideSizesExceptSneakers.length,
                  itemBuilder: (_, index) {
                    return Padding(
                        padding: const EdgeInsets.only(
                            top: 5, bottom: 6,right: 5),
                        child: GestureDetector(
                          onTap: (){
                            for(int i=0;i<slideSizesExceptSneakers.length;i++){
                              if(i!=index){
                                slideSizesExceptSneakers[i].isSelected = false;
                              }
                            }
                            setState(() {
                              slideSizesExceptSneakers[index].isSelected = !slideSizesExceptSneakers[index].isSelected;
                            });
                            selectedSize = slideSizesExceptSneakers[index];
                          },
                          child: Container(
                            height: 54,
                            width: MediaQuery.of(context).size.width / 7.5,
                            decoration: BoxDecoration(
                              border: Border.all(color: slideSizesExceptSneakers[index].isSelected?Colors.black:Color(0xffD5D5D5)),
                              borderRadius: BorderRadius.circular(6),
                            ),
                            child: Center(
                              child: Text(
                                slideSizesExceptSneakers[index].size,
                                style:
                                TextStyle(fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        ));
                  }),
            ),
            SizedBox(height: 15,),
            Text('PRICE RANGE',style:Textprimary,),
            SizedBox(height: 10,),
            Container(
              child: Row(children: [
                Container(
                  height: 45,
                  width: MediaQuery.of(context).size.width /3,
                  child: TextFormField(
                    controller: minValue,
                    inputFormatters: [
                      FilteringTextInputFormatter.digitsOnly,
                    ],
                      style: TextStyle(
                          fontSize: 15,
                          fontFamily: 'Metropolis',
                          letterSpacing: 2.2),
                      decoration: InputDecoration(
                          hintText:'min:00',
                          border: InputBorder.none,
                          fillColor: Color(0xfff3f3f4),
                          filled: true)
                  ),
                ),
                Spacer(),
                Container(
                  height: 45,
                  width: MediaQuery.of(context).size.width / 3,
                  child: TextFormField(
                    controller: maxValue,
                    inputFormatters: [
                      FilteringTextInputFormatter.digitsOnly,
                    ],
                      style: TextStyle(
                          fontSize: 15,
                          fontFamily: 'Metropolis',
                          letterSpacing: 2.2),
                      decoration: InputDecoration(
                          hintText:'max:00',
                          border: InputBorder.none,
                          fillColor: Color(0xfff3f3f4),
                          filled: true)),
                ),
              ],),
            ),
            SizedBox(height: MediaQuery.of(context).size.height*.15,),
            GestureDetector(
              onTap: ()async{
                  pr.show();
                  String s_Brand = selectedBrand!=null?selectedBrand.id.toString():"";
                  String s_Color = selectedColor!=null?selectedColor.colorName:"";
                  String s_Category = selectedCategory!=null?selectedCategory.id.toString():"";
                  String s_Size = selectedSize!=null?selectedSize.size.toString():"";
                  try{
                    await API.SearchWithFilters(s_Brand, s_Color, s_Category, s_Size, minValue.text, maxValue.text);
                    if(API.complete == 'true'){
                      pr.hide();
                        widget.callBack();
                      FocusScope.of(context).unfocus();
                      Navigator.pop(context);
                    }
                  }catch(e){
                    print(e);
                    pr.hide();
                  }
              },
              child: Container(
                height: MediaQuery.of(context).size.height*.058,
                // width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  color: Colors.black,
                  borderRadius: BorderRadius.circular(5),
                ),
                child: Center(
                    child: Text('APPLY FILTERS',style: TextStyle(color: Colors.white,fontWeight: FontWeight.w800,letterSpacing: 2,fontSize: 12),
                    )),
              ),
              // child: Container(
              //   alignment: Alignment.bottomCenter,
              //   height: MediaQuery.of(context).size.height/13,
              //   width: MediaQuery.of(context).size.width/1.1,
              //   decoration: BoxDecoration(
              //     color: Colors.black,
              //     borderRadius: BorderRadius.circular(6),
              //   ),
              //   child: Center(
              //       child: Text('SEARCH',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 12),
              //       )),
              // ),
            )
          ],
        ),
      ),
    );
  }
}




