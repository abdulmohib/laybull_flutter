import 'package:flutter/material.dart';
import 'package:Laybull/API/BaseUrl.dart';

import 'API/API.dart';
import 'API/GloabalVariable.dart';
import 'Addtochart.dart';
import 'Model/Product.dart';
import 'Model/favourite.dart';
import 'constant.dart';
import 'lists.dart';
import 'main.dart';

class Shoe extends StatefulWidget {
  int id;
  String name;
  List<Product>products;
  Shoe({this.id,this.name,this.products});
  @override
  _ShoeState createState() => _ShoeState();
}

class _ShoeState extends State<Shoe> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // API.CategoryProduct(widget.id);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
          child: Container(
            padding: EdgeInsets.only(top: 18,left: 18,right: 18),
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: ListView(
              children: [
                Row(
                  children: [
                    InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                    child: Container(
                      padding: EdgeInsets.only(top: 15,left: 5,bottom: 20,right: 15),
                      child: Image.asset(
                        'assets/ARROW.png',
                      )),
                ),
                        SizedBox(
                          width: 10,
                        ),
                    Container(
                      child: Text(
                          widget.name.toUpperCase(),
                        style: headingStyle,
                      ),
                    ),
                    // Spacer(),
                    // Image.asset('assets/Vector.png'),
                  ],
                ),
                // Row(
                //   children: [
                //     InkWell(
                //       onTap: () {
                //         Navigator.pop(context);
                //       },
                //       child: Container(
                //           padding: EdgeInsets.all(15),
                //           child: Image.asset(
                //             'assets/ARROW.png',
                //           )),
                //     ),
                //     SizedBox(
                //       width: 10,
                //     ),
                //     Text(
                //       widget.name,
                //       style: Textprimary,
                //     ),
                //   ],
                // ),
                SizedBox(height: 5,),
                Container(
                  child: widget.products.length==0?
                  Container(
                    height: MediaQuery.of(context).size.height*.6,
                    child: Center(
                      child: Text(
                          "No Data To Show",
                    ),
                    ),
                  ):
                  productGridView(product: widget.products,onFavoutireButtonTapped: (){setState(() {});},),
                )
              ],
            ),
          ),
        ),
    );
  }
}