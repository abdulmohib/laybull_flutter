import 'dart:convert';
import 'package:Laybull/BottomNavigation/BottomNav.dart';
import 'package:Laybull/Model/notificationModel.dart';
import 'package:Laybull/Model/routes.dart';
import 'package:Laybull/Model/userModel.dart';
import 'package:Laybull/customBadge/flutter_badge.dart';
import 'package:Laybull/notificationScreen.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:Laybull/API/BaseUrl.dart';
import 'package:Laybull/Model/Product.dart';
import 'package:Laybull/Model/bidModel.dart';
import 'package:Laybull/Offers/Offer.dart';
import 'package:Laybull/Registration/Login.dart';
import 'package:Laybull/main.dart';
import 'package:Laybull/myCustomProgressDialog.dart';
import 'package:Laybull/selling.dart';
import 'package:Laybull/sold.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';
import 'API/API.dart';
import 'API/GloabalVariable.dart';
import 'Addtochart.dart';
import 'PopularProduct.dart';
import 'constant.dart';
import 'lists.dart';

class Account extends StatefulWidget {
  final int userId;
  final bool isFromBottomNav;
  Account({this.userId,this.isFromBottomNav});
  @override
  _AccountState createState() => _AccountState();
}
class UserFollower{
  int userId;
  String profilePic;
  String userName;
  UserFollower({this.userId,this.userName,this.profilePic});

  factory UserFollower.fromJson(Map<String,dynamic>json){
    return UserFollower(
      userId: json["follow"]["id"],
      userName: json["follow"]["name"],
      profilePic: json["follow"]["detail"]!=null?json["follow"]["detail"]["image"]:null,
    );
  }
}

class _AccountState extends State<Account> {
  bool isProfileFetched = false;
  bool isLoading = true;
  Map jsonData = Map();
  bool isFollowed = false;
  int noOfFollowers = 0;
  int noOfFollowings = 0;
  List<Product>listOfSelling = [];
  List<Product>listOfSold = [];
  List<Product>listOfPurchased = [];
  fetchUserProfile()async{
      var url = BaseUrl + "api/user_profile";
      Dio dio = new Dio();
      dio.options.headers["Authorization"] = "Bearer " +
          MyApp.sharedPreferences.getString("access_token").toString();
      dio.options.queryParameters= {"user_id":widget.userId.toString()};
      // try{
        await dio
            .get(url,queryParameters: {"user_id":widget.userId.toString()} )
            .then((response) {
              // print("Response Data = ${response.data}");
          if (response.statusCode == 200) {
            var jj = response.data;
            jsonData = jj["data"];
            print("data = ${jsonData["unread_notification"]}");
              listOfSelling = List<Product>.from(jsonData["list_of_products"].map((product) => Product.fromJson(product)).toList());
              noOfFollowers = jsonData["no_of_follower"];
              noOfFollowings = jsonData["no_of_following"];
              int ratingCount = jsonData["user"]["no_of_rating"]??0;
              double ratingSum =  jsonData["user"]["sum_of_rating"].toDouble();

              if(ratingCount!=0){
                userRating = ratingSum/ratingCount.toDouble();
              }
              if(myUserId==widget.userId){
                for(int i =0;i<jsonData["list_of_sold_product"].length;i++){
                  if(jsonData["list_of_sold_product"][i]["product"]!=null)
                  listOfSold.add(
                    Product(
                      id: jsonData["list_of_sold_product"][i]["product"]["id"],
                      condition: jsonData["list_of_sold_product"][i]["product"]["condition"],
                      price: jsonData["list_of_sold_product"][i]["product"]["price"],
                      feature_image: "uploads/productImages/"+jsonData["list_of_sold_product"][i]["product"]["feature_image"],
                      size: jsonData["list_of_sold_product"][i]["product"]["size"],
                      name: jsonData["list_of_sold_product"][i]["product"]["name"],
                      images: jsonData["list_of_sold_product"][i]["product"]["images"],
                      updated_at: jsonData["list_of_sold_product"][i]["created_at"],
                    ),
                  );
                }
                for(int i =0;i<jsonData["list_of_purchase_product"].length;i++){
                  if(jsonData["list_of_purchase_product"][i]["product"]!=null)
                  listOfPurchased.add(
                    Product(
                      id: jsonData["list_of_purchase_product"][i]["product"]["id"],
                      condition: jsonData["list_of_purchase_product"][i]["product"]["condition"],
                      price: jsonData["list_of_purchase_product"][i]["product"]["price"],
                      feature_image: "uploads/productImages/"+jsonData["list_of_purchase_product"][i]["product"]["feature_image"],
                      size: jsonData["list_of_purchase_product"][i]["product"]["size"],
                      name: jsonData["list_of_purchase_product"][i]["product"]["name"],
                      images: jsonData["list_of_purchase_product"][i]["product"]["images"],
                      updated_at: jsonData["list_of_purchase_product"][i]["created_at"],
                    ),
                  );
                }
              }
              else{
                isFollowed = jsonData["is_Followed"]==1?true:false;
              }
            setState(() {
              isProfileFetched = true;
              isLoading = false;
            });
          } else {
            setState(() {
              isLoading = false;
            });
            success = "true";
            print("Exception in Api = $url");
            Fluttertoast.showToast(
                msg: "Failed to get data Please Try Again!",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.CENTER,
                timeInSecForIosWeb: 1,
                backgroundColor: Colors.black38,
                textColor: Colors.white,
                fontSize: 16.0
            );
          }
        });
      // }catch(e){
      //   print("Exception = $e");
      //   setState(() {
      //     isLoading = false;
      //   });
      //   Fluttertoast.showToast(
      //       msg: "Failed to connect with Server Please Try Again!",
      //       toastLength: Toast.LENGTH_SHORT,
      //       gravity: ToastGravity.CENTER,
      //       timeInSecForIosWeb: 1,
      //       backgroundColor: Colors.black38,
      //       textColor: Colors.white,
      //       fontSize: 16.0
      //   );
      // }
    }
  fetchUserFollowerList()async{
    List<UserFollower>listFollowers = [];
    pr = MyProgressDialog(context);
    pr.show();
      var url = BaseUrl + "api/list-follower";
      Dio dio = new Dio();
      dio.options.headers["Authorization"] = "Bearer " +
          MyApp.sharedPreferences.getString("access_token").toString();
      // dio.options.queryParameters= {"user_id":widget.userId.toString()};
      try{
        await dio
            .get(url, )
            .then((response) {
              // print("Response Data = ${response.data}");
          if (response.statusCode == 200) {
            listFollowers = List<UserFollower>.from(response.data["user_data"].map((follower) => UserFollower.fromJson(follower)).toList());
            pr.hide();
            showListFollowers(listFollowers);
          } else {
            pr.hide();
            print("Exception in Api = $url");
            Fluttertoast.showToast(
                msg: "Failed to get data Please Try Again!",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.CENTER,
                timeInSecForIosWeb: 1,
                backgroundColor: Colors.black38,
                textColor: Colors.white,
                fontSize: 16.0
            );
          }
        });
      }catch(e){
        pr.hide();
        print("Exception = $e");
        Fluttertoast.showToast(
            msg: "Failed to connect with Server Please Try Again!",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.black38,
            textColor: Colors.white,
            fontSize: 16.0
        );
      }
    pr.hide();
    }

    showListFollowers(List<UserFollower> list){
      showModalBottomSheet(
          backgroundColor: Colors.transparent,
          context: context,
          builder: (ctx) {
            return ClipRRect(
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(20),
                topLeft: Radius.circular(20),
              ),
              child: Container(
                  color: Colors.white,
                  height: MediaQuery.of(context).size.height * .6,
                  padding: EdgeInsets.symmetric(vertical: 10,horizontal: 20),
                  child: Column(
                    children: [
                      Container(
                        color: Colors.white,
                        height: 55,
                        child: Center(
                          child: Row(
                            // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                "FOLLOWINGS",
                                style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold,color: Colors.black),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 5),
                        child: Divider(height: 2,),
                      ),
                      Expanded(
                        child: list.length != 0
                            ? ListView.builder(
                            itemCount:
                           list.length,
                            itemBuilder: (context, index) {
                              return Container(
                                margin: EdgeInsets.only(top: 10, left: 10),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment:
                                  CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      width: 40,
                                      height: 40,
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        image: DecorationImage(
                                            image: list[index].profilePic != null
                                                ? NetworkImage(
                                              list[index].profilePic,
                                              scale: 1.0,
                                            )
                                                :AssetImage('assets/page2.png'),
                                            fit: BoxFit.cover),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Text(
                                    "${list[index].userName}",
                                      // style: LiteText,
                                    ),
                                  ],
                                ),
                              );
                            })
                            : Center(
                          child: Text(
                            "No Following",
                            style: TextStyle(
                              fontSize: 20,
                            ),
                          ),
                        ),
                      ),
                    ],
                  )
              ),
            );
          });
    }

  int myUserId;
  int prodId;
  Product prod;

  @override
  void initState() {
    myUserId = int.parse(MyApp.sharedPreferences.getString("userId"));
    fetchUserProfile();
    super.initState();
  }

  double _userRatingResult = 0.0;
  _showRatingDialogue()async{
    double rating = 3.5;
    await showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return StatefulBuilder(
            builder: (BuildContext context, StateSetter setState) =>
                ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: AlertDialog(
                title: Text('RATE USER',style: TextStyle(fontWeight: FontWeight.bold,letterSpacing: 2,fontFamily: "MetropolisExtraBold"),),
                content: Container(
                  // height: 23,
                  // width: MediaQuery.of(context).size.width,
                  child: RatingBar.builder(
                    initialRating: rating,
                    minRating: 0.5,
                    glow: false,
                    direction: Axis.horizontal,
                    allowHalfRating: true,
                    updateOnDrag: true,
                    itemCount: 5,
                    maxRating: 5,
                    itemSize: 50,
                    itemBuilder: (context, _) => Icon(
                      Icons.star,
                      color: Colors.amber,
                    ),
                    onRatingUpdate: (value) {
                      print(value);
                      setState(() {
                        rating = value;
                      });
                    },
                  ),
                ),
                actions: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      TextButton(
                        child: Text('CANCEL'),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                      TextButton(
                        child: Text('RATE'),
                        onPressed: () async{
                          Navigator.pop(context);
                          pr.show();
                          try{
                            await API.rateUser(widget.userId,rating);
                           print("User Rating = ${API.rating}");
                            pr.hide();
                            if(API.complete == 'true'){
                              if(success=='error'){
                                Fluttertoast.showToast(
                                    msg: "Something went wrong",
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.CENTER,
                                    timeInSecForIosWeb: 1,
                                    backgroundColor: Colors.black38,
                                    textColor: Colors.white,
                                    fontSize: 16.0
                                );
                              }
                              else{
                                _userRatingResult = API.rating;
                                Fluttertoast.showToast(
                                    msg: "Rate Successfully",
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.CENTER,
                                    timeInSecForIosWeb: 1,
                                    backgroundColor: Colors.black38,
                                    textColor: Colors.white,
                                    fontSize: 16.0
                                );

                              }
                            }
                          }catch(e){
                            print("Exception: $e");
                            pr.hide();
                            Fluttertoast.showToast(
                                msg: "Something went wrog",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIosWeb: 1,
                                backgroundColor: Colors.black38,
                                textColor: Colors.white,
                                fontSize: 16.0
                            );
                          }
                        },
                      ),
                    ],
                  ),
                ],
              ),
            ),
        );
      },
    );
  }

  MyProgressDialog pr;
  int firstTime = 0;
  double userRating = 0.0;

  NotificationProvider _notificationProvider;

  @override
  Widget build(BuildContext context) {
    _notificationProvider = Provider.of<NotificationProvider>(context,);
    return isLoading?
        Container(color:Colors.white,height: MediaQuery.of(context).size.height,child: Center(child:Image.asset(
          "assets/Larg-Size.gif",
          height: 125.0,
          width: 125.0,
        ),),):
        !isProfileFetched?
        Container(height: MediaQuery.of(context).size.height,child: Center(child: Text("Failed to get Data"),),):
    Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            width: MediaQuery.of(context).size.width,
            child: Stack(
              children: [
                Container(
                  child: Image.asset(
                    'assets/profileBack.jpg',
                    height: 200,
                    width: MediaQuery.of(context).size.width,
                    fit: BoxFit.cover,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 160.0),
                  child: Container(
                    // height: MediaQuery.of(context).size.height*1.5,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30.0),
                        topRight: Radius.circular(30.0),
                      ),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.white.withOpacity(0.5),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: Offset(0, 3), // changes position of shadow
                        ),
                      ],
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(18.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Column(

                                children: [
                                  SizedBox(height: 2,),
                                  GestureDetector(
                                    onTap: widget.userId==myUserId?null:()async{
                                      pr = MyProgressDialog(context);
                                      pr.show();
                                      try {
                                        isFollowed?
                                        await API.unFollowUser(widget.userId)
                                            : await API.followUser(widget.userId);
                                      }catch(e){
                                        pr.hide();
                                      }
                                      pr.hide();
                                      if(API.complete == 'true'&& success=="true"){
                                        setState(() {
                                          isFollowed?noOfFollowers--:noOfFollowers++;
                                          isFollowed = !isFollowed;
                                        });
                                      }
                                      pr.hide();
                                    },
                                    child: Container(
                                      height: 28,
                                      width: 103,
                                      decoration: BoxDecoration(
                                        color: widget.userId==myUserId?Colors.white:Colors.black,
                                        borderRadius: BorderRadius.circular(3),
                                      ),
                                      child: Center(
                                          child: Text(
                                            widget.userId==myUserId?'':isFollowed?'Following':"Follow",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 12),
                                          )),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(width: 15,),
                              Padding(
                                padding: const EdgeInsets.only(top:5.0),
                                child: Container(
                                  height: MediaQuery.of(context).size.height / 5.3,
                                  width: MediaQuery.of(context).size.width / 1.8,
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    // mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text('${jsonData["user"]["name"]}',style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold,color: Colors.black),),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      GestureDetector(
                                        onTap:myUserId!=widget.userId? ()async{
                                          pr = MyProgressDialog(context);
                                          await _showRatingDialogue();
                                          if(_userRatingResult!=0.0)
                                            setState((){
                                              userRating = _userRatingResult;
                                            });
                                        }:null,
                                        child: Container(
                                          height: 23,
                                          child: RatingBar.builder(
                                            onRatingUpdate: (rate){
                                              return;
                                            },
                                            initialRating: userRating,
                                            glow: false,
                                            tapOnlyMode: true,
                                            minRating: 0.5,
                                            direction: Axis.vertical,
                                            allowHalfRating: true,
                                            itemCount: 5,
                                            maxRating: 5,
                                            itemBuilder: (context, _) => Icon(
                                              Icons.star,
                                              color: Colors.amber,
                                            ),
                                            ignoreGestures: true,
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          Column(
                                            children: [
                                              Text("${jsonData["list_of_products"].length}"),
                                              Text('Sales',style: LiteText,),
                                            ],
                                          ),
                                          // SizedBox(
                                          //   width: 15,
                                          // ),
                                          Container(
                                            margin: EdgeInsets.symmetric(vertical: 1,horizontal: 2),
                                            child: Column(
                                              children: [
                                                Text('$noOfFollowers'),
                                                Text('Follower',style: LiteText,),
                                              ],
                                            ),
                                          ),
                                          // SizedBox(
                                          //   width: 20,
                                          // ),
                                          GestureDetector(
                                            onTap: myUserId!=widget.userId?null:(){
                                              print("Tapped");
                                              fetchUserFollowerList();
                                            },
                                            child: Container(
                                              margin: EdgeInsets.symmetric(vertical: 1,horizontal: 2),
                                              child: Column(
                                                children: [
                                                  Text('$noOfFollowings'),
                                                  Text('Following',style: LiteText),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 3,
                          ),
                          Container(
                            child: Divider(
                              color: Color(0xffF4F4F4),
                              height: 5,
                              thickness: 1,
                            ),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                              // height: MediaQuery.of(context).size.height*1.3,
                              width: MediaQuery.of(context).size.width,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  MyApp.sharedPreferences.getString("isSeller")=="0"&&widget.userId==myUserId?
                                  InkWell(
                                    onTap:()async{
                                      String country = MyApp.sharedPreferences.getString("country");
                                      if(country.trim().toUpperCase()!="United Arab Emirates".toUpperCase()){
                                        Fluttertoast.showToast(
                                            msg: "Can't become seller from $country",
                                            toastLength: Toast.LENGTH_SHORT,
                                            gravity: ToastGravity.CENTER,
                                            timeInSecForIosWeb: 1,
                                            backgroundColor: Colors.black38,
                                            textColor: Colors.white,
                                            fontSize: 16.0
                                        );
                                        return;
                                      }
                                      Navigator.push(context, MaterialPageRoute(builder: (_)=>BecomeSeller(userId: myUserId,)));
                                    },
                                    child: Container(
                                      width:  MediaQuery.of(context).size.width/1.07,
                                      height: 45,
                                      margin: EdgeInsets.only(bottom: 15),
                                      alignment: Alignment.center,
                                      decoration: BoxDecoration(
                                        color: Colors.black,
                                        borderRadius: BorderRadius.circular(5.0),
                                      ),
                                      child: Text(
                                        'BECOME SELLER',
                                        style:TextStyle(fontSize: 14,fontWeight: FontWeight.bold,color: Colors.white,letterSpacing: 2,fontFamily: "MetropolisExtraBold"),
                                      ),
                                    ),
                                  ):
                                  Column(
                                    children: [
                                      Row(
                                        children: [
                                          Row(
                                            children: [
                                              Text('SELLING',style: Textprimary ,),
                                              SizedBox(
                                                width: 5,
                                              ),
                                              Text('${jsonData["list_of_products"].length}',style: LiteText,),
                                            ],
                                          ),
                                          Spacer(),
                                          jsonData["list_of_products"].length==0?
                                          SizedBox():
                                          InkWell(
                                              onTap: ()
                                              {
                                                Navigator.push(
                                                  context,
                                                  MaterialPageRoute(builder: (context) => Selling(sellingProducts: listOfSelling,)),
                                                );
                                              },
                                              child: Padding(
                                                padding: const EdgeInsets.all(12.0),
                                                child: Text('View All  -->',style: TextStyle(fontSize: 13,color: Colors.blue),),
                                              )),
                                        ],
                                      ),
                                      SizedBox(height: 15,),
                                      listOfSelling.length==0?
                                      Container(
                                        height: 170,
                                        decoration: BoxDecoration(
                                            color: Color(0xffF4F4F4),
                                            borderRadius: BorderRadius.circular(10)
                                        ),
                                        child: Center(child: Text("No Products"),),
                                      ):
                                      productGridView(product: listOfSelling.length>2?[listOfSelling[0],listOfSelling[1]]:listOfSelling,onFavoutireButtonTapped: (){setState(() {});}),
                                      Divider(
                                        color: Color(0xffF4F4F4),
                                        height: 5,
                                        thickness: 1,
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 4,
                                  ),
                                  widget.userId==myUserId?
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Row(
                                          children: [
                                            Row(
                                              children: [
                                                Text('OFFERS',style: Textprimary ,),
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                Text(receivedOrders.length.toString(),style: LiteText,),
                                              ],
                                            ),
                                            Spacer(),
                                            receivedOrders.length>0?
                                            InkWell(
                                                onTap: (){
                                                  Navigator.push(
                                                    context,
                                                    MaterialPageRoute(builder: (context) => Offer()),
                                                  );
                                                },
                                                child: Padding(
                                                  padding: const EdgeInsets.all(12.0),
                                                  child: Text('View All  -->',style: TextStyle(fontSize: 13,color: Colors.blue),),
                                                ))
                                                :SizedBox(),
                                          ],
                                        ),
                                      ),
                                      SizedBox(height: 10,),
                                      Container(
                                        height: 170,
                                        decoration: BoxDecoration(
                                            color: Color(0xffF4F4F4),
                                            borderRadius: BorderRadius.circular(10)
                                        ),
                                        child: receivedOrders.length>0?Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                          children: [
                                            Container(
                                              width: 120,
                                              margin: EdgeInsets.symmetric(vertical: 20),
                                              child: ClipRRect(
                                                borderRadius: BorderRadius.circular(10),
                                                child:receivedOrders.length!=0 ?Image.network(BaseUrl+receivedOrders.first.product.feature_image
                                                    , fit: BoxFit.cover,):Image.asset('assets/bigshoe.png'),
                                              ),
                                            ),
                                            SizedBox(width: 15,),
                                            Column(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                SizedBox(height: 10,),
                                                Text(receivedOrders.length!=0?receivedOrders.first.product.name:'Hello', style: TextStyle(fontWeight: FontWeight.w700, fontSize: 16),),
                                                SizedBox(height: 8,),
                                                Text(receivedOrders.length!=0?receivedOrders.first.product.condition+" | " +
                                                    receivedOrders.first.product.size:'New',
                                                  style: TextStyle(color: Colors.grey),),
                                                SizedBox(height: 8,),
                                                Text(receivedOrders.length!=0?"AED "+receivedOrders.first.product.price:"AED 3000", style: TextStyle(fontWeight: FontWeight.w600, fontSize: 14),),
                                                SizedBox(height: 10,),
                                                GestureDetector(
                                                  onTap: ()async{
                                                    pr = MyProgressDialog(context);
                                                    pr.show();
                                                    try {
                                                      await API.acceptRejectBid(receivedOrders[0].bidId.toString(), "Accept");
                                                    }catch(e){
                                                      pr.hide();
                                                    }
                                                    pr.hide();
                                                    if(API.complete == 'true'&& success=="true"){
                                                      Fluttertoast.showToast(
                                                          msg: "Offer Accepted",
                                                          toastLength: Toast.LENGTH_SHORT,
                                                          gravity: ToastGravity.CENTER,
                                                          timeInSecForIosWeb: 1,
                                                          backgroundColor: Colors.black38,
                                                          textColor: Colors.white,
                                                          fontSize: 16.0
                                                      );
                                                      if(receivedOrders[0].vendorId!=int.parse(MyApp.sharedPreferences.getString("userId")))
                                                        setState(() {
                                                          ProductBid prod = ProductBid(product:receivedOrders[0].product ,bidPrice: receivedOrders[0].bidPrice,status: receivedOrders[0].status,productId:receivedOrders[0].productId ,bidId: receivedOrders[0].bidId,counter:receivedOrders[0].counter ,vendorId:receivedOrders[0].vendorId);
                                                          Navigator.push(context, MaterialPageRoute(builder: (_)=>ConfirmAddress(bidProduct: prod)));
                                                        });
                                                      else
                                                        setState(() {
                                                          receivedOrders.removeAt(0);
                                                        });
                                                    }
                                                    pr.hide();
                                                  },
                                                  child: Container(
                                                    height: 30,
                                                    width: 100,
                                                    decoration: BoxDecoration(
                                                        border: Border.all(color: Colors.green),
                                                        borderRadius: BorderRadius.circular(3)
                                                    ),
                                                    child:  Center(child: Text("Accept", style: TextStyle(color: Colors.green , fontSize: 10),)),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ],
                                        ):Center(child: Text("No Pending Offers"),),
                                      ),
                                    ],
                                  ):SizedBox(),
                                  SizedBox(height: 10,),
                                  MyApp.sharedPreferences.getString("isSeller")!="0"&&widget.userId==myUserId?
                                  Column(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Row(
                                          children: [
                                            Row(
                                              children: [
                                                Text('SOLD',style: Textprimary ,),
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                Text("${listOfSold.length}",style: LiteText,),
                                              ],
                                            ),
                                            Spacer(),
                                            listOfSold.length==0?
                                                SizedBox(): InkWell(
                                                onTap: (){
                                                  Navigator.push(
                                                    context,
                                                    MaterialPageRoute(builder: (context) => SoldProducts(listSold: listOfSold,)),
                                                  );
                                                },
                                                child: Text('View All  -->',style: TextStyle(fontSize: 11,color: Colors.blue),)),
                                          ],
                                        ),
                                      ),
                                      listOfSold.length==0?
                                      Container(
                                        height: 170,
                                        decoration: BoxDecoration(
                                            color: Color(0xffF4F4F4),
                                            borderRadius: BorderRadius.circular(10)
                                        ),
                                        child: Center(child: Text("No Products"),),
                                      ):
                                      ListView.builder(
                                        itemCount: listOfSold.length>2?2:listOfSold.length,
                                        shrinkWrap: true,
                                        physics: BouncingScrollPhysics(),
                                        itemBuilder: (context,index){
                                          return Container(
                                            height: 170,
                                            margin: EdgeInsets.symmetric(vertical: 3),
                                            decoration: BoxDecoration(
                                                color: Color(0xffF4F4F4),
                                                borderRadius: BorderRadius.circular(10)
                                            ),
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                              children: [
                                                Container(
                                                  width: 120,
                                                  margin: EdgeInsets.symmetric(vertical: 20),
                                                  child: ClipRRect(
                                                    borderRadius: BorderRadius.circular(10),
                                                    child:listOfSold.length!=0 &&listOfSold[index].feature_image!=null?
                                                      Image.network(BaseUrl+listOfSold[index].feature_image, fit: BoxFit.cover,):Image.asset('assets/bigshoe.png'),
                                                  ),
                                                ),
                                                SizedBox(width: 15,),
                                                Column(
                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: [
                                                    SizedBox(height: MediaQuery.of(context).size.height*.02,),
                                                    Row(
                                                      children: [
                                                        Text(
                                                          '${listOfSold[index].updated_at.toString().substring(0,10)}',
                                                          style: TextStyle(color: Colors.grey),
                                                          textAlign: TextAlign.end,
                                                        ),
                                                      ],
                                                      mainAxisAlignment: MainAxisAlignment.end,
                                                    ),
                                                    SizedBox(height: 10,),
                                                    Text(listOfSold.length!=0?listOfSold[index].name??"Product":'Product', style: TextStyle(fontWeight: FontWeight.w700, fontSize: 16),),
                                                    SizedBox(height: 8,),
                                                    Text(listOfSold.length!=0?listOfSold[index].condition + " | " + listOfSold[index].size:"New",
                                                      style: TextStyle(color: Colors.grey),),
                                                    SizedBox(height: 8,),
                                                    Text(listOfSold.length!=0?"AED "+listOfSold[index].price:"AED 3000", style: TextStyle(fontWeight: FontWeight.w600, fontSize: 14),),
                                                    SizedBox(height: 10,),
                                                    Container(
                                                      height: 30,
                                                      width: 100,
                                                      decoration: BoxDecoration(
                                                          border: Border.all(color: Colors.green),
                                                          borderRadius: BorderRadius.circular(3)
                                                      ),
                                                      child:  Center(child: Text("SOLD", style: TextStyle(color: Colors.green , fontSize: 10),)),
                                                    )
                                                  ],
                                                ),
                                              ],
                                            ),
                                          );
                                        },
                                      ),
                                    ],
                                  ):SizedBox(),
                                  widget.userId==myUserId?
                                  Column(
                                    children: [
                                      SizedBox(height: 10,),
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Row(
                                          children: [
                                            Row(
                                              children: [
                                                Text('ORDERED',style: Textprimary ,),
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                Text("${listOfPurchased.length}",style: LiteText,),
                                              ],
                                            ),
                                            Spacer(),
                                            listOfPurchased.length==0?
                                            SizedBox(): InkWell(
                                                onTap: (){
                                                  Navigator.push(
                                                    context,
                                                    MaterialPageRoute(builder: (context) => SoldProducts(listSold: listOfPurchased,isSold:false,)),
                                                  );
                                                },
                                                child: Text('View All  -->',style: TextStyle(fontSize: 11,color: Colors.blue),)),
                                          ],
                                        ),
                                      ),
                                      if (listOfPurchased.length==0) Container(
                                        height: 170,
                                        decoration: BoxDecoration(
                                            color: Color(0xffF4F4F4),
                                            borderRadius: BorderRadius.circular(10)
                                        ),
                                        child: Center(child: Text("No Products"),),
                                      ) else ListView.builder(
                                        itemCount: listOfPurchased.length>2?2:listOfPurchased.length,
                                        shrinkWrap: true,
                                        physics: BouncingScrollPhysics(),
                                        itemBuilder: (context,index){
                                          return Container(
                                            height: 170,
                                            width: MediaQuery.of(context).size.width,
                                            margin: EdgeInsets.symmetric(vertical: 3),
                                            decoration: BoxDecoration(
                                                color: Color(0xffF4F4F4),
                                                borderRadius: BorderRadius.circular(10)
                                            ),
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                              children: [
                                                Container(
                                                  width: 120,
                                                  margin: EdgeInsets.symmetric(vertical: 20),
                                                  child: ClipRRect(
                                                    borderRadius: BorderRadius.circular(10),
                                                    child:listOfPurchased.length!=0 &&listOfPurchased[index].feature_image!=null?
                                                    Image.network(BaseUrl+listOfPurchased[index].feature_image, fit: BoxFit.cover,):Image.asset('assets/bigshoe.png'),
                                                  ),
                                                ),
                                                SizedBox(width: 15,),
                                                Expanded(
                                                  child: Column(
                                                    mainAxisAlignment: MainAxisAlignment.start,
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: [
                                                      SizedBox(height: MediaQuery.of(context).size.height*.02,),
                                                      Row(
                                                        children: [
                                                          SizedBox(width: 50,),
                                                          Text(
                                                            '${listOfPurchased[index].updated_at.toString().substring(0,10)}',
                                                            style: TextStyle(color: Colors.grey),
                                                            textAlign: TextAlign.end,
                                                          ),
                                                        ],
                                                        mainAxisAlignment: MainAxisAlignment.center,
                                                      ),
                                                      SizedBox(height: 10,),
                                                      Text(
                                                        listOfPurchased.length!=0?listOfPurchased[index].name??"Product":'Product',
                                                        style: TextStyle(fontWeight: FontWeight.w700, fontSize: 16),
                                                        overflow: TextOverflow.ellipsis,
                                                      ),
                                                      SizedBox(height: 8,),
                                                      Text(listOfPurchased.length!=0?listOfPurchased[index].condition + " | " + listOfPurchased[index].size:"New",
                                                        style: TextStyle(color: Colors.grey),),
                                                      SizedBox(height: 8,),
                                                      Text(listOfPurchased.length!=0?"AED1 "+listOfPurchased[index].price:"AED 3000", style: TextStyle(fontWeight: FontWeight.w600, fontSize: 14),),
                                                      SizedBox(height: 10,),
                                                      Container(
                                                        height: 30,
                                                        width: 100,
                                                        decoration: BoxDecoration(
                                                            border: Border.all(color: Colors.green),
                                                            borderRadius: BorderRadius.circular(3)
                                                        ),
                                                        child:  Center(child: Text("ORDERED", style: TextStyle(color: Colors.green , fontSize: 10),)),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          );
                                        },
                                      ),
                                      SizedBox(height: 20,),
                                      myUserId==widget.userId?
                                          GestureDetector(
                                            onTap: ()async{
                                              String url = "https://laybull.com/get-support";
                                              if (await canLaunch(url))
                                              await launch(url);
                                              else
                                              Fluttertoast.showToast(
                                              msg: "Failed to open Link",
                                              toastLength: Toast.LENGTH_SHORT,
                                              gravity: ToastGravity.CENTER,
                                              timeInSecForIosWeb: 1,
                                              backgroundColor: Colors.black38,
                                              textColor: Colors.white,
                                              fontSize: 16.0
                                              );
                                            },
                                            child:  Container(
                                              width:  MediaQuery.of(context).size.width/1.07,
                                              height: 45,
                                              margin: EdgeInsets.only(bottom: 5),
                                              alignment: Alignment.center,
                                              decoration: BoxDecoration(
                                                color: Colors.black,
                                                borderRadius: BorderRadius.circular(5.0),
                                              ),
                                              child: Text(
                                                'GET SUPPORT',
                                                style:TextStyle(fontSize: 14,fontWeight: FontWeight.bold,color: Colors.white,letterSpacing: 2,fontFamily: "MetropolisExtraBold"),
                                              ),
                                            ),
                                          )
                                          :SizedBox(),
                                    ],
                                  )
                                      :SizedBox(),
                                  SizedBox(height: 10,),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                Positioned(
                  top:115,
                  left: 20,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.grey[200],
                        borderRadius: BorderRadius.circular(5),
                      ),
                      height: 103,
                      width: 103,
                      child: jsonData["user"]["detail"]["image"]!=null? Image.network(jsonData["user"]["detail"]["image"],):Image.asset('assets/page2.png'),
                    ),
                  ),
                ),
                widget.userId==myUserId?
                Align(
                  alignment: Alignment.topRight,
                  child: Container(
                    margin: EdgeInsets.only(top: 5,),
                    child: GestureDetector(
                      onTap: (){
                        MyApp.sharedPreferences.clear();
                        Navigator.of(context).popUntil((predicate) => predicate.isFirst);
                        Navigator.of(context).pushReplacement(
                          MaterialPageRoute(builder: (context) => LoginPage()),
                        );
                      },
                      child: Card(
                        elevation: 5,
                        shadowColor: Colors.grey,
                        child: Container(
                          width: 70,
                          height: 45,
                          child: Center(child: Icon(Icons.logout,color: Colors.black,)),
                        ),
                      ),
                    ),
                  ),
                ):
                SizedBox(),
                widget.isFromBottomNav?
                Align(
                  alignment: Alignment.topLeft,
                  child: Container(
                    margin: EdgeInsets.only(top: 5,),
                    child: GestureDetector(
                      onTap: (){
                        AppRoutes.push(context, notification(isFromProfile: true,));
                      },
                      child: Card(
                        elevation: 5,
                        shadowColor: Colors.grey,
                        child: Container(
                          width: 70,
                          height: 45,
                          child: Center(
                              child: FlutterBadge(
                                itemCount: _notificationProvider.getCount(),
                                badgeTextColor: Colors.white,
                                badgeColor: Colors.black38,
                                textSize: 16,
                                borderRadius: 100,
                                icon: Icon(
                                  Icons.notifications_none_rounded,
                                  size: 30,
                                ),
                              ),
                          ),
                        ),
                      ),
                    ),
                  ),
                )
                :Align(
                  alignment: Alignment.topLeft,
                  child: Container(
                    margin: EdgeInsets.only(top: 5,),
                    child: GestureDetector(
                      onTap: (){
                        Navigator.pop(context);
                      },
                      child: Card(
                        elevation: 5,
                        shadowColor: Colors.grey,
                        child: Container(
                          width: 70,
                          height: 45,
                          child: Center(child: Image.asset('assets/ARROW.png',)),
                        ),
                      ),
                    ),
                  ),
                ),

              ],
            ),
          ),
        ),
      ),
    );
  }
}


class BecomeSeller extends StatefulWidget {
  final int userId;
  BecomeSeller({@required this.userId,});
  @override
  BecomeSellerState createState() => BecomeSellerState();
}
class BecomeSellerState extends State<BecomeSeller> {
  MyProgressDialog pr;
  TextEditingController _accountName = TextEditingController();
  TextEditingController _accountNumber = TextEditingController();
  TextEditingController _ibanNumber = TextEditingController();
  TextEditingController _accountHolderPhone = TextEditingController();
  TextEditingController _bankName = TextEditingController();
  var _sellerFormKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    pr = MyProgressDialog(context);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.only(left: 30,right: 30, top: 30),
          child: ListView(
            children: [
              Row(
                children: [
                  GestureDetector(
                    onTap: (){
                      Navigator.pop(context);
                    },
                    child: Container(
                        padding:EdgeInsets.only(right: 10,bottom: 10,top: 10),child: Image.asset(
                      'assets/ARROW.png',
                    )
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "account details".toUpperCase(),
                    style: headingStyle,
                  ),

                ],
              ),
              SizedBox(height: 20,),
              Form(
                key: _sellerFormKey,
                child: Column(
                  children: [
                    /// Bank Name
                    Container(
                      width: MediaQuery.of(context).size.width / 1.07,
                      child: TextFormField(
                          controller: _bankName,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Enter Bank Name';
                            }
                            return null;
                          },
                          style: TextStyle(
                              fontSize: 15,
                              fontFamily: 'Metropolis',
                              letterSpacing: 2.2),
                          decoration: InputDecoration(
                              hintText:'Bank Name',
                              border: InputBorder.none,
                              fillColor: Color(0xfff3f3f4),
                              filled: true)),
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    /// IBAN
                    Container(
                      width: MediaQuery.of(context).size.width / 1.07,
                      child: TextFormField(
                          controller: _ibanNumber,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Enter IBAN';
                            }
                            return null;
                          },
                          style: TextStyle(
                              fontSize: 15,
                              fontFamily: 'Metropolis',
                              letterSpacing: 2.2),
                          decoration: InputDecoration(
                              hintText:'IBAN',
                              border: InputBorder.none,
                              fillColor: Color(0xfff3f3f4),
                              filled: true)),
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    /// Account Number
                    Container(
                      width: MediaQuery.of(context).size.width / 1.07,
                      child: TextFormField(
                          controller: _accountNumber,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Enter Account Number';
                            }
                            return null;
                          },
                          style: TextStyle(
                              fontSize: 15,
                              fontFamily: 'Metropolis',
                              letterSpacing: 2.2),
                          decoration: InputDecoration(
                              hintText:'Account Number',
                              border: InputBorder.none,
                              fillColor: Color(0xfff3f3f4),
                              filled: true)),
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    /// Account Holder Name
                    Container(
                      width: MediaQuery.of(context).size.width / 1.07,
                      child: TextFormField(
                          controller: _accountName,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Enter Account Holder Name';
                            }
                            return null;
                          },
                          style: TextStyle(
                              fontSize: 15,
                              fontFamily: 'Metropolis',
                              letterSpacing: 2.2),
                          decoration: InputDecoration(
                              hintText:'Account Holder Name',
                              border: InputBorder.none,
                              fillColor: Color(0xfff3f3f4),
                              filled: true)),
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    /// Account Holder Phone Number
                    Container(
                      width: MediaQuery.of(context).size.width / 1.07,
                      child: TextFormField(
                          controller: _accountHolderPhone,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Enter Account Holder Phone Number';
                            }
                            return null;
                          },
                          style: TextStyle(
                              fontSize: 15,
                              fontFamily: 'Metropolis',
                              letterSpacing: 2.2),
                          decoration: InputDecoration(
                              hintText:'Account Holder Phone Number',
                              border: InputBorder.none,
                              fillColor: Color(0xfff3f3f4),
                              filled: true)),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  GestureDetector(
                    onTap: ()async{
                      String url = "https://laybull.com/data-collection-policy";
                      if (await canLaunch(url))
                        await launch(url);
                      else
                        Fluttertoast.showToast(
                            msg: "Failed to open Link",
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.CENTER,
                            timeInSecForIosWeb: 1,
                            backgroundColor: Colors.black38,
                            textColor: Colors.white,
                            fontSize: 16.0
                        );
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(left:12.0),
                      child: Text('Why we need this?',style: TextStyle(color: Color(0xff5DB3F5,),decoration: TextDecoration.underline)),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 30,),
              InkWell(
                onTap:()async{
                  if(_sellerFormKey.currentState.validate()){
                    try{
                      pr = MyProgressDialog(context);
                      pr.show();
                      await API.becomeSeller(_accountNumber.text.trim(),_accountName.text.trim(),_bankName.text.trim(),_accountHolderPhone.text.trim(),widget.userId,_ibanNumber.text.trim());
                      if(API.complete == 'true'){
                        pr.hide();
                        if("success"==API.error){
                          Fluttertoast.showToast(
                              msg: "Become Seller Successfully",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.CENTER,
                              timeInSecForIosWeb: 1,
                              backgroundColor: Colors.black38,
                              textColor: Colors.white,
                              fontSize: 16.0
                          );
                          MyApp.sharedPreferences.setString("isSeller", "1");
                          Navigator.of(context).popUntil((predicate) => predicate.isFirst);
                          Navigator.of(context).pushReplacement(
                            MaterialPageRoute(builder: (context) => BottomNav()),
                          );
                        }
                      }
                      else{
                        Fluttertoast.showToast(
                            msg: "Operation Failed please try again",
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.CENTER,
                            timeInSecForIosWeb: 1,
                            backgroundColor: Colors.black38,
                            textColor: Colors.white,
                            fontSize: 16.0
                        );
                        pr.hide();
                      }
                    }
                    catch(e){
                      print("Exception = $e");
                      pr.hide();
                      Fluttertoast.showToast(
                          msg: "Something went wrogeee",
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.CENTER,
                          timeInSecForIosWeb: 1,
                          backgroundColor: Colors.black38,
                          textColor: Colors.white,
                          fontSize: 16.0
                      );
                    }
                  pr.hide();
                  }else return;
                },
                child: Container(
                  width:  MediaQuery.of(context).size.width/1.07,
                  height: 45,
                  margin: EdgeInsets.only(bottom: 5),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: Colors.black,
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  child: Text(
                    'SAVE',
                    style:TextStyle(fontSize: 14,fontWeight: FontWeight.bold,color: Colors.white,letterSpacing: 2,fontFamily: "MetropolisExtraBold"),
                  ),
                ),
              ),
              SizedBox(height: 20,),
            ],
          ),
        ),
      ),
    );
  }
}