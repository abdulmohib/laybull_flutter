import 'package:Laybull/main.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:Laybull/API/API.dart';
import 'package:Laybull/API/BaseUrl.dart';
import 'package:Laybull/API/GloabalVariable.dart';
import 'package:Laybull/Addtochart.dart';
import 'package:Laybull/Cart/Cart.dart';
import 'package:Laybull/Model/Product.dart';
import 'package:Laybull/Model/bidModel.dart';
import 'package:Laybull/constant.dart';
import 'package:Laybull/myCustomProgressDialog.dart';
import 'package:progress_dialog/progress_dialog.dart';

class Offer extends StatefulWidget {
  @override
  _OfferState createState() => _OfferState();
}

class _OfferState extends State<Offer> {
  bool sentvisible = true;
  bool recevied = false ;
  Color colors = Colors.black;
  Color textSentColor = Colors.white;
  Color textReceivedColor = Colors.black;
  Color color = Colors.grey[100];

@override
  void initState() {
   sentvisible = true;
   recevied = false ;
    super.initState();
  }

  MyProgressDialog pr;
  @override
  Widget build(BuildContext context) {
    pr = MyProgressDialog(context);
    return Scaffold(
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.only(left: 10,right: 10, top: 20),
          child: ListView(
            shrinkWrap: true,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 10,),
                child: Text("OFFERS", style: headingStyle,),
              ),
              SizedBox(height: 15,),
              Row(
                children: [
                  InkWell(
                    onTap: (){
                      setState(() {
                        if(sentvisible != true)
                          {
                            print(receivedOrders.length);
                            textReceivedColor = Colors.black;
                            textSentColor = Colors.white;
                            colors = Colors.black;
                            sentvisible = true;
                            recevied = false;
                            setState(() {
                              color = Color(0xFFE5E5E5);
                            });
                          }
                      });
                    },
                    child: Container(
                      height: 40,
                        width: MediaQuery.of(context).size.width/2.2,
                        decoration: BoxDecoration(
                            color: colors,
                          borderRadius: BorderRadius.circular(10)
                        ),
                        child: Center(child: Text("SENT", style: TextStyle(fontSize:14,color: textSentColor,letterSpacing: 2,fontFamily: "MetropolisExtraBold",fontWeight: FontWeight.bold),))
                    ),
                  ),
                  Spacer(),
                  InkWell(
                    onTap: (){
                      setState(() {
                        if(recevied != true)
                        {
                          color = Colors.black;
                          textReceivedColor = Colors.white;
                          textSentColor = Colors.black;
                          colors = Color(0xFFE5E5E5);
                          recevied = true;
                          sentvisible = false;

                        }
                      });
                    },
                    child: Container(
                      height: 40,
                        width: MediaQuery.of(context).size.width/2.2,
                        decoration: BoxDecoration(
                            color: color,
                            borderRadius: BorderRadius.circular(10)

                        ),
                        child: Center(child: Text("Received".toUpperCase(), style: TextStyle(fontSize:14,color:  textReceivedColor,fontWeight: FontWeight.bold,letterSpacing: 2,fontFamily: "MetropolisExtraBold",),))
                    ),
                  ),

                ],
              ),
              SizedBox(height: 30,),
              Visibility(
                visible: sentvisible ,
                child: Column(
                  children: [
                    Container(
                      // height: MediaQuery.of(context).size.height/1.4,
                      child: Padding(
                        padding: const EdgeInsets.only(top: 10,),
                        child: ListView.builder(
                            itemCount: sentOffer.length,
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            itemBuilder: (BuildContext cntxt, int index){
                          return Container(
                            height: 130,
                            padding: EdgeInsets.symmetric(horizontal: 20),
                            decoration: BoxDecoration(
                                color: Color(0xffF4F4F4),
                                borderRadius: BorderRadius.circular(10)
                            ),
                            child: Row(
                              children: [
                                Container(
                                  width: 120,
                                  margin: EdgeInsets.symmetric(vertical: 10),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(10),
                                    child: Image.network(BaseUrl + sentOffer[index].product.feature_image, fit:BoxFit.cover),
                                  ),
                                ),
                                SizedBox(width: 30,),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SizedBox(height: 10,),
                                    Text(sentOffer[index].product.name, style: TextStyle(fontWeight: FontWeight.w700, fontSize: 16),),
                                    SizedBox(height: 8,),
                                    Text(sentOffer[index].product.condition+" | "+sentOffer[index].product.size.toString(), style: TextStyle(color: Colors.grey),),
                                    SizedBox(height: 8,),
                                    Text("AED ${sentOffer[index].counter!=null?sentOffer[index].counter:sentOffer[index].bidPrice}", style: TextStyle(fontWeight: FontWeight.w600, fontSize: 14),),
                                    SizedBox(height: 8,),
                                    InkWell(
                                      onTap:sentOffer[index].status!="Accept"?null: (){
                                        Navigator.of(context).push(
                                            MaterialPageRoute(
                                                builder: (_) => ConfirmAddress(bidProduct: sentOffer[index],)));
                                      },
                                      child: Container(
                                        height: 30,
                                        width: 100,
                                        decoration: BoxDecoration(
                                            // color: Colors.green,
                                            color: sentOffer[index].status==null?Color(0xfff89406):sentOffer[index].status.toUpperCase()=="Accept".toUpperCase()?Colors.green:Colors.red,
                                            borderRadius: BorderRadius.circular(5)
                                        ),
                                        child:  Center(child: Text(sentOffer[index].status==null?"Pending":sentOffer[index].status.toUpperCase()=="Accept".toUpperCase()?"Accepted":"Rejected", style: TextStyle(color: Colors.white),)),
                                      ),
                                    )
                                  ],
                                )

                              ],
                            ),
                          );
                        })
                      ),
                    ),
                    SizedBox(height: 50,),
                  ],
                ),
              ),
              Visibility(
                visible: recevied,
                child:  Container(
                  height: 170,
                  padding: EdgeInsets.only(top: 10),
                  child: ListView.builder(
                      itemCount: receivedOrders.length,
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemBuilder: (BuildContext ctxt, int index) {
                        return Container(
                          padding: EdgeInsets.symmetric(horizontal: 20,vertical: 20),
                          // height: 170,
                          decoration: BoxDecoration(
                              color: Color(0xffF4F4F4),
                              borderRadius: BorderRadius.circular(10)
                          ),
                          child: Column(
                            children: [
                              Row(
                                // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  Container(
                                    width: 120,
                                    height: 80,
                                    margin: EdgeInsets.symmetric(vertical: 5),
                                    child:  ClipRRect(
                                      child: Container(
                                          width: 120,
                                          height: 80,
                                          child: Center(child: Image.network(BaseUrl + receivedOrders[index].product.feature_image, fit: BoxFit.cover,))
                                      ),
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                  ),
                                  SizedBox(width: 30,),
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text(receivedOrders[index].product.name, style: TextStyle(fontWeight: FontWeight.w700, fontSize: 16),),
                                      SizedBox(height: 8,),
                                      Text(receivedOrders[index].product.type=="0"?"Used |":"New |" + receivedOrders[index].product.size, style: TextStyle(color: Color(0xffB0B0B0)),),
                                      SizedBox(height: 8,),
                                      Text("AED ${receivedOrders[index].counter==null?receivedOrders[index].bidPrice:receivedOrders[index].counter}", style: TextStyle(fontWeight: FontWeight.w600, fontSize: 14),),
                                    ],
                                  )
                                ],
                              ),
                              SizedBox(height: 10,),
                              receivedOrders[index].status==null?
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  receivedOrders[index].counter==null?
                                  Expanded(
                                    child: GestureDetector(
                                      child: Container(
                                        height: 30,
                                        decoration: BoxDecoration(
                                            color: Colors.black,
                                            borderRadius: BorderRadius.circular(3)
                                        ),
                                        child:  Center(child: Text("COUNTER OFFER", style: TextStyle(color: Colors.white, fontSize: 9),)),
                                      ),
                                      onTap: (){
                                        _showMyDialog(index);
                                      },
                                    ),
                                  ):SizedBox(),
                                  SizedBox(width: receivedOrders[index].counter==null?3:0,),
                                  Expanded(
                                    child: GestureDetector(
                                      child: Container(
                                        height: 30,
                                        decoration: BoxDecoration(
                                            border: Border.all(color: Colors.green),
                                            borderRadius: BorderRadius.circular(3)
                                        ),
                                        child:  Center(child: Text("Accept", style: TextStyle(color: Colors.green,fontSize: 10),)),
                                      ),
                                      onTap: ()async{
                                        pr.show();
                                        try {
                                          await API.acceptRejectBid(receivedOrders[index].bidId.toString(), "Accept");
                                        }catch(e){
                                          pr.hide();
                                        }
                                        pr.hide();
                                        if(API.complete == 'true'&& success=="true"){
                                          Fluttertoast.showToast(
                                              msg: "Offer Accepted",
                                              toastLength: Toast.LENGTH_SHORT,
                                              gravity: ToastGravity.CENTER,
                                              timeInSecForIosWeb: 1,
                                              backgroundColor: Colors.black38,
                                              textColor: Colors.white,
                                              fontSize: 16.0
                                          );
                                          if(receivedOrders[index].vendorId!=int.parse(MyApp.sharedPreferences.getString("userId")))
                                          setState(() {
                                            ProductBid prod = ProductBid(product:receivedOrders[index].product ,bidPrice: receivedOrders[index].bidPrice,status: receivedOrders[index].status,productId:receivedOrders[index].productId ,bidId: receivedOrders[index].bidId,counter:receivedOrders[index].counter ,vendorId:receivedOrders[index].vendorId);
                                            Navigator.push(context, MaterialPageRoute(builder: (_)=>ConfirmAddress(bidProduct: prod)));
                                          });
                                          else
                                          setState(() {
                                            receivedOrders.removeAt(index);
                                          });
                                        }
                                        pr.hide();
                                      },
                                    ),
                                  ),
                                  SizedBox(width: 3,),
                                  Expanded(
                                    child: GestureDetector(
                                      onTap: ()async{
                                        pr.show();
                                        try {
                                          await API.acceptRejectBid(receivedOrders[index].bidId.toString(), "Reject");
                                        }catch(e){
                                          pr.hide();
                                        }
                                        pr.hide();
                                        if(API.complete == 'true'&& success=="true"){
                                          Fluttertoast.showToast(
                                              msg: "Offer Rejected",
                                              toastLength: Toast.LENGTH_SHORT,
                                              gravity: ToastGravity.CENTER,
                                              timeInSecForIosWeb: 1,
                                              backgroundColor: Colors.black38,
                                              textColor: Colors.white,
                                              fontSize: 16.0
                                          );
                                            setState(() {
                                              receivedOrders.removeAt(index);
                                            });
                                        }
                                        pr.hide();
                                      },
                                      child: Container(
                                        height: 30,
                                        width: 100,
                                        decoration: BoxDecoration(
                                            border: Border.all(color: Colors.red),
                                            borderRadius: BorderRadius.circular(3)
                                        ),
                                        child:  Center(child: Text("REJECT", style: TextStyle(color: Colors.red , fontSize: 10),)),
                                      ),
                                    ),
                                  ),
                                ],
                              )
                                  : Row(
                                    children: [
                                      Container(
                                      padding: EdgeInsets.symmetric(vertical: 5,horizontal: 10,),
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(5),
                                        border: Border.all(color: Colors.green,width: 1)
                                      ),
                                child: Center(child: Text("${receivedOrders[index].status}ed"),),
                                // width: MediaQuery.of(context).size.width*.2,
                              ),
                                    ],
                                mainAxisAlignment: MainAxisAlignment.center,
                                  ),
                            ],
                          ),
                        );
                        SizedBox(height: 10,);
                      }),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _bidController = new TextEditingController();
  Future<void> _showMyDialog(int index) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return Form(
          key: _formKey,
          child: AlertDialog(
            title: Text('COUNTER OFFER'),
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  Text('Enter your price below'),
                  SizedBox(height: 20),
                  TextFormField(
                      controller: _bidController,
                      inputFormatters: [
                        FilteringTextInputFormatter.digitsOnly,
                      ],

                      validator: (value){
                        if(value.isEmpty){
                          return 'Please enter Counter Offer amount';
                        }
                        return null;
                      },
                      style: TextStyle(
                          fontSize: 15, fontFamily: 'Metropolis', letterSpacing: 2.2),
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          suffix: Text(
                            "AED",
                            style: TextStyle(fontWeight: FontWeight.bold,letterSpacing: 2,fontFamily: "MetropolisExtraBold"),
                          ),
                          fillColor: Color(0xfff3f3f4),
                          filled: true)
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  TextButton(
                    child: Text('CANCEL'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                  TextButton(
                    child: Text('SEND'),
                    onPressed: () async{
                      if(_formKey.currentState.validate()){
                        FocusScope.of(context).unfocus();
                        Navigator.pop(context);
                        pr.show();
                        try {

                          await API.counterBid(receivedOrders[index].bidId,int.parse(_bidController.text.trim()) ,receivedOrders[index].product);
                        }catch(e){
                          pr.hide();
                        }
                        pr.hide();
                        if(API.complete == 'true'&& success=="true"){
                          Fluttertoast.showToast(
                              msg: "Counter Offer Sent",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.CENTER,
                              timeInSecForIosWeb: 1,
                              backgroundColor: Colors.black38,
                              textColor: Colors.white,
                              fontSize: 16.0
                          );
                          setState(() {
                            receivedOrders.removeAt(index);
                          });
                        }
                        pr.hide();
                      }
                    },
                  ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}
