import 'dart:io';

import 'package:Laybull/constant.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:Laybull/API/API.dart';
import 'package:Laybull/API/BaseUrl.dart';
import 'package:Laybull/API/GloabalVariable.dart';
import 'package:Laybull/Model/countryModel.dart';
import 'package:Laybull/ProductPages/addproduct.dart';
import 'package:Laybull/customDioPkg/lib/dio.dart';
import 'package:Laybull/main.dart';
import 'package:Laybull/myCustomProgressDialog.dart';
import 'package:image_picker/image_picker.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:url_launcher/url_launcher.dart';
import 'Login.dart';

class Signup extends StatefulWidget {

  @override
  _SignupState createState() => _SignupState();
}

class _SignupState extends State<Signup> {

  var _formKey = GlobalKey<FormState>();
  var isLoading = false;



  @override
  void initState() {
    fetchCountry();
    _accountHolderPhone.text = "";
    _accountName.text = "";
    _accountNumber.text = "";
    _bankName.text = "";
    super.initState();
  }

  // void _submit() {
  //   final isValid = _formKey.currentState.validate();
  //   if (!isValid) {
  //     return;
  //   }else
  //   {
  //
  //     setState(() {
  //       widget.getUserDetails(_first.text,_last.text,_email.text,_phone.text);
  //     });
  //
  //     Future.delayed(
  //         const Duration(
  //             milliseconds: 500), () {
  //
  //       Navigator.pop(context);
  //     });
  //   }
  //   _formKey.currentState.save();
  // }
  TextEditingController _first = TextEditingController();
  TextEditingController _last = TextEditingController();
  TextEditingController _address = TextEditingController();
  TextEditingController _city = TextEditingController();
  TextEditingController _email = TextEditingController();
  TextEditingController _password = TextEditingController();
  TextEditingController _phone = TextEditingController();
  TextEditingController _accountName = TextEditingController();
  TextEditingController _accountNumber = TextEditingController();
  TextEditingController _accountHolderPhone = TextEditingController();
  TextEditingController _bankName = TextEditingController();
  TextEditingController _ibanNumber = TextEditingController();
  var _sellerFormKey = GlobalKey<FormState>();
  ImageClass imageFile = ImageClass(isPicked: false,image: null);
  CountryModel selectedCountry;
  bool isCityFetched = false;
  Future fetchCountry()async{
    var url = BaseUrl+"api/list-of-countries";
    Dio dio = new Dio();
    // dio.options.headers["authorization"] = "Bearer " +
    //     MyApp.sharedPreferences.getString("access_token").toString();
    await dio
        .get(url,)
        .then((response) {
          if (response.statusCode==200){
        listCountry = List<CountryModel>.from(response.data["data"]["country"].map((prod) => CountryModel.fromJson(prod)).toList());
        setState(() {
          isCityFetched = true;
        });
      } else {
        success = "true";
        print("Exception in Api = $url");
        throw Exception('Failed to Fetch Vendors');
      }
    });
  }

  void _showPicker(context,) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text('Photo Library'),
                      onTap: () {
                        _imgFromGallery();
                        Navigator.of(context).pop();
                      }),
                  new ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text('Camera'),
                    onTap: () {
                      _imgFromCamera();
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        }
    );
  }

  _imgFromGallery() async {
    File image = await  ImagePicker.pickImage(
        source: ImageSource.gallery, imageQuality: 50
    );
    if(image!=null)
      setState(() {
        imageFile.image = image;
        imageFile.isPicked = true;
      });
  }
  _imgFromCamera() async {
    File image = await ImagePicker.pickImage(
        source: ImageSource.camera, imageQuality: 50
    );
    setState(() {
      imageFile.image = image;
      imageFile.isPicked = true;
    });
  }

  MyProgressDialog pr;
  bool isSeller = false;
  @override
  Widget build(BuildContext context) {
    pr = MyProgressDialog(context);
    return Scaffold(
      // resizeToAvoidBottomPadding: false,
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Container(
          height: MediaQuery.of(context).size.height,
          child: Form(
            key: _formKey,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: ListView(
                children: [
                  SizedBox(height: 20,),
                  /// SignUp Text
                  Row(
                    children: [
                      // IconButton(
                      //   icon: Icon(Icons.arrow_back),
                      //   // onPressed: () => Navigator.of(context)
                      //   // .push(MaterialPageRoute(builder: (_) => MainPage(0,0,0, "" , "")))
                      // ),
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Container(
                            // width: MediaQuery.of(context).size.width/1.2,
                            child: Center(
                              child: Text(
                                "SignUp",
                                style:
                                TextStyle(fontSize: 20, fontWeight: FontWeight.w900,letterSpacing: 2),
                              ),
                            )),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 25,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                          height: 75,
                          width: 75,
                          decoration: BoxDecoration(
                            border: Border.all(
                              width: 1.0,
                            ),
                            borderRadius:
                            BorderRadius.all(Radius.circular(5.0)),
                          ),
                          child: Center(
                            child: GestureDetector(
                              onLongPress: (){
                                setState(() {
                                  imageFile.isPicked = false;
                                  imageFile.image = null;
                                });
                              },
                              onTap: () {
                                _showPicker(context,);
                              },
                              child: Container(
                                child: imageFile.isPicked
                                    ? Image.file(
                                  imageFile.image,
                                  width: 100,
                                  height: 100,
                                  fit: BoxFit.fill,
                                )
                                    : Container(
                                  decoration: BoxDecoration(
                                      color: Color(0xFFE5F3FD),
                                      borderRadius: BorderRadius.circular(0)),
                                  width: 100,
                                  height: 100,
                                  child: Icon(
                                    Icons.camera_alt,
                                    color: Colors.grey[800],
                                  ),
                                ),
                              ),
                            ),
                          )
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 25,
                  ),
                  /// First Name
                  Container(
                    width: MediaQuery.of(context).size.width / 1.07,
                    child: TextFormField(
                        controller: _first,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Enter First Name';
                          }
                          return null;
                        },
                        style: TextStyle(
                            fontSize: 15,
                            fontFamily: 'Metropolis',
                            letterSpacing: 2.2),
                        decoration: InputDecoration(
                            hintText:'First Name',
                            border: InputBorder.none,
                            fillColor: Color(0xfff3f3f4),
                            filled: true)),
                  ),
                  SizedBox(
                    height: 25,
                  ),
                  /// Last Name
                  Container(
                    width: MediaQuery.of(context).size.width / 1.07,
                    child: TextFormField(
                        controller: _last,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Enter Last Name';
                          }
                          return null;
                        },
                        style: TextStyle(

                            fontSize: 15,
                            fontFamily: 'Metropolis',
                            letterSpacing: 2.2),
                        decoration: InputDecoration(
                            hintText:'Last Name',
                            border: InputBorder.none,
                            fillColor: Color(0xfff3f3f4),
                            filled: true)),
                  ),
                  SizedBox(
                    height: 25,
                  ),
                  /// Email
                  Container(
                    width: MediaQuery.of(context).size.width / 1.07,
                    child: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Enter Email';
                          }
                          return null;
                        },
                        controller: _email,
                        style: TextStyle(
                            fontSize: 15, letterSpacing: 2.2),
                        decoration: InputDecoration(
                            hintText:'Email',
                            border: InputBorder.none,
                            fillColor: Color(0xfff3f3f4),
                            filled: true)),
                  ),
                  SizedBox(
                    height: 25,
                  ),
                  /// Password
                  Container(
                    width: MediaQuery.of(context).size.width / 1.07,
                    child: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Enter Password';
                          }else if(value.length<8){
                            return 'Password must be at least 8 characters';
                          }
                          return null;
                        },
                        controller: _password,
                        obscureText: true,
                        style: TextStyle(
                            fontSize: 15, letterSpacing: 2.2),
                        decoration: InputDecoration(
                            hintText:'Password',
                            border: InputBorder.none,
                            fillColor: Color(0xfff3f3f4),
                            filled: true)),
                  ),
                  SizedBox(
                    height:25,
                  ),
                  /// Phone Number
                  Container(
                    width: MediaQuery.of(context).size.width / 1.07,
                    child: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Enter Phone Number';
                          }
                          return null;
                        },
                        controller: _phone,
                        style: TextStyle(
                            fontSize: 15, letterSpacing: 2.2),
                        decoration: InputDecoration(
                            hintText:'Phone Number',
                            border: InputBorder.none,
                            fillColor: Color(0xfff3f3f4),
                            filled: true)),
                  ),
                  SizedBox(
                    height:25,
                  ),
                  /// Country
                  isCityFetched?
                  Container(
                    padding: EdgeInsets.only(left: 10, right: 10),
                    decoration: BoxDecoration(
                      color: Color(0xfff3f3f4),
                      // border: Border.all(color:Colors.grey[400]),
                      borderRadius: BorderRadius.circular(5.0),),
                    child: DropdownButton(
                      isExpanded: true,
                      hint: Text("Country"),
                      dropdownColor: Colors.grey[100],
                      icon: Icon(
                        Icons.keyboard_arrow_down_outlined,
                        size: 20,
                        color: Colors.black,
                      ),
                      iconSize: 12,
                      underline: SizedBox(),
                      style: TextStyle(color: Colors.black, fontSize: 15),
                      value: selectedCountry,
                      onChanged: (newValue) {
                        setState(() {
                          print("value = $selectedCountry");
                          selectedCountry = newValue;
                        });
                      },
                      items: listCountry.map((valueItem) {
                        return DropdownMenuItem(
                          value: valueItem,
                          child: new Row(
                            children: <Widget>[
                              Text(valueItem.countryName),
                            ],
                          ),
                        );
                      }).toList(),
                    ),
                  )
                      :SizedBox(),
                  SizedBox(
                    height:25,
                  ),
                  /// City
                  Container(
                    width: MediaQuery.of(context).size.width / 1.07,
                    child: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Enter City';
                          }
                          return null;
                        },
                        controller: _city,
                        style: TextStyle(
                            fontSize: 15, letterSpacing: 2.2),
                        decoration: InputDecoration(
                            hintText:'City',
                            border: InputBorder.none,
                            fillColor: Color(0xfff3f3f4),
                            filled: true)),
                  ),
                  SizedBox(
                    height:25,
                  ),
                  /// Address
                  Container(
                    width: MediaQuery.of(context).size.width / 1.07,
                    child: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Enter Address ';
                          }
                          return null;
                        },
                        controller: _address,
                        style: TextStyle(
                            fontSize: 15, letterSpacing: 2.2),
                        decoration: InputDecoration(
                            hintText:'Address',
                            border: InputBorder.none,
                            fillColor: Color(0xfff3f3f4),
                            filled: true)
                    ),
                  ),
                  SizedBox(height: 15,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("Become Seller ",style: TextStyle(fontWeight: FontWeight.w400,fontSize: 15, color:Colors.black,letterSpacing: 2.2),),
                      SizedBox(width: 10,),
                      Checkbox(
                          value: isSeller,
                          activeColor: Colors.black,
                          checkColor: Colors.white,
                          onChanged: (value){
                            setState(() {
                              isSeller = value;
                              _accountNumber.clear();
                              _accountName.clear();
                              _accountHolderPhone.clear();
                              _bankName.clear();
                            });
                          }),
                    ],
                  ),
                  SizedBox(height: isSeller?25:0,),
                  isSeller?
                  Form(
                    key: _sellerFormKey,
                    child: Column(
                      children: [
                        /// Bank Name
                        Container(
                          width: MediaQuery.of(context).size.width / 1.07,
                          child: TextFormField(
                              controller: _bankName,
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Enter Bank Name';
                                }
                                return null;
                              },
                              style: TextStyle(
                                  fontSize: 15,
                                  fontFamily: 'Metropolis',
                                  letterSpacing: 2.2),
                              decoration: InputDecoration(
                                  hintText:'Bank Name',
                                  border: InputBorder.none,
                                  fillColor: Color(0xfff3f3f4),
                                  filled: true)),
                        ),
                        SizedBox(
                          height: 25,
                        ),
                        /// IBAN
                        Container(
                          width: MediaQuery.of(context).size.width / 1.07,
                          child: TextFormField(
                              controller: _ibanNumber,
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Enter IBAN';
                                }
                                return null;
                              },
                              style: TextStyle(
                                  fontSize: 15,
                                  fontFamily: 'Metropolis',
                                  letterSpacing: 2.2),
                              decoration: InputDecoration(
                                  hintText:'IBAN',
                                  border: InputBorder.none,
                                  fillColor: Color(0xfff3f3f4),
                                  filled: true)),
                        ),
                        SizedBox(
                          height: 25,
                        ),
                        /// Account Number
                        Container(
                          width: MediaQuery.of(context).size.width / 1.07,
                          child: TextFormField(
                              controller: _accountNumber,
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Enter Account Number';
                                }
                                return null;
                              },
                              style: TextStyle(
                                  fontSize: 15,
                                  fontFamily: 'Metropolis',
                                  letterSpacing: 2.2),
                              decoration: InputDecoration(
                                  hintText:'Account Number',
                                  border: InputBorder.none,
                                  fillColor: Color(0xfff3f3f4),
                                  filled: true)),
                        ),
                        SizedBox(
                          height: 25,
                        ),
                        /// Account Holder Name
                        Container(
                          width: MediaQuery.of(context).size.width / 1.07,
                          child: TextFormField(
                              controller: _accountName,
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Enter Account Holder Name';
                                }
                                return null;
                              },
                              style: TextStyle(
                                  fontSize: 15,
                                  fontFamily: 'Metropolis',
                                  letterSpacing: 2.2),
                              decoration: InputDecoration(
                                  hintText:'Account Holder Name',
                                  border: InputBorder.none,
                                  fillColor: Color(0xfff3f3f4),
                                  filled: true)),
                        ),
                        SizedBox(
                          height: 25,
                        ),

                        /// Account Holder Phone Number
                        Container(
                          width: MediaQuery.of(context).size.width / 1.07,
                          child: TextFormField(
                              controller: _accountHolderPhone,
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Enter Account Holder Phone Number';
                                }
                                return null;
                              },
                              style: TextStyle(
                                  fontSize: 15,
                                  fontFamily: 'Metropolis',
                                  letterSpacing: 2.2),
                              decoration: InputDecoration(
                                  hintText:'Account Holder Phone Number',
                                  border: InputBorder.none,
                                  fillColor: Color(0xfff3f3f4),
                                  filled: true)),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        GestureDetector(
                          onTap: ()async{
                            String url = "https://laybull.com/data-collection-policy";
                            if (await canLaunch(url))
                              await launch(url);
                            else
                              Fluttertoast.showToast(
                                  msg: "Failed to open Link",
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.CENTER,
                                  timeInSecForIosWeb: 1,
                                  backgroundColor: Colors.black38,
                                  textColor: Colors.white,
                                  fontSize: 16.0
                              );
                          },
                          child: Padding(
                            padding: const EdgeInsets.only(left:12.0),
                            child: Text('Why we need this?',style: TextStyle(color: Color(0xff5DB3F5,),decoration: TextDecoration.underline)),
                          ),
                        )
                      ],
                    ),
                  )
                  :SizedBox(),
                  SizedBox(
                    height: 50,
                  ),
                  /// SignUp Button
                  InkWell(
                    onTap:isCityFetched? ()async{
                      // print(_sellerFormKey.currentState.validate());
                      if(_formKey.currentState.validate()&&((_sellerFormKey.currentState!=null&&_sellerFormKey.currentState.validate()&&isSeller)||!isSeller)){
                        // print("Control is Here");
                        if(imageFile.isPicked==false){
                          Fluttertoast.showToast(
                              msg: "Select profile image",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.CENTER,
                              timeInSecForIosWeb: 1,
                              backgroundColor: Colors.black38,
                              textColor: Colors.white,
                              fontSize: 16.0
                          );
                          return;
                        }
                        if(selectedCountry==null){
                          Fluttertoast.showToast(
                              msg: "Select your Country",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.CENTER,
                              timeInSecForIosWeb: 1,
                              backgroundColor: Colors.black38,
                              textColor: Colors.white,
                              fontSize: 16.0
                          );
                          return;
                        }
                        pr = MyProgressDialog(context);
                        pr.show();
                        try {
                          await API.Sigup(
                              _first.text,
                              _last.text,
                              _email.text,
                              _password.text,
                              _phone.text,
                              selectedCountry.countryName,
                              _city.text,
                              _address.text,
                            selectedCountry.countryCurrencyLabel,
                            imageFile.image,
                            selectedCountry.countryISOCode,
                            isSeller,
                            isSeller?_bankName.text.trim():"",
                            isSeller?_ibanNumber.text.trim():"",
                            isSeller?_accountNumber.text.trim():"",
                            isSeller?_accountName.text.trim():"",
                            isSeller?_accountHolderPhone.text.trim():"",
                          );
                        }catch(e)
                      {
                        pr.hide();
                      }
                      pr.hide();
                        if(API.complete == 'true'){
                          if(success=='error'){
                            Fluttertoast.showToast(
                                msg: "Email already exists or city invalid",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIosWeb: 1,
                                backgroundColor: Colors.black38,
                                textColor: Colors.white,
                                fontSize: 16.0
                            );
                            // Scaffold.of(context).showSnackBar(SnackBar(content: Text('Something Went Wrong')));
                            // FocusScope.of(context).unfocus();
                          }
                          else{
                            Fluttertoast.showToast(
                                msg: "Registered Successfully",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIosWeb: 1,
                                backgroundColor: Colors.black38,
                                textColor: Colors.white,
                                fontSize: 16.0
                            );
                            await Future.delayed(Duration(milliseconds: 500));
                            Navigator.of(context).pushReplacement(MaterialPageRoute(
                                builder: (BuildContext context) => LoginPage()));
                          }
                          print('data updated');
                        }
                      }
                    }:null,
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: Container(
                        width:isCityFetched? MediaQuery.of(context).size.width / 3.5:MediaQuery.of(context).size.width*.35,
                        height: 45,
                        margin: EdgeInsets.only(bottom: 5),
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          color: isCityFetched?Colors.black:Colors.grey,
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                        child: Text(
                          isCityFetched?'SIGNUP':"Fetching Countries",
                          style: const TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w800,
                              fontStyle: FontStyle.normal,
                              fontFamily: 'aveh',
                              letterSpacing: 0.11,
                              fontSize: 12.0),
                        ),
                      ),
                    ),
                  ),
                  /// Go to Login Page Button
                  Padding(
                    padding: const EdgeInsets.only(top:10.0,bottom: 20),
                    child: InkWell(
                      onTap: ()
                      {
                        Navigator.pop(context);
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            'ALREADY HAVE A ',
                            style: TextStyle(
                                fontSize: 14,
                                color: Color(0XFF878787),
                                letterSpacing: 2,
                                fontWeight: FontWeight.w600,
                                fontFamily: 'Metropolis'),
                          ),

                          InkWell(
                            child: Text(
                              'ACCOUNT',
                              style: TextStyle(
                                  color: Color(0XFF878787),
                                  fontSize: 14,
                                  letterSpacing: 2,
                                  fontWeight: FontWeight.w600,
                                  fontFamily: 'Metropolis'),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}