import 'dart:convert';
import 'dart:ffi';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:Laybull/API/API.dart';
import 'package:Laybull/API/GloabalVariable.dart';
import 'package:Laybull/BottomNavigation/BottomNav.dart';
import 'package:Laybull/ProductPages/BrandList.dart';
import 'package:Laybull/ProductPages/ColorList.dart';
import 'package:Laybull/ProductPages/categories.dart';
import 'package:Laybull/ProductPages/sizelist.dart';
import 'package:Laybull/constant.dart';
// import 'package:Laybull/customDioPkg/lib/dio.dart';
import 'package:dio/dio.dart';
import 'package:Laybull/main.dart';
import 'package:Laybull/myCustomProgressDialog.dart';
import 'package:image_picker/image_picker.dart';
import 'package:progress_dialog/progress_dialog.dart';
class ImageClass{
  File image;
  bool isPicked;
  ImageClass({this.image,this.isPicked});
}
class addProduct extends StatefulWidget {
  @override
  _addProductState createState() => _addProductState();
}

class _addProductState extends State<addProduct> {

  Category selectedCategory;
  List<String>imageFrameTextList = ['Appearance','Front','Left Shot','Right Shot','Back Shot','Box Label','Extras','Other'];
  Brand selectedBrand;
  String selectedCondition;
  SlideSizeClass selectedSize;
  ColorClass selectedColor;
  List listCondition = [
    "New",
    "Used",
  ];
  List<ImageClass>imgeFiles = [
    ImageClass(isPicked: false,image: null),
    ImageClass(isPicked: false,image: null),
    ImageClass(isPicked: false,image: null),
    ImageClass(isPicked: false,image: null),
    ImageClass(isPicked: false,image: null),
    ImageClass(isPicked: false,image: null),
    ImageClass(isPicked: false,image: null),
    ImageClass(isPicked: false,image: null),
  ];

  _imgFromCamera(int index) async {
    File image = await ImagePicker.pickImage(
        source: ImageSource.camera,
    );
    if(image!=null) {
      /// Image Compression
      imgeFiles[index].image = await imageCompression(image);
      imgeFiles[index].isPicked = true;
      setState(() {

      });
    }
  }

  _imgFromGallery(int index) async {
    File image = await  ImagePicker.pickImage(
        source: ImageSource.gallery,
    );
    if(image!=null) {
      /// Image Compression
      imgeFiles[index].image = await imageCompression(image);
      imgeFiles[index].isPicked = true;
      setState(() {

      });
    }
  }

  @override void initState() {
    colorList.forEach((color) {
      color.isSelected = false;
    });
    brands.forEach((brand) {
      brand.isSelected = false;
    });
      slideSizesOfSneakers.forEach((size) {
          size.isSelected = false;
      });
    slideSizesExceptSneakers.forEach((size) {
      size.isSelected = false;
    });
    super.initState();
  }

  double filedHeight = 40.0;
  TextEditingController _prodName = TextEditingController();
  TextEditingController _prodPrice = TextEditingController();

  MyProgressDialog pr;
  @override
  Widget build(BuildContext context) {
    pr = MyProgressDialog(context);
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.only(left: 18,right: 18, top: 18),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Container(
                          padding: EdgeInsets.all(15),
                          child: Image.asset(
                            'assets/ARROW.png',
                          )),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      "SELL",
                      style: headingStyle,
                    ),
                  ],
                ),
                SizedBox(
                  height: 25,
                ),
                Text(
                  "CATEGORY",
                  style: Textprimary,
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  height: filedHeight,
                  padding: EdgeInsets.only(left: 10, right: 10),
                  decoration: BoxDecoration(
                  border: Border.all(color:Colors.grey[400]),
                  borderRadius: BorderRadius.circular(5.0),),
                  child: DropdownButton(
                    isExpanded: true,
                    hint: Text("Select Category"),
                    dropdownColor: Colors.grey[100],
                    icon: Icon(
                      Icons.keyboard_arrow_down_outlined,
                      size: 20,
                      color: Colors.black,
                    ),
                    iconSize: 12,
                    underline: SizedBox(),
                    style: TextStyle(color: Colors.black, fontSize: 15),
                    value: selectedCategory,
                    onChanged: (newValue) {
                      setState(() {
                        selectedCategory = newValue;
                        slideSizesOfSneakers.forEach((element) {element.isSelected = false;});
                        slideSizesExceptSneakers.forEach((element) {element.isSelected = false;});
                        selectedSize = null;
                      });
                    },
                    items: categoryList.map((valueItem) {
                      return DropdownMenuItem(
                        value: valueItem,
                        child: new Row(
                          //  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Text(valueItem.categoryName),
                          ],
                        ),
                      );
                    }).toList(),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  "PRODUCT NAME",
                  style: Textprimary,
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 10,),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      border: Border.all(color: Colors.grey[400])
                  ),
                  child: TextField(
                      style: TextStyle(
                        fontSize: 15,
                      ),
                      controller: _prodName,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        fillColor: Color(0xfff3f3f4),
                        hintText: "Enter Product Name",
                        contentPadding: EdgeInsets.only(left: 10),
                        hintStyle: TextStyle(
                            fontSize: 13,fontWeight: FontWeight.w500,color: Colors.grey
                        ),
                      ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  "BRAND",
                  style: Textprimary,
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  height: filedHeight,
                  padding: EdgeInsets.only(left: 10, right: 10),
                  decoration: BoxDecoration(
                    border: Border.all(color:Colors.grey[400]),
                    borderRadius: BorderRadius.circular(5.0),),
                  child: DropdownButton(
                    isExpanded: true,
                    hint: Text("Select Brand"),
                    dropdownColor: Colors.grey[100],
                    icon: Icon(
                      Icons.keyboard_arrow_down_outlined,
                      size: 20,
                      color: Colors.black,
                    ),
                    iconSize: 12,
                    underline: SizedBox(),
                    style: TextStyle(color: Colors.black, fontSize: 15),
                    value: selectedBrand,
                    onChanged: (brand) {
                      setState(() {
                        print("valueee= ${brand.id}");
                        selectedBrand = brand;
                        selectedBrand.isSelected = true;
                      });
                    },
                    items: brands.map((brand) {
                      return DropdownMenuItem(
                        value: brand,
                        child: new Row(
                          //  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Text(brand.name),
                          ],
                        ),
                      );
                    }).toList(),
                  ),
                ),
                // Container(
                //   padding: EdgeInsets.only(left: 10, right: 10),
                //   decoration: BoxDecoration(
                //     border: Border.all(color:Colors.grey[400]),
                //     borderRadius: BorderRadius.circular(5.0),),
                //   child: DropdownButton(
                //     isExpanded: true,
                //     hint: Text('Select Brand'),
                //     dropdownColor: Colors.grey[100],
                //     icon: Icon(
                //       Icons.keyboard_arrow_down_outlined,
                //       size: 20,
                //       color: Colors.black,
                //     ),
                //     iconSize: 12,
                //     underline: SizedBox(),
                //     style: TextStyle(color: Colors.black, fontSize: 15),
                //     value: selectedBrand,
                //     onChanged: (newValue) {
                //       setState(() {
                //         selectedBrand = newValue;
                //       });
                //     },
                //     items: brands.map((valueItem) {
                //       print("value Item Brand = ${valueItem.name}");
                //       return DropdownMenuItem(
                //         value: valueItem,
                //         child: new Row(
                //           children: <Widget>[
                //             Text("${valueItem.name}"),
                //           ],
                //         ),
                //       );
                //     }).toList(),
                //   ),
                // ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  "CONDITION",
                  style: Textprimary,
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  height: filedHeight,
                  padding: EdgeInsets.only(left: 10, right: 10),
                  decoration: BoxDecoration(
                    border: Border.all(color:Colors.grey[400]),
                    borderRadius: BorderRadius.circular(5.0),),
                  child: DropdownButton(
                    isExpanded: true,
                    hint: Text('Used'),
                    dropdownColor: Colors.grey[100],
                    icon: Icon(
                      Icons.keyboard_arrow_down_outlined,
                      size: 20,
                      color: Colors.black,
                    ),
                    iconSize: 12,
                    underline: SizedBox(),
                    style: TextStyle(color: Colors.black, fontSize: 15),
                    value: selectedCondition,
                    onChanged: (newValue) {
                      setState(() {
                        selectedCondition = newValue;
                      });
                    },
                    items: listCondition.map((valueItem) {
                      return DropdownMenuItem(
                        value: valueItem,
                        child: new Row(
                          //  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Text(valueItem),
                          ],
                        ),
                      );
                    }).toList(),
                  ),
                ),
                SizedBox(
                  height: 25,
                ),
                Text(
                  "SIZES (US)",
                  style: Textprimary,
                ),
                SizedBox(
                  height: 5,
                ),
                selectedCategory!=null&&selectedCategory.categoryName.toUpperCase()=="SNEAKERS"?
                Container(
                  //color: Colors.grey,
                  height: 50,
                  width: MediaQuery.of(context).size.width / 1.13,
                  child: new ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: slideSizesOfSneakers.length,
                      itemBuilder: (_, index) {
                        return Padding(
                            padding: const EdgeInsets.only(
                                top: 5, bottom: 6,right: 5),
                            child: GestureDetector(
                              onTap: (){
                                for(int i=0;i<slideSizesOfSneakers.length;i++){
                                  if(i!=index){
                                    slideSizesOfSneakers[i].isSelected = false;
                                  }
                                }
                                setState(() {
                                  slideSizesOfSneakers[index].isSelected = !slideSizesOfSneakers[index].isSelected;
                                });
                                selectedSize = slideSizesOfSneakers[index];
                              },
                              child: Container(
                                height: 54,
                                width: MediaQuery.of(context).size.width / 7.5,
                                decoration: BoxDecoration(
                                  border: Border.all(color: slideSizesOfSneakers[index].isSelected?Colors.black:Color(0xffD5D5D5)),
                                  borderRadius: BorderRadius.circular(6),
                                ),
                                child: Center(
                                  child: Text(
                                    slideSizesOfSneakers[index].size,
                                    style:
                                    TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                            ));
                      }),
                )
                    : Container(
                  //color: Colors.grey,
                  height: 50,
                  width: MediaQuery.of(context).size.width / 1.13,
                  child: new ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: slideSizesExceptSneakers.length,
                      itemBuilder: (_, index) {
                        return Padding(
                            padding: const EdgeInsets.only(
                                top: 5, bottom: 6,right: 5),
                            child: GestureDetector(
                              onTap: (){
                                for(int i=0;i<slideSizesExceptSneakers.length;i++){
                                  if(i!=index){
                                    slideSizesExceptSneakers[i].isSelected = false;
                                  }
                                }
                                setState(() {
                                  slideSizesExceptSneakers[index].isSelected = !slideSizesExceptSneakers[index].isSelected;
                                });
                                selectedSize = slideSizesExceptSneakers[index];
                              },
                              child: Container(
                                height: 54,
                                width: MediaQuery.of(context).size.width / 7.5,
                                decoration: BoxDecoration(
                                  border: Border.all(color: slideSizesExceptSneakers[index].isSelected?Colors.black:Color(0xffD5D5D5)),
                                  borderRadius: BorderRadius.circular(6),
                                ),
                                child: Center(
                                  child: Text(
                                    slideSizesExceptSneakers[index].size,
                                    style:
                                    TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                            ));
                      }),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  "PRICE",
                  style: Textprimary,
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  // height: filedHeight,
                  padding: EdgeInsets.only(left: 10, right: 10),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      border: Border.all(color: Colors.grey[400])
                  ),
                  child: TextField(
                    textAlignVertical: TextAlignVertical.center,
                      controller: _prodPrice,
                      style: TextStyle(
                        fontSize: 15,
                      ),
                      inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: "Price",
                        fillColor: Colors.grey,
                        contentPadding: EdgeInsets.only(left: 10),
                        hintStyle: TextStyle(
                            fontSize: 13,fontWeight: FontWeight.w500,color: Colors.grey
                        ),
                        suffixIcon: Container(width:30,child: Center(child: Text("AED", style: TextStyle(fontSize: 13,fontWeight: FontWeight.w500),))),
                      )),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  "COLORS",
                  style: Textprimary,
                ),
                SizedBox(
                  height: 5,
                ),
                Container(
                  height: filedHeight,
                  width: MediaQuery.of(context).size.width / 1.13,
                  child: new ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount:  colorList.length,
                      itemBuilder: (_, index) {
                        return Container(
                          margin: EdgeInsets.only(right: 5),
                          child: GestureDetector(
                            onTap: (){
                              for(int i=0;i<colorList.length;i++){
                                if(i!=index){
                                  colorList[i].isSelected = false;
                                }
                              }
                              setState(() {
                                colorList[index].isSelected = !colorList[index].isSelected;
                              });
                              selectedColor = colorList[index];
                            },
                            child: Stack(
                              alignment: Alignment.center,
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                    border: Border.all(color: Colors.black45,width: 0.5,),shape: BoxShape.circle,
                                    color: Color(colorList[index].colorValue),
                                  ),
                                  height: 40,
                                  width: 40,
                                ),
                                colorList[index].isSelected?
                                Icon(Icons.done,color: index==0?Colors.white:Colors.black,size: 30,)
                                    :SizedBox(width: 5,)
                              ],
                            ),
                          ),
                        );
                      }),
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "PHOTOS",
                      style: Textprimary,
                    ),
                    GestureDetector(
                        onTap: ()
                        {
                          showDialog(
                              context: context,
                              builder: (BuildContext context) => Dialog(
                                  backgroundColor: Colors.transparent,
                                  child: PictureDialog()
                              )
                          );
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(12.0),
                          child: Text('Photo Guide',style: TextStyle(fontSize: 13,color: Colors.blue),),
                        )),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                /// Photos Data GridView
                GridView.builder(
                  shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: imageFrameTextList.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 4,childAspectRatio: 0.85),
                    itemBuilder: (context,index){
                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                              height: 75,
                              width: 75,
                              decoration: BoxDecoration(
                                border: Border.all(
                                  width: 1.0,
                                ),
                                borderRadius:
                                BorderRadius.all(Radius.circular(5.0)),
                              ),
                              child: Center(
                                child: GestureDetector(
                                  onLongPress: (){
                                    setState(() {
                                      imgeFiles[index].isPicked = false;
                                      imgeFiles[index].image = null;
                                    });
                                  },
                                  onTap: () {
                                    _showPicker(context,index);
                                  },
                                  child: Container(
                                    child: imgeFiles[index].isPicked
                                        ? Image.file(
                                      imgeFiles[index].image,
                                      width: 100,
                                      height: 100,
                                      fit: BoxFit.fill,
                                    )
                                        : Container(
                                      decoration: BoxDecoration(
                                          color: Color(0xFFE5F3FD),
                                          borderRadius: BorderRadius.circular(0)),
                                      width: 100,
                                      height: 100,
                                      child: Icon(
                                        Icons.camera_alt,
                                        color: Colors.grey[800],
                                      ),
                                    ),
                                  ),
                                ),
                              )
                          ),
                          SizedBox(height: 5,),
                          Text(imageFrameTextList[index], style:TextStyle(fontSize: 11,fontWeight: FontWeight.w600, letterSpacing: 1.1),),
                        ],
                      );
                    }
                ),
                SizedBox(height: 20,),
                /// Add Button
                InkWell(
                  onTap:()async{
                    List<ImageClass> a  = imgeFiles.where((i) => i.isPicked).toList();
                    if((!imgeFiles.last.isPicked&&a.length<6)||(imgeFiles.last.isPicked&&a.length<8)||(imgeFiles[6].isPicked&&a.length<7)) {
                  Fluttertoast.showToast(
                      msg: " Please add all necessary images",
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.CENTER,
                      timeInSecForIosWeb: 1,
                      backgroundColor: Colors.black38,
                      textColor: Colors.white,
                      fontSize: 16.0);
                  return;
                }
                    else if(selectedColor==null||selectedSize==null||selectedCondition==null||selectedBrand==null||selectedCategory==null||_prodPrice.text.trim().isEmpty||_prodName.text.trim().isEmpty){
                      Fluttertoast.showToast(
                          msg: " Please fill Product missing data",
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.CENTER,
                          timeInSecForIosWeb: 1,
                          backgroundColor: Colors.black38,
                          textColor: Colors.white,
                          fontSize: 16.0);
                      return;
                    }
                    else{
                      int vendorId = int.parse(MyApp.sharedPreferences.get('userId').toString());
                      List<File> imgs = [];
                      a.forEach((element) {
                        imgs.add(element.image);
                      });
                      try{
                        pr.show();
                          await API.addProduct(vendorId, selectedCategory.id, _prodName.text.trim(), selectedBrand.id, selectedCondition, selectedSize.size, selectedColor.colorName, _prodPrice.text.trim(), imgs);
                          if(API.complete == 'true'){
                            pr.hide();
                            if(success=='error'){
                              Fluttertoast.showToast(
                                  msg: "Something went wrong",
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.CENTER,
                                  timeInSecForIosWeb: 1,
                                  backgroundColor: Colors.black38,
                                  textColor: Colors.white,
                                  fontSize: 16.0
                              );
                            }
                            else{
                              Fluttertoast.showToast(
                                  msg: "Product Uploaded Successfully",
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.CENTER,
                                  timeInSecForIosWeb: 1,
                                  backgroundColor: Colors.black38,
                                  textColor: Colors.white,
                                  fontSize: 16.0
                              );
                              Navigator.of(context).popUntil((predicate) => predicate.isFirst);
                              Navigator.of(context).pushReplacement(
                                MaterialPageRoute(builder: (context) => BottomNav()),
                              );
                            }
                          }
                          else{
                            pr.hide();
                          }
                        }
                        catch(e){
                        print("Exception = $e");
                          pr.hide();
                          Fluttertoast.showToast(
                              msg: "Something went wrong",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.CENTER,
                              timeInSecForIosWeb: 1,
                              backgroundColor: Colors.black38,
                              textColor: Colors.white,
                              fontSize: 16.0
                          );
                        }

                      pr.hide();
                      }
                    pr.hide();
              },
                  child: Container(
                    width:  MediaQuery.of(context).size.width/1.07,
                    height: 45,
                    margin: EdgeInsets.only(bottom: 5),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      color: Colors.black,
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                    child: Text(
                      'ADD',
                      style: const TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w800,
                          fontStyle: FontStyle.normal,
                          fontFamily: 'MetropolisBold',
                          letterSpacing: 0.11,
                          fontSize: 12.0),
                    ),
                  ),
                ),
                SizedBox(height: 30,),

              ],
            ),
          ),
        ),
      ),
    );
  }


  void _showPicker(context,int index) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text('Photo Library'),
                      onTap: () {
                        _imgFromGallery(index);
                        Navigator.of(context).pop();
                      }),
                  new ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text('Camera'),
                    onTap: () {
                      _imgFromCamera(index);
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        }
    );
  }
}

class PictureDialog extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return _PictureDialog();
  }
}
class _PictureDialog extends State<PictureDialog> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: [
          Image.asset('assets/PhotoguideLaybull.jpg',fit: BoxFit.contain,),
          Align(
            alignment: Alignment.topRight,
            child: IconButton(
              icon: Icon(Icons.cancel,color: Colors.black,),
              onPressed: (){
                Navigator.pop(context);
              },
            ),
          ),
        ],
      ),
    );
  }
}

