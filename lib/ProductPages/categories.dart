import 'package:Laybull/Model/Product.dart';

class Category{
  int id;
  String categoryName;
  String categoryImage;
  // List<Product> categoryProducts;
  Category({this.id,this.categoryImage,this.categoryName,});

  factory Category.fromJson(Map<String,dynamic>json){
    return Category(
      id: json["id"],
      categoryName: json["name"],
      categoryImage: json["picture"],
      // categoryProducts: List<Product>.from(json["product"].map((product) => Product.fromJson(product)).toList()),
    );
  }
}
List<Category> categoryList= [];
