class Brand{
  String name;
  String image;
  bool isSelected;
  int id;

  Brand({this.name,this.image,this.isSelected,this.id});
}
List<Brand> brands = [
  Brand(
    id: 1,
    name: "Adidas",
    isSelected: false,
    image: "assets/adidas.png",
  ),
  Brand(
    id: 2,
    name: "Anti Social Social Club",
    isSelected: false,
    image: "assets/anti social.png",
  ),
  Brand(
    id: 3,
    name: "Bape",
    isSelected: false,
    image: "assets/bape.png",
  ),
  Brand(
    id: 4,
    name: "Bearbrick",
    isSelected: false,
    image: "assets/Bear Brick.jpg",
  ),
  Brand(
    id: 5,
    name: "Travis Scott",
    isSelected: false,
    image: "assets/Cactus Jack.png",
  ),
  Brand(
    id: 6,
    name: "Dior",
    isSelected: false,
    image: "assets/DIOR.png",
  ),
  Brand(
    id: 7,
    name: "Fear of God",
    isSelected: false,
    image: "assets/gucci.png",
  ),
  Brand(
    id: 8,
    name: "Jordan",
    isSelected: false,
    image: "assets/jumpman.png",
  ),
  Brand(
    id: 9,
    name: "Kaws",
    isSelected: false,
    image: "assets/kaws.png",
  ),
  Brand(
    id: 10,
    name: "Sacai",
    isSelected: false,
    image: "assets/logo-sacai.png",
  ),
  Brand(
    id: 11,
    name: "Louis Vuitton",
    isSelected: false,
    image: "assets/louis-vuitton.png",
  ),
  Brand(
    id: 12,
    name: "Nike",
    isSelected: false,
    image: "assets/nike.png",
  ),
  Brand(
    id: 13,
    name: "Vlone",
    isSelected: false,
    image: "assets/palace.png",
  ),
  Brand(
    id: 14,
    name: "Supreme",
    isSelected: false,
    image: "assets/supreme.png",
  ),
  Brand(
    id: 15,
    name: "Yeezy",
    isSelected: false,
    image: "assets/yeezy.png",
  ),
  Brand(
    id: 16,
    name: "Other",
    isSelected: false,
    image: "",
  ),
];