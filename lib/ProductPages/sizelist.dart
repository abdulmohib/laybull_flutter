import 'package:flutter/material.dart';

class SlideSizeClass {
  String size;
  bool isSelected;



  SlideSizeClass({
    @required this.size,
    this.isSelected,
  });
}
List<SlideSizeClass> slideSizesOfSneakers = [
  SlideSizeClass(
      size: '4',
      isSelected: false
  ), SlideSizeClass(
      size: '4.5',
      isSelected: false
  ),SlideSizeClass(
      size: '5',
      isSelected: false
  ), SlideSizeClass(
      size: '5.5',
      isSelected: false
  ), SlideSizeClass(
      size: '6',
      isSelected: false
  ), SlideSizeClass(
      size: '6.5',
      isSelected: false
  ), SlideSizeClass(
      size: '7',
      isSelected: false
  ), SlideSizeClass(
      size: '7.5',
      isSelected: false
  ),SlideSizeClass(
      size: '8',
      isSelected: false
  ),SlideSizeClass(
      size: '8.5',
      isSelected: false
  ),SlideSizeClass(
      size: '9',
      isSelected: false
  ),SlideSizeClass(
      size: '9.5',
      isSelected: false
  ),SlideSizeClass(
      size: '10',
      isSelected: false
  ),SlideSizeClass(
      size: '11',
      isSelected: false
  ),SlideSizeClass(
      size: '12',
      isSelected: false
  ),SlideSizeClass(
      size: '13',
      isSelected: false
  ),SlideSizeClass(
      size: '14',
      isSelected: false
  ),SlideSizeClass(
      size: '15',
      isSelected: false
  ),
];
List<SlideSizeClass> slideSizesExceptSneakers = [
  SlideSizeClass(
    size: 'XS',
    isSelected: false
  ), SlideSizeClass(
    size: 'S',
      isSelected: false
  ), SlideSizeClass(
    size: 'M',
      isSelected: false
  ), SlideSizeClass(
    size: 'L',
      isSelected: false
  ), SlideSizeClass(
    size: 'XL',
      isSelected: false
  ), SlideSizeClass(
    size: 'XXL',
      isSelected: false
  ),
];