import 'package:Laybull/customDioPkg/lib/dio.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'API/BaseUrl.dart';
import 'API/GloabalVariable.dart';
import 'Model/Product.dart';
import 'Model/favourite.dart';
import 'constant.dart';
import 'main.dart';
class PopularProduct extends StatefulWidget {
  final bool isFromNavBar;
  PopularProduct({@required this.isFromNavBar});
  @override
  _PopularProductState createState() => _PopularProductState();
}

class _PopularProductState extends State<PopularProduct> {

  var click = false;
  List<Product> wishlistshow= [];
  @override void initState() {
    getFavouriteProducts();
    super.initState();
  }

  bool isFetching = true;
  getFavouriteProducts()async{
    var url = BaseUrl + "api/list-like";
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "Bearer " + MyApp.sharedPreferences.getString("access_token").toString();
    try{
      await dio
          .get(url,)
          .then((response) {
        if (response.statusCode == 200) {
          for(int i = 0;i<response.data["products"].length;i++){
            wishlistshow.add(
                Product(
                  id: response.data["products"][i]['id'],
                  category_id: response.data["products"][i]['category_id'],
                  brand_id: response.data["products"][i]['brand_id'],
                  vendor_id: response.data["products"][i]['vendor_id'],
                  type: response.data["products"][i]['type'],
                  status: response.data["products"][i]['status'],
                  popular: response.data["products"][i]['popular'],
                  name: response.data["products"][i]['name'],
                  price: response.data["products"][i]['price'],
                  feature_image: response.data["products"][i]["vendorName"]!=null?response.data["products"][i]['feature_image']:"uploads/productImages/${response.data["products"][i]['feature_image']}",
                  images: response.data["products"][i]['images']!=null?response.data["products"][i]['images']:[],
                  color: response.data["products"][i]['color'],
                  size: response.data["products"][i]["size"],
                  condition: response.data["products"][i]["condition"],
                  discount: response.data["products"][i]['discount'],
                  original_price: response.data["products"][i]['original_price'],
                  tags: response.data["products"][i]['tags'],
                  pusblish: response.data["products"][i]['pusblish'],
                  short_desc: response.data["products"][i]['short_desc']??"",
                  fulldesc: response.data["products"][i]['fulldesc']??"",
                  payment: response.data["products"][i]['payment'],
                  warrenty: response.data["products"][i]['warrenty'],
                  isFavourite: true,
                  bid_price: response.data["products"][i]["bid_price"]??"",
                  updated_at: response.data["products"][i]["updated_at"]??"",
                  vendorName: response.data["products"][i]["vendorName"]!=null?response.data["products"][i]["vendorName"]:response.data["products"][i]["user"]!=null?response.data["products"][i]["user"]["name"]:"",
                )
            );
          }
          if(mounted)
          setState(() {
            isFetching = false;
          });
        } else {
          if(mounted)
          setState(() {
            isFetching = false;
          });
          print("Exception in Api = $url");
          Fluttertoast.showToast(
              msg: "Failed to get data Please Try Again!",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.black38,
              textColor: Colors.white,
              fontSize: 16.0
          );
        }
      });
    }catch(e){
      if(mounted)
     setState(() {
       isFetching = false;
     });
      print("Exception = $e");
      Fluttertoast.showToast(
          msg: "Failed to connect with Server Please Try Again!",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.black38,
          textColor: Colors.white,
          fontSize: 16.0
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child:isFetching?
        Container(color:Colors.white,height: MediaQuery.of(context).size.height,child: Center(child:Image.asset(
          "assets/Larg-Size.gif",
          height: 125.0,
          width: 125.0,
        ),),):
        ListView(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 30,bottom: 18),
              child: Row(
                children: [
                  widget.isFromNavBar?
                  SizedBox():
                  Container(
                      height: 20,
                      width: 20,
                      child: Image.asset('assets/ARROW.png',)),
                  SizedBox(width: widget.isFromNavBar?10:15,),
                  Text(
                    'Favourites'.toUpperCase(),
                    style: headingStyle,
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10),
              child: Container(
                width: MediaQuery.of(context).size.width,
                child: wishlistshow.isEmpty?Container(height:MediaQuery.of(context).size.height*.4, child: Center(child: Text("Nothing To Show")),):
                productGridView(product: wishlistshow,onFavoutireButtonTapped: (){setState(() {wishlistshow.removeWhere((element) => element.isFavourite==false);});}),
                // GridView.builder(
                //   gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                //       maxCrossAxisExtent: 200,
                //       // childAspectRatio: (itemWidth / itemHeight),
                //       crossAxisSpacing: 20,
                //       mainAxisSpacing: 20),
                //   itemCount: wishlistshow.length,
                //   itemBuilder: (BuildContext ctx, index) {
                //     return Container(
                //       child: Column(
                //         mainAxisAlignment: MainAxisAlignment.start,
                //         crossAxisAlignment: CrossAxisAlignment.start,
                //         children: [
                //           Stack(
                //             children: [
                //               Column(
                //                 children: [
                //                   Container(
                //                     height: 100,
                //                     width: MediaQuery.of(context)
                //                         .size
                //                         .width /
                //                         2.4,
                //                     decoration: BoxDecoration(
                //                       color: Greycolor,
                //                       borderRadius:
                //                       BorderRadius.circular(10),
                //                     ),
                //           // BaseUrl+wishlistshow[index].image ??
                //                     child: Image.network( BaseUrl+wishlistshow[index].image),
                //                   ),
                //                 ],
                //               ),
                //               InkWell(
                //                 // onTap: (){
                //                 //   if(!click){
                //                 //     click = true;
                //                 //     //var contain = wishlist.where((element) => element.id == popularproduct[index].id.toString());
                //                 //     //if(contain.isNotEmpty){
                //                 //     wishlist.removeWhere((item) => item.id == wishlistshow[index].id.toString());
                //                 //      //wishlist.remove(Favourite(id: popularproduct[index].id.toString()));
                //                 //
                //                 //     final String encodedData =
                //                 //     Favourite.encodeMusics(wishlist);
                //                 //     MyApp.sharedPreferences.setString("wishlist", encodedData);
                //                 //     wishlistshow[index].isFavourite = false;
                //                 //     wishlistshow.removeWhere((item) => item.id == wishlistshow[index].id);
                //                 //     // wishlistshow.remove(Product(color: popularproduct[index].color ,brand: popularproduct[index].brand,discount: popularproduct[index].discount,id: popularproduct[index].id,
                //                 //     //     name: popularproduct[index].name,prices: popularproduct[index].prices,
                //                 //     //     image: popularproduct[index].image ,popular: popularproduct[index].popular ,type: popularproduct[index].type));
                //                 //     final String encodedData1 =
                //                 //     Product.encodeMusics(wishlistshow);
                //                 //     MyApp.sharedPreferences.setString("wishlistshow", encodedData1);
                //                 //
                //                 //     //}
                //                 //
                //                 //     setState(() {
                //                 //
                //                 //     });
                //                 //
                //                 //     click = false;
                //                 //   }
                //                 //
                //                 // },
                //                 child: Padding(
                //                   padding: const EdgeInsets.only(
                //                       left: 120.0, top: 77),
                //                   child: Container(
                //                     height: 30,
                //                     width: 30,
                //                     decoration: BoxDecoration(
                //                       color: Colors.white,
                //                       borderRadius:
                //                       BorderRadius.circular(20),
                //                     ),
                //                     child: Image.asset(
                //                         'assets/fire.png'),
                //                   ),
                //                 ),
                //               ),
                //             ],
                //           ),
                //           Padding(
                //             padding: const EdgeInsets.only(top: 1.0),
                //             child: Column(
                //               mainAxisAlignment: MainAxisAlignment.start,
                //               crossAxisAlignment:
                //               CrossAxisAlignment.start,
                //               children: [
                //                 Container(
                //                   width:MediaQuery.of(context).size.width/5,
                //                   child: Text(
                //                     wishlistshow[index].name.toUpperCase(),
                //                     style: listtext1,
                //                     maxLines: 1,
                //                   ),
                //                 ),
                //                 Text(wishlistshow[index].prices.toUpperCase(),
                //                     style:  listtext2),
                //                 Text(wishlistshow[index].type==0?"Used".toUpperCase():"New".toUpperCase(),
                //                     style:  listtext3),
                //               ],
                //             ),
                //           )
                //         ],
                //       ),
                //     );
                //   },
                // ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
