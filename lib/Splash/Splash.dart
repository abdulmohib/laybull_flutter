    import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:Laybull/Registration/Login.dart';
import 'package:Laybull/Splash/Login&Registrationpage.dart';

class IntroScreen extends StatefulWidget {
  @override
  _IntroScreenState createState() => _IntroScreenState();
}

class _IntroScreenState extends State<IntroScreen> {
  SwiperController _controller = SwiperController();

  int _currentIndex = 0;
  final List<String> titles = [
    "WELCOME TO LAYBULL",
    "Explore Items Uploaded Daily",
    "Guaranteed Authentic ",
    "Our Locations",
  ];
  final List<String> subtitles = [
    "The Middle East's marketplace for buying & selling the most sought-after fashion items",
    "Shop the latest and rarest items new & used. ",
    "All items are sent to Laybull's authentication team before being shipped to you",
    "United Arab Emirates, Saudi Arabia, Oman, India, Bahrain, Kuwait, Lebanon, Jordan & Israel",
  ];
  final List<String> image=
  [
    'Page1.png',
    'page2.png',
    'page3.png',
    'page4.png',
  ];
  final List<Color> colors = [
    Colors.white,
    Colors.white,
    Colors.white,
    Colors.white,
  ];

  final _scaffoldKey = new GlobalKey<ScaffoldState>();


  @override
  void initState() {
    super.initState();
  }

  bool isAutoPlay = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Swiper(
        loop: false,
        autoplay: isAutoPlay,
        index: _currentIndex,
        onIndexChanged: (index) async{
          setState(() {
            _currentIndex = index;
          });
          if(index>=titles.length-1){
            setState(() {
              isAutoPlay = false;
            });
            await Future.delayed(Duration(seconds: 1));
            return Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (BuildContext context) => LoginPage()));
          }
        },
        controller: _controller,
        pagination: SwiperPagination(
          builder: DotSwiperPaginationBuilder(
            activeColor: Colors.black,
            activeSize: 8.0,
          ),
        ),
        itemCount: 4,
        itemBuilder: (context, index) {
          return IntroItem(
            title: titles[index],
            subtitle: subtitles[index],
            bg: colors[index],
            imageUrl: "assets/"+image[index],
          );
        },
      ),
    );
  }
}

class IntroItem extends StatelessWidget {
  final String title;
  final String subtitle;
  final Color bg;
  final String imageUrl;

  const IntroItem(
      {Key key, @required this.title, this.subtitle, this.bg, this.imageUrl})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: bg ?? Theme.of(context).primaryColor,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            child: Image.asset(imageUrl,fit: BoxFit.cover,),
          ),
          Container(
            alignment: Alignment.center,
            width: MediaQuery.of(context).size.width/1.8,
            child: Padding(
              padding: const EdgeInsets.only(left: 8.0, right: 8.0),
              child: Center(
                child: Text(
                  title,
                  textAlign:TextAlign.center,
                  style: TextStyle(
                      fontWeight: FontWeight.w800,
                      wordSpacing: 1,
                      fontSize: 26.0,
                      color: Colors.black87),
                ),
              ),
            ),
          ),
          if (subtitle != null) ...[
            const SizedBox(height: 5.0),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(
                child: Text(
                  subtitle,
                  style: TextStyle(color: Colors.black54, fontSize: 18.0),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ],
        ],
      ),
    );
  }
}