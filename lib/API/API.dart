import 'dart:io';
import 'package:Laybull/searchonTap.dart';
import 'package:dio/dio.dart';
import 'package:Laybull/Model/Product.dart';
import 'package:Laybull/Model/bidModel.dart';
import 'package:Laybull/Model/countryModel.dart';
import 'package:Laybull/ProductPages/categories.dart';
import '../main.dart';
import 'BaseUrl.dart';
import 'GloabalVariable.dart';

class API {
  static var complete = "false";
  static var error = "";
  static double rating = 0.0;
  static var paymentlink;

  static Future login(var username, var password,String fcmToken) async {
    complete = "false";
    var url = BaseUrl + "api/login";
    Dio dio = new Dio();
    FormData formData = new FormData.fromMap({
      'email': username,
      'password': password,
      "FCMToken": fcmToken,
    });


    await dio.post(
        url,
        data: formData,
        options: Options(
        followRedirects: false,
        validateStatus: (status) {
          return status < 500;
        })
    ).then((response)async {
      Map<String, dynamic> data = response.data;

      if (response.data["status"]=="True") {
        success = data['token'];
        MyApp.sharedPreferences.setString('access_token', data['token'].toString());
        MyApp.sharedPreferences.setString('userEmail', username.toString());
        MyApp.sharedPreferences.setString('userPassword', password.toString());
        MyApp.sharedPreferences.setString('userId', data['user']["id"].toString());
        MyApp.sharedPreferences.setString('isSeller', data['user']["is_seller"].toString());
        MyApp.sharedPreferences.setString('currency', data['user']["currency"].toString());
        MyApp.sharedPreferences.setString('country', data['user']["detail"]["country"].toString());
        /// Do this in Parallel
        await API.getCategory();
        // await API.getProduct();
        API.fetchCountry();
        await API.getPopularProduct();
        // await API.UserProduct();
        // // API.getBrands();
        await API.getRecivedBids();
        await API.getSentBids();
        // // API.getUserdetail();
        complete = "true";
        //success = "true";
      } else {
        complete = "false";
      }

    }).catchError((e) {
      complete = "true";
      print(url);
      print("error $e");
      success = "error";
    });
  }

  static Future Sigup(
      String fname,
      String lname,
      String email,
      String password,
      String phone,
      String country,
      String city,
      String address,
      String currencyLabel,
      File image,
      String countryISOCode,
      bool isSeller,
      String bankName,
      String Iban,
      String bankNumber,
      String bankAccountName,
      String bankAccountPhone,
      ) async {
    complete = "false";
    var url = BaseUrl + "api/signup";
    Dio dio = new Dio();
    FormData formData = new FormData.fromMap({
      'fname': fname.trim(),
      'lname': lname.trim(),
      'email': email.trim(),
      'password': password.trim(),
      'phone': phone.trim(),
      'address': address.trim(),
      'city': city.trim(),
      'country': country.toString(),
      "currency": currencyLabel,
      "profile_image": await MultipartFile.fromFile(image.path, filename: image.path.split('/').last),
      "country_code": countryISOCode,
      "is_seller":isSeller?1:0,
      "account_bank_name":bankName,
      "IBAN_Number": Iban,
      "account_number":bankNumber,
      "account_holder_name":bankAccountName,
      "account_phone_number":bankAccountPhone,
    });
    await dio.post(url,
      data: formData,
    ).then((response) {

      Map<String, dynamic> data = response.data;
      if (response.data["status"]=="True") {
        complete = "true";
        success = "true";
      } else {
        complete = "false";
      }
    }).catchError((e) {
      complete = "true";
      if (e.error is SocketException) {
        success = "error";
      } else {
        success = "error";
      }
    });
  }

  static Future getCategory() async {
    categoryList.clear();
    var url = BaseUrl + "api/allCategories";
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "Bearer " +
        MyApp.sharedPreferences.getString("access_token").toString();
    await dio
        .get(url,)
        .then((response) {
      if (response.data["status"] == "True") {
        categoryList = List<Category>.from(response.data["categories"].map((product) => Category.fromJson(product)).toList());
        } else {
        success = "true";
        print("Exception in Api = $url");
        throw Exception('Failed to Fetch Vendors');
      }
    });
  }

  fetchUserProfile()async{
    var url = BaseUrl + "api/List-of-countries";
    complete = "false";
    Dio dio = new Dio();
    // dio.options.headers["Authorization"] = "Bearer " +
    //     MyApp.sharedPreferences.getString("access_token").toString();
    //
    try{
      await dio
          .get(url,)
          .then((response) {
        // // print("Response Data of List Countries = ${response.data}");
        if (response.statusCode == 200) {

        } else {
          success = "true";
          // // print("Exception in Api = $url");
        }
      });
    }catch(e){
      // print(e);
      success = "true";
    }
  }

  static List<Product>categoryProductsList = List<Product>();
  static Future categoryProduct(int param) async {
    categoryProductsList.clear();
    // print("param = $param");
    complete = "false";
    var url = BaseUrl + "api/categoryWiseProducts";
    Dio dio = new Dio();
    dio.options.queryParameters= {"category_id":param};
    dio.options.headers["Authorization"] = "Bearer " +
        MyApp.sharedPreferences.getString("access_token").toString();
   await dio.get(url, ).then((response) {
      if (response.data["status"] == "True") {
        var data = response.data["products"];
        List<Product> list = List<Product>.from(data.map((product) => Product.fromJson(product)).toList());
        if(list.isNotEmpty)
        categoryProductsList = list;
        return list;
      } else {
        success = "true";
        print("Exception in Api = $url");
        throw Exception('Failed to Fetch Vendors');
      }
    });
  }

  static Future Search(String name) async {
    // print("Name to be searched = $name");
    searchs.clear();
    complete = "false";
    var url = BaseUrl + "api/searchProduct";
    Dio dio = new Dio();
    // print(MyApp.sharedPreferences.getString("access_token"));
    dio.options.headers["Authorization"] = "Bearer "+MyApp.sharedPreferences.getString("access_token").toString();
    dio.options.queryParameters = {"search":name};
    // print(dio);
    await dio
        .get(
      "$url",
        queryParameters: {'search': name,}
    ).then((response) {
      // print("getSearched Product Api Result = ${response.data}");
      if (response.data["status"] == "True") {
        var data = response.data;
        if (data != []) {
          searchs = List<Product>.from(data["products"].map((prod) => Product.fromJson(prod)).toList());
        }
        complete = "true";
      } else {
        complete = "true";
        // print(response.statusCode);
      }
      //// print(response.data);
    }).catchError((e) {
      // print("$url");
      // print("error $e");
      complete = "true";
    });
  }
  static Future SearchWithFilters(String brandId,String colorName,String categoryId,String size,String minValue,String maxValue) async {

    searchs.clear();
    complete = "false";
    var url = BaseUrl + "api/searchFilterProduct";
    Dio dio = new Dio();
    // print("brand_id = $brandId");
    // print("Color = $colorName");
    // print("Cat_id = $categoryId");
    // print("size  = $size");
    // print("minValue  = $minValue");
    // print("max  = $maxValue");
    dio.options.headers["Authorization"] = "Bearer "+MyApp.sharedPreferences.getString("access_token").toString();
    dio.options.queryParameters = {
      "brand_id": brandId,
    "color": colorName,
    "category_id": categoryId.toString(),
    "size": size,
    "min_price": minValue,
    "max_price": maxValue,
    };
    await dio
        .get(
      "$url",
      queryParameters: {
        "brand_id":brandId,
        "color":colorName,
        "category_id":categoryId.toString(),
        "size":size,
        "min_price":minValue,
        "max_price":maxValue,
      }
    )
        .then((response) {
      print("getSearched Product Api Result = ${response}");
      if (response.data["status"] == "True") {
        var data = response.data;
        if (data != []) {
          searchs = List<Product>.from(data["products"].map((prod) => Product.fromJson(prod)).toList());
          print("Searches Length = ${searchs.length}");
        }
        complete = "true";
      } else {
        complete = "true";
      }
      //// print(response.data);
    }).catchError((e) {
      // print("$url");
      // print("error $e");
      complete = "true";
    });
  }

  // static Future getProduct() async {
  //   product.clear();
  //   complete = "false";
  //   var url = BaseUrl + "api/getProducts";
  //   Dio dio = new Dio();
  //   dio.options.headers["Authorization"] = "Bearer " +
  //       MyApp.sharedPreferences.getString("access_token").toString();
  //   await dio
  //       .get(
  //     url,
  //   )
  //       .then((response) {
  //     // print("getProducts Api Result = ${response}");
  //     if (response.data["status"] == "True") {
  //       var data = response.data;
  //       if (data != []) {
  //         product = List<Product>.from(data["products"].map((prod) => Product.fromJson(prod)).toList());
  //       }
  //       complete = "true";
  //       success = "true";
  //     } else {
  //       complete = "false";
  //       // print(response.statusCode);
  //     }
  //   }).catchError((e) {
  //     complete = "true";
  //     // print(url);
  //     // print("error $e");
  //   });
  // }

  static Future getPopularProduct() async {
    popularproduct.clear();
    complete = "false";
    var url = BaseUrl + "api/popularProducts";
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "Bearer " +
        MyApp.sharedPreferences.getString("access_token").toString();
    await dio
        .get(
      url,
    )
        .then((response) {
      if (response.data["status"] == "True") {
        var data = response.data;
        popularproduct = List<Product>.from(data["products"].map((prod) => Product.fromJson(prod)).toList());
        complete = "true";
      } else {
        complete = "false";
       }
    }).catchError((e) {
      complete = "true";
      print("Exception in Api = $url");
      print("error $e");
    });
  }

  static Future addProduct( int vendorId,int cat,String prodName, int brandId, String condition,String size,String color, String price,List<File> images) async {

    int i = 0;
    List<MultipartFile> multipartImagesList = List<MultipartFile>();
    while(i<images.length){
      MultipartFile ff = await MultipartFile.fromFile(images[i].path, filename: images[i].path.split('/').last);
      multipartImagesList.add(ff);
      i++;
    }

    // complete = "false";
    var url = BaseUrl + "api/addProduct";
    Dio dio = new Dio();

    // String myJson = json.encode(xyz);
    FormData formData = new FormData.fromMap({
      "category_id": "$cat",
      'name': '$prodName',
      'brand_id': '$brandId',
      "condition": condition,
      "size": size,
      'price': '$price',
      'color': color,
      "vendor_id": vendorId.toString(),
      "feature_image":await MultipartFile.fromFile(images[0].path,filename: images[0].path.split('/').last),
      "images": multipartImagesList,
      // "images": [0,1,2,3,6],
    });
    dio.options.headers["Authorization"] = "Bearer " + MyApp.sharedPreferences.getString("access_token").toString();
    await dio.post(url,
      data: formData,
    )
        .then((response) {
      if (response.data["status"] == "True") {
        var data = response.data;
        complete = "true";
        complete = "true";
        success = "true";
      } else {
        complete = "false";
        success = "error";
      }
     }).catchError((e) {
      complete = "true";
      success = "error";
     });
  }
  static Future setProduct( int vendorId,int cat,String prodName, int brandId, String condition,String size,String color, String price,List<File> images,int productId) async {
    complete = "false";
    var url = BaseUrl + "api/editProduct";
    Dio dio = new Dio();
    int i=0;
    List abc = [];
    while(i<images.length){
      abc.add(await MultipartFile.fromFile(images[i].path,filename: images[i].path.split('/').last));
      i++;
    }
    FormData formData = new FormData.fromMap({
      "category_id": "$cat",
      'name': '$prodName',
      'brand_id': '$brandId',
      "condition": condition,
      "size": size,
      'price': '$price',
      'color': color,
      "vendor_id": vendorId.toString(),
      "feature_image":await MultipartFile.fromFile(images[0].path,filename: images[0].path.split('/').last),
      "images": abc,
      "product_id":"$productId",
    });
    dio.options.headers["Authorization"] = "Bearer " + MyApp.sharedPreferences.getString("access_token").toString();
    await dio.post(url,
      data: formData,
    )
        .then((response) {
      if (response.data["status"] == "True") {
        var data = response.data;
        complete = "true";
        success = "true";
      } else {
        complete = "false";
        success = "error";
      }
      //print(response.data);
    }).catchError((e) {
      complete = "true";
      success = "error";
      print(url);
      print("error $e");
    });
  }

  static Future UserProduct() async {
    userproduct.clear();
    var url = BaseUrl + "api/userProducts";
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "Bearer " +
        MyApp.sharedPreferences.getString("access_token").toString();
    //dio.options.headers["content"] = "token"+success.toString();
    await dio
        .get(
      url,
      //     options: Options(
      //
      //     headers: {
      //
      //       "Authorization":"Bearer"+success.toString()}
      // )
    )
        .then((response) {
      if (response.statusCode == 200) {

        print("userProduct Result = ${response}");
        var data = response.data;
        if (data != []) {
          for (var data in data) {
            userproduct.add(Product(
                color: data['color'],
                brand_id: data['brand'],
                discount: data['discount'],
                id: data['id'],
                name: data['name'],
                price: data['price'],
                feature_image: data['feature_image'],
                popular: data['popular'],
                type: data['type']));
          }
        }
      } else {
        // success = "true";
        throw Exception('Failed to Fetch Vendors');
      }
    });
  }

  // static Future WishList() async
  // {
  //
  //   userproduct.clear();
  //   var url=BaseUrl+"api/wishlist";
  //   Dio dio = new Dio();
  //   dio.options.headers["authorization"] = "Bearer "+MyApp.sharedPreferences.getString("access_token").toString();
  //   //dio.options.headers["content"] = "token"+success.toString();
  //   await dio.get(url,).then((response) {
  //
  //     if(response.statusCode == 200){
  //       //vendorList.clear();
  //       //print(response.data.toString());
  //       var data = response.data;
  //       if(data != []) {
  //         for (var data in data) {
  //           wishlist.add(Favourite(color: data['color'],
  //               brand: data['brand'],
  //               discount: data['discount'],
  //               id: data['id'],
  //               name: data['name'],
  //               prices: data['price'],
  //               image: data['feature_image'],
  //               popular: data['popular'],
  //               type: data['type']));
  //         }
  //       }
  //     }
  //     else{
  //       // success = "true";
  //       throw Exception('Failed to Fetch Vendors');
  //     }
  //
  //   });
  //
  // }

  // static Future getBrands() async {
  //   brands.clear();
  //   var url = BaseUrl + "api/brands";
  //   Dio dio = new Dio();
  //   await dio
  //       .get(url,
  //           options: Options(
  //               // headers: {"token_type":"Bear","access_token":success.toString()}
  //               ))
  //       .then((response) {
  //     if (response.statusCode == 200) {
  //       //vendorList.clear();
  //       //print(response.data.toString());
  //       var data = response.data;
  //       if (data != []) {
  //         for (var da in data) {
  //           brands.add(Brand(
  //               name: da['name'],
  //               id: da['id'],
  //               picture: da['picture'],
  //               description: da['description'],
  //             isSelected: false,
  //           )
  //           );
  //         }
  //       }
  //     } else {
  //       //   success = "true";
  //       throw Exception('Failed to Fetch Vendors');
  //     }
  //   });
  // }

  static Future getRecivedBids() async {
    var url = BaseUrl + "api/getReceivedOffers";
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "Bearer " +
        MyApp.sharedPreferences.getString("access_token").toString();
    await dio.get(url,).then((response) {
      if (response.data["status"]=="True") {
        var data = response.data;
        if (data != []) {

          receivedOrders =  List<ProductBid>.from(data["bids"].map((prod) => ProductBid.fromJson(prod)).toList());
          receivedOrders.removeWhere((element) => element.product==null);
        }
      } else {
        //   success = "true";
        print("Exception in Api = $url");
        throw Exception('Failed to Fetch Vendors');
      }
    });
  }

  static Future fetchCountry()async{
    var url = BaseUrl + "api/list-of-countries";
    Dio dio = new Dio();
    // dio.options.headers["authorization"] = "Bearer " + MyApp.sharedPreferences.getString("access_token").toString();
    await dio
        .get(url,)
        .then((response) {
      if (response.statusCode==200) {
        listCountry = List<CountryModel>.from(response.data["data"]["country"].map((prod) => CountryModel.fromJson(prod)).toList());
        complete = "true";
        // print("List of COuntries = ${listCountry.length}");
        success = "true";
      } else {
        success = "true";
        print("Exception in Api = $url");
        throw Exception('Failed to Fetch Vendors');
      }
    });
  }


  static Future getSentBids() async {
    var url = BaseUrl + "api/getSendOffersList";
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "Bearer " +
        MyApp.sharedPreferences.getString("access_token").toString();
    await dio.get(url,).then((response) {
      // print("Response = ${response.data}");
      if (response.data["status"]=="True") {
        var data = response.data;
        // print("Bids = ${data["bids"]}");
        if (data != []) {
          sentOffer =  List<ProductBid>.from(data["bids"].map((prod) => ProductBid.fromJson(prod)).toList());
          sentOffer.removeWhere((element) => element.product==null);
        }
      } else {
        //   success = "true";
        print("Exception in Api = $url");
        throw Exception('Failed to Fetch Vendors');
      }
    }).catchError((e){
      print("Exception in Api = $url");
      print("Exception = $e");
    });
  }

  // static Future getUserdetail() async {
  //   var url = BaseUrl + "api/user";
  //   try{
  //     Dio dio = new Dio();
  //     dio.options.headers["Authorization"] = "Bearer " +
  //         MyApp.sharedPreferences.getString("access_token").toString();
  //     await dio
  //         .get(url,
  //         options: Options(
  //           //headers: {"token_type":"Bear","access_token":success.toString()}
  //         ))
  //         .then((response) {
  //       if (response.statusCode == 200) {
  //         //vendorList.clear();
  //         //print(response.data.toString());
  //         var data = response.data;
  //         MyApp.sharedPreferences.setString('user_name', data['name'].toString());
  //         MyApp.sharedPreferences.setString('user_id', data['id'].toString());
  //         MyApp.sharedPreferences.setString('user_email', data['email'].toString());
  //       } else {
  //
  //         //   success = "true";
  //         throw Exception('Failed to Fetch Vendors');
  //       }
  //     });
  //   }catch(e){
  //     print("Exception in Api = $url");
  //     print("$e");
  //   }
  // }

  static Future rateUser(int userId,double rate) async {
    rating = 0.0;
    var url = BaseUrl + "api/add-rating";
    complete = "false";
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "Bearer "+MyApp.sharedPreferences.getString("access_token").toString();
    FormData formData = new FormData.fromMap({
      "user_id":userId,
      "star": rate,
    });
    print("Body = ${formData.fields}");
    await dio.post(url, data: formData,)
        .then((response) {
      if (response.data["status"] == "True") {
        print("Response of rateUser = ${response.data}");
        double sum = response.data["sum_of_rating"].toDouble();
        int count = response.data["no_of_rating"]??0;
        double rat = 0.0;
        if(count!=0){
          rating = sum/count.toDouble();
        }
        print("Rating Calculated = $rating");
        complete = "true";
        success = "true";
      }
    }).catchError((e) {
      complete = "true";
      print(url);
      print("error: $e");
      if (e.error is SocketException) {
        success = "error";
        print(e.error);
        print(e);
      } else {
        success = "error";
      }
    });
  }

  static Future followUser(int id) async {
    var url = BaseUrl + "api/follow";
    complete = "false";
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "Bearer "+MyApp.sharedPreferences.getString("access_token").toString();
    FormData formData = new FormData.fromMap({
      "follow_id":id
    });
    await dio.post(url, data: formData,)
        .then((response) {
      if (response.data["status"] == "True") {
        print("Response of followUser = ${response.data}");
        complete = "true";
        success = "true";
      }
    }).catchError((e) {
      complete = "true";
      print(url);
      print("error $e");
      if (e.error is SocketException) {
        success = "error";
        print(e.error);
        print(e);
      } else {
        success = "error";
      }
    });
  }
  static Future unFollowUser(int id) async {
    var url = BaseUrl + "api/unfollow";
    complete = "false";
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "Bearer "+MyApp.sharedPreferences.getString("access_token").toString();
    FormData formData = new FormData.fromMap({
      "follow_id":id
    });
    await dio.post(url, data: formData,)
        .then((response) {
      if (response.data["status"] == "True") {
        print("Response of followUser = ${response.data}");
        complete = "true";
        success = "true";
      }
    }).catchError((e) {
      complete = "true";
      print(url);
      print("error $e");
      if (e.error is SocketException) {
        success = "error";
        print(e.error);
        print(e);
      } else {
        success = "error";
      }
    });
  }
  static Future unLikeProduct(int prodId) async {
    var url = BaseUrl + "api/remove-like";
    complete = "false";
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "Bearer "+MyApp.sharedPreferences.getString("access_token").toString();
    FormData formData = new FormData.fromMap({
      "product_id":prodId
    });
    print("URL = $url");
    print("FormData = ${formData.fields}");
    await dio.post(url, data: formData,)
        .then((response) {
      if (response.data["status"] == "True") {
        print("Response of UnLikeProduct = ${response.data}");
        complete = "true";
        success = "true";
      }
    }).catchError((e) {
      complete = "true";
      print(url);
      print("error $e");
      if (e.error is SocketException) {
        success = "error";
        print(e.error);
        print(e);
      } else {
        success = "error";
      }
    });
  }
  static Future likeProduct(int prodId) async {
    var url = BaseUrl + "api/add-like";
    complete = "false";
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "Bearer "+MyApp.sharedPreferences.getString("access_token").toString();
    FormData formData = new FormData.fromMap({
      "product_id":prodId
    });
    print("URL = $url");
    print("FormData = ${formData.fields}");
    await dio.post(url, data: formData,)
        .then((response) {
      if (response.data["status"] == "True") {
        print("Response of like Product = ${response.data}");
        complete = "true";
        success = "true";
      }
    }).catchError((e) {
      complete = "true";
      print(url);
      print("error $e");
      if (e.error is SocketException) {
        success = "error";
        print(e.error);
        print(e);
      } else {
        success = "error";
      }
    });
  }


  static Future Bid( String vendorId,String productId, String bidPrice,Product prod) async {
    var url = BaseUrl + "api/biding";
    print("USer of Bid = $url");
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "Bearer " +
        MyApp.sharedPreferences.getString("access_token").toString();
    FormData formData = new FormData.fromMap({
      'product_id': productId,
      'bid_price': bidPrice,
      'vendor_id': vendorId,
    });
    await dio.post(url, data: formData,)
        .then((response) {
      if (response.data["status"] == "True") {
        print("Response of Biding = ${response.data}");
        var data = response.data['bid'];
        sentOffer.add(
            ProductBid(
              vendorId: int.parse(data["vendor_id"].toString()),
              counter: null,
              bidId: int.parse(data["id"].toString()),
              productId: int.parse(data["product_id"].toString()),
              status: null,
              bidPrice: data["bid_price"].toString(),
              product: prod,
        )
        );
        complete = "true";
        //success = "true";
      }
    }).catchError((e) {
      complete = "true";
      print(url);
      print("error $e");
      if (e.error is SocketException) {
        success = "error";
        print(e.error);
        print(e);
      } else {
        success = "error";
      }
    });
  }
  static Future acceptRejectBid( String bidId,String status,) async {
    print("BIDID = $bidId");
    print("Status = $status");
    var url = BaseUrl + "api/bid-status";
    print("USer of Bid = $url");
    complete = "false";
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "Bearer "+MyApp.sharedPreferences.getString("access_token").toString();
    FormData formData = new FormData.fromMap({
      'bid_id': bidId,
      'status': status,
    });
    await dio.post(url, data: formData,)
        .then((response) {
      if (response.data["status"] == "True") {
        print("Response of Biding = ${response.data}");
        complete = "true";
        success = "true";
      }
    }).catchError((e) {
      complete = "true";
      print(url);
      print("error $e");
      if (e.error is SocketException) {
        success = "error";
        print(e.error);
        print(e);
      } else {
        success = "error";
      }
    });
  }
  static Future counterBid( int bidId,int counter,Product prod) async {
    var url = BaseUrl + "api/bid-counter";
    print("USer of Bid = $url");
    complete = "false";
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "Bearer " +
        MyApp.sharedPreferences.getString("access_token").toString();
    FormData formData = new FormData.fromMap({
      'bid_id': bidId,
      'counter': counter,
    });
    await dio.post(url, data: formData,)
        .then((response) {
      if (response.data["status"] == "True") {
        print("Response of Biding = ${response.data}");
        var data = response.data['bid'];
        sentOffer.add(
            ProductBid(
              vendorId: int.parse(data["vendor_id"].toString()),
              counter: null,
              bidId: int.parse(data["id"].toString()),
              productId: int.parse(data["product_id"].toString()),
              status: null,
              bidPrice: data["counter"].toString(),
              product: prod,
            )
        );
        complete = "true";
        success = "true";
      }
    }).catchError((e) {
      complete = "true";
      print(url);
      print("error $e");
      if (e.error is SocketException) {
        success = "error";
        print(e.error);
        print(e);
      } else {
        success = "error";
      }
    });
  }


  static Future payment(
      String email,
      String fName,
      String lName,
      String userEmail,
      String phone,
      String address1,
      String address2,
      String city,
      String country,
      String currencyUnit,
      double couponPercent,
      int userId,
      int productId,
      double shippingPrice,
      double amount,
      int vendorId,
      String countryISOCode,
      ) async {
   print("calling api/shipment");
    complete = "false";
    // var url = BaseUrl + "api/payment";
    var url = BaseUrl + "api/shipment";
    // var url = "https://multi.financesettlers.com/multi/public/api/payment";
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "Bearer " +
        MyApp.sharedPreferences.getString("access_token").toString();
    FormData formData = new FormData.fromMap({
      "amount":amount,
      'email': email,
      "first_name": fName,
      "last_name": lName,
      "user_email":userEmail,
      "phone": phone,
      "address_1":address1,
      "address_2": address2,
      "city":city,
      "country":country,
      "currency_unit":currencyUnit.toUpperCase(),
      "discount_percent":couponPercent,
      "user_id": userId,
      "product_id":productId,
      "country_code": countryISOCode,
      "shipment_charges":shippingPrice,
      "seller_id": vendorId,

    });
  print(formData.fields);

    await dio
        .post(
      url,
      data: formData,
    )
        .then((response) {
      // print("response = ${response.data}");
      if (response.data["status"] == "True") {
        paymentlink = response.data['payment_link'];
        complete = "true";
        error = "no Error";
        //success = "true";
      }else{
        complete = "true";
        error = "City Invalid";
      }

    }).catchError((e) {
      complete = "true";
      print(url);
      error = "payerror";
      print("error $e");
      if (e.error is SocketException) {
        success = "error";
        print(e.error);
        print(e);
      } else {
        success = "error";
      }
    });
  }

  static Future doPayment(
      String accountNumber,
      String expiryMonth,
      String expiryYear,
      String cvvNumber,
      String accountHolderName,
      String amount,
      int productId,
      ) async {
    complete = "false";
    // var url = BaseUrl + "api/payment";
    var url = BaseUrl + "api/payment-process";
    // var url = "https://multi.financesettlers.com/multi/public/api/payment";
    print("calling api/payment-process");
      FormData formData = new FormData.fromMap({
        "amount":double.parse(amount),
        'card_number': accountNumber,
        "expiry": "${expiryYear}-${expiryMonth}",
        "cvv":cvvNumber,
        "account_name":accountHolderName,
        "product_id":productId,
      });

    // print(formData.fields);

    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "Bearer " +
        MyApp.sharedPreferences.getString("access_token").toString();
    await dio
        .post(
      url,
      data: formData,
    )
        .then((response) {
      print("api/payment-process response = ${response.data}");
      if (response.data["status"] == "True") {
        complete = "true";
        error = "payment success";
        //success = "true";
      }else{
        complete = "true";
        error = "invalid";
      }

    }).catchError((e) {
      complete = "true";
      print(url);
      error = "payerror";
      print("error $e");
      if (e.error is SocketException) {
        success = "error";
        print(e.error);
        print(e);
      } else {
        success = "error";
      }
    });
  }
  static Future becomeSeller(
      String accountNumber,
      String accountName,
      String bankName,
      String accountHolderPhoneNumber,
      int userId,
      String IBAN,
      ) async {
    complete = "false";
    var url = BaseUrl + "api/seller_account_details";
    // var url = "https://multi.financesettlers.com/multi/public/api/payment";
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "Bearer " +
        MyApp.sharedPreferences.getString("access_token").toString();
    FormData formData = new FormData.fromMap({
      "account_holder_name":accountName,
      "account_number":accountNumber,
      "account_phone_number":accountHolderPhoneNumber,
      "account_bank_name":bankName,
      "user_id":userId,
      "IBAN_Number":IBAN,
    });
    print(formData.fields);
    await dio
        .post(
      url,
      data: formData,
    )
        .then((response) {
      print("response = ${response.data}");
      if (response.data["status"] == "True") {
        complete = "true";
        error = "success";
        //success = "true";
      }else{
        complete = "true";
        error = "error";
      }

    }).catchError((e) {
      complete = "true";
      print(url);
      error = "payerror";
      print("error $e");
      if (e.error is SocketException) {
        success = "error";
        print(e.error);
        print(e);
      } else {
        success = "error";
      }
    });
  }


  static String TrackingNumber = '';
  static Future confirmCheckout(
      int productId,
      String countryISOCode,
      ) async {
    print("calling api/place_order_aramex");
    complete = "false";
    // var url = BaseUrl + "api/payment";
    var url = BaseUrl + "api/place_order_aramex";
    // var url = "https://multi.financesettlers.com/multi/public/api/payment";
    Dio dio = new Dio();
    dio.options.headers["Authorization"] = "Bearer " +
        MyApp.sharedPreferences.getString("access_token").toString();
    FormData formData = new FormData.fromMap({
      "product_id":productId,
      "country_code":countryISOCode,
    });
    print(formData.fields);
    await dio
        .post(
      url,
      data: formData,
    )
        .then((response) {
      print("api/place_order_aramex response = ${response.data}");
      if (response.data["status"] == "True") {
        complete = "true";
        error = "order success";
        TrackingNumber =  response.data["Tracking Number"].toString();
        //success = "true";
      }else{
        complete = "true";
        error = "invalid";
      }

    }).catchError((e) {
      complete = "true";
      print(url);
      error = "payerror";
      print("error $e");
      if (e.error is SocketException) {
        success = "error";
        print(e.error);
        print(e);
      } else {
        success = "error";
      }
    });
  }

  // static Future Cypheme( var vendor_id,var product_id, var bid_Price) async {
  //   sentOffer.clear();
  //   API.getRecivedOrder();
  //   complete = "false";
  //   var url = BaseUrl + "api/bidProduct";
  //   Dio dio = new Dio();
  //   dio.options.headers["authorization"] = "Bearer " +
  //       MyApp.sharedPreferences.getString("access_token").toString();
  //   FormData formData = new FormData.fromMap({
  //     'product_id': int.parse(product_id.toString()),
  //     'price': int.parse(bid_Price.toString()),
  //     'seller_id': int.parse(vendor_id.toString()),
  //   });
  //
  //   await dio
  //       .post(
  //     url,
  //     data: formData,
  //   )
  //       .then((response) {
  //     if (response.statusCode == 200) {
  //       var data = response.data['products'];
  //       print("sdfsgjksdgkjhfdjk "+response.data.toString());
  //       if (data != []) {
  //         for (var da in data) {
  //           sentOffer.add(
  //               Product(
  //                   color: da['color'],
  //                   brand: da['brand'],
  //                   discount: da['discount'],
  //                   productId: da['id'],
  //                   name: da['name'],
  //                   price: da['price'],
  //                   featureImage: da['feature_image'],
  //                   popular: da['popular'],
  //                   images: da['image'],
  //                   shortdesc: da['short_desc'],
  //                   vendorId: da['vendor'],
  //                   size: da['size'],
  //                   type: da['type'])
  //           );
  //         }
  //       }
  //
  //       complete = "true";
  //       //success = "true";
  //     }
  //
  //   }).catchError((e) {
  //     Fluttertoast.showToast(
  //         msg: "$url\n$e",
  //         toastLength: Toast.LENGTH_SHORT,
  //         gravity: ToastGravity.CENTER,
  //         timeInSecForIosWeb: 1,
  //         backgroundColor: Colors.red,
  //         textColor: Colors.white,
  //         fontSize: 16.0
  //     );
  //     complete = "true";
  //     print(url);
  //     print("error $e");
  //     if (e.error is SocketException) {
  //       success = "error";
  //       print(e.error);
  //       print(e);
  //     } else {
  //       success = "error";
  //     }
  //   });
  // }
}
