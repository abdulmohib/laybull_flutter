import 'dart:async';
import 'package:Laybull/Model/notificationModel.dart';
import 'package:Laybull/constant.dart';
import 'package:Laybull/notificationScreen.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:Laybull/API/GloabalVariable.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'API/API.dart';
import 'BottomNavigation/BottomNav.dart';
import 'Registration/Login.dart';
import 'Splash/Splash.dart';
import 'main.dart';


class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  int apisResult = 0;
  int totalApis = 3;
  Future getPopularProduct()async{
    await API.getPopularProduct();
    print("get Popular Products Apis");
    apisResult++;
    if(apisResult==4){
      Navigator.of(context).popUntil((predicate) => predicate.isFirst);
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (context) => BottomNav()),
      );
    }
  }
  Future getReceivedOrders()async{
    // await API.getRecivedOrder();
    print("received Orders api");
    apisResult++;
    if(apisResult==4){
      Navigator.of(context).popUntil((predicate) => predicate.isFirst);
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (context) => BottomNav()),
      );
    }
  }

  initi()async{
    await FirebaseMessaging.instance.getToken().then((value) {
      SharedPreferences.getInstance().then((onValue) {
        onValue.setString("FCMToken", value);
      });
    });
    await SharedPreferences.getInstance().then((value) async{
              MyApp.sharedPreferences = value;
          if(MyApp.sharedPreferences.containsKey("access_token")){
            if(MyApp.sharedPreferences.getString("access_token").isNotEmpty){
              String userName = MyApp.sharedPreferences.getString("userEmail");
              String userPassword = MyApp.sharedPreferences.getString("userPassword");
              String fcmToken = MyApp.sharedPreferences.getString("FCMToken");
              try{
                await API.login(userName, userPassword,fcmToken);
              }catch(e){
                Fluttertoast.showToast(
                    msg: "Failed to Login",
                    toastLength: Toast.LENGTH_SHORT,
                    gravity: ToastGravity.CENTER,
                    timeInSecForIosWeb: 1,
                    backgroundColor: Colors.black38,
                    textColor: Colors.white,
                    fontSize: 16.0
                );
                Navigator.of(context).pushReplacement(MaterialPageRoute(
                    builder: (BuildContext context) => LoginPage()));
                // Scaffold.of(context).showSnackBar(SnackBar(content: Text('Failed to Login!',style: TextStyle(color: Colors.white,backgroundColor : Colors.black),)));
              }
              if(API.complete == 'true'){
                if(success=='error'){
                  Fluttertoast.showToast(
                      msg: "Something went wrong",
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.CENTER,
                      timeInSecForIosWeb: 1,
                      backgroundColor: Colors.black38,
                      textColor: Colors.white,
                      fontSize: 16.0
                  );
                  Navigator.of(context).pushReplacement(MaterialPageRoute(
                      builder: (BuildContext context) => LoginPage()));
                  // Scaffold.of(context).showSnackBar(SnackBar(content: Text('Failed to Login!')));
                }
                else{

                  Navigator.of(context).popUntil((predicate) => predicate.isFirst);
                  Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: (context) => BottomNav()),
                  );
                }
              }
              else
              Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (BuildContext context) => LoginPage()));
            }
            else{
              Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (BuildContext context) => IntroScreen()));
            }
          }
          else{
            Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (BuildContext context) => IntroScreen()));
          }

      return null;
    });
  }
  loginOnFCMessage()async{
    await FirebaseMessaging.instance.getToken().then((value) {
      SharedPreferences.getInstance().then((onValue) {
        onValue.setString("FCMToken", value);
      });
    });
    await SharedPreferences.getInstance().then((value) async{
              MyApp.sharedPreferences = value;
          if(MyApp.sharedPreferences.containsKey("access_token")){
            if(MyApp.sharedPreferences.getString("access_token").isNotEmpty){
              String userName = MyApp.sharedPreferences.getString("userEmail");
              String userPassword = MyApp.sharedPreferences.getString("userPassword");
              String fcmToken = MyApp.sharedPreferences.getString("FCMToken");
              try{
                await API.login(userName, userPassword,fcmToken);
              }catch(e){
                Fluttertoast.showToast(
                    msg: "Failed to Login",
                    toastLength: Toast.LENGTH_SHORT,
                    gravity: ToastGravity.CENTER,
                    timeInSecForIosWeb: 1,
                    backgroundColor: Colors.black38,
                    textColor: Colors.white,
                    fontSize: 16.0
                );
                Navigator.of(context).pushReplacement(MaterialPageRoute(
                    builder: (BuildContext context) => LoginPage()));
                // Scaffold.of(context).showSnackBar(SnackBar(content: Text('Failed to Login!',style: TextStyle(color: Colors.white,backgroundColor : Colors.black),)));
              }
              if(API.complete == 'true'){
                if(success=='error'){
                  Fluttertoast.showToast(
                      msg: "Something went wrong",
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.CENTER,
                      timeInSecForIosWeb: 1,
                      backgroundColor: Colors.black38,
                      textColor: Colors.white,
                      fontSize: 16.0
                  );
                  Navigator.of(context).pushReplacement(MaterialPageRoute(
                      builder: (BuildContext context) => LoginPage()));
                  // Scaffold.of(context).showSnackBar(SnackBar(content: Text('Failed to Login!')));
                }
                else{
                  Navigator.pushNamed(context, LaybullRoute.NotificationScreenRoute,arguments: {"is_from_splash":true});
                }
              }
              else
              Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (BuildContext context) => LoginPage()));
            }
          }
      return null;
    });
  }


  @override
  void initState() {
    configureFirebase();
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.white,
      statusBarIconBrightness: Brightness.dark,
      statusBarBrightness: Brightness.light,
      systemNavigationBarColor: Colors.black,
    ));
    initi();
    super.initState();

  }

  // Future goToMainPage()async{
  //   await Future.delayed(duration)
  // }
  NotificationProvider _notificationProvider;
  @override
  Widget build(BuildContext context) {
    _notificationProvider = Provider.of<NotificationProvider>(context,);
    return Scaffold(
      // backgroundColor: Colors.white,
      body:Container(
        // margin: EdgeInsets.only(top: MediaQuery.of(context).size.height*.05),
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        color: Colors.black,
        child: Center(child: Image.asset('assets/splashLogo.png',width: MediaQuery.of(context).size.width*.7, height: MediaQuery.of(context).size.height*.07,))
      )
    );
  }

  configureFirebase()async{
    // FirebaseMessaging.instance.getInitialMessage().then((RemoteMessage message) {
    //   if (message != null) {
    //     print('A new onMessageOpenedApp Resume event was published! {$message}}');
    //     // Navigator.pushNamed(context, '/message',
    //     //     arguments: MessageArguments(message, true));
    //   }
    // });

    // Returns an instance using a specified FirebaseApp
    // If app is not provided, the default Firebase app will be used. Returns a Stream that is called when an incoming FCM payload is received whilst the Flutter instance is in the foreground.
    // The Stream contains the RemoteMessage.
    // To handle messages whilst the app is in the background or terminated, see onBackgroundMessage.
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      print('A new onMessage event was published! {${message}}');
      _notificationProvider.incCount();
      RemoteNotification notification = message.notification;
      Fluttertoast.showToast(
          msg: "You received a new notification",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.TOP,
          timeInSecForIosWeb: 2,
          backgroundColor: Colors.black38,
          textColor: Colors.white,
          fontSize: 16.0
      );
      AndroidNotification android = message.notification?.android;
      if (notification != null && android != null && !kIsWeb) {
        flutterLocalNotificationsPlugin.show(
            notification.hashCode,
            notification.title,
            notification.body,
            NotificationDetails(
              android: AndroidNotificationDetails(
                channel.id,
                channel.name,
                channel.description,
                // TODO add a proper drawable resource to android, for now using
                //      one that already exists in example app.

                icon: 'launch_background',
              ),

            ));
      }
    });


    // Set a message handler function which is called when the app is in the background or terminated.
    // This provided handler must be a top-level function and cannot be anonymous otherwise an ArgumentError will be thrown.
    FirebaseMessaging.onBackgroundMessage((message){
      print('A new onMessageOpenedApp Resume event was published! {$message}}');
      return;
    });


    // Returns a Stream that is called when a user presses a notification message displayed via FCM.
    // A Stream event will be sent if the app has opened from a background state (not terminated).
    // If your app is opened via a notification whilst the app is terminated, see getInitialMessage.
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      print('A new onMessageOpenedApp appOpened event was published! {$message}}');
      // Navigator.pushNamed(context, '/message',
      //     arguments: MessageArguments(message, true));
    });
      FirebaseMessaging.instance.getToken().then((value) {
        SharedPreferences.getInstance().then((onValue) {
          onValue.setString("FCMToken", value);
      });
    });
  }
}