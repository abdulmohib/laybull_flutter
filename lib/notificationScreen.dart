import 'dart:convert';
import 'dart:io';

import 'package:Laybull/API/API.dart';
import 'package:Laybull/API/BaseUrl.dart';
import 'package:Laybull/BottomNavigation/BottomNav.dart';
import 'package:Laybull/Model/notificationModel.dart';
import 'package:Laybull/Model/routes.dart';
import 'package:Laybull/constant.dart';
import 'package:Laybull/main.dart';
import 'package:Laybull/myCustomProgressDialog.dart';
import 'package:http/http.dart'as http;
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class notification extends StatefulWidget {
  bool isFromProfile;
  Map <String,bool>argument;
  notification({this.isFromProfile,this.argument});
  @override
  State<StatefulWidget> createState() {
    return _notification();
  }
}

class _notification extends State<notification> {
  NotificationProvider _notificationProvider;

  Future<dynamic> _getNotifications()async{

    await apiCallMethod(
        "api/list-notification",
        {
          "userId": "$myUserId",
        },
    );
  }


  AsyncSnapshot snapshotData;

  int myUserId;
  @override void initState() {
    if(widget.isFromProfile==null){
      widget.isFromProfile = false;
    }
    myUserId = int.parse(MyApp.sharedPreferences.getString("userId"));
    super.initState();
  }

  String connectionMessage = "No Internet Connection";
  @override
  Widget build(BuildContext context) {
    _notificationProvider = Provider.of<NotificationProvider>(context,);
    return WillPopScope(
      onWillPop: ()async{
        if(!widget.isFromProfile){
          AppRoutes.makeFirst(context, BottomNav(), 0);
          return false;
        }else{
          return true;
        }
      },
      child: Scaffold(
        body: Container(
          padding: EdgeInsets.only(left: 18, top: 18),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 30,bottom: 18),
                child: Row(
                  children: [
                    GestureDetector(
                      child: Container(
                          height: 20,
                          width: 20,
                          child: Image.asset('assets/ARROW.png',)),
                      onTap: (){
                        if(!widget.isFromProfile){
                          AppRoutes.makeFirst(context, BottomNav(), 0);
                        }else{
                         AppRoutes.pop(context);
                        }
                      },
                    ),
                    SizedBox(width: 15,),
                    Text(
                      'Notification'.toUpperCase(),
                      style: headingStyle,
                    ),
                  ],
                ),
              ),
              FutureBuilder(
                  future: _getNotifications(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData||snapshotData!=null) {
                      print("snapshot.hasData = ${snapshot.hasData}");
                      if(snapshotData==null)
                        snapshotData = snapshot;
                      return Expanded(
                        child:  snapshotData.data['data']['result'].length == 0
                              ? Center(
                            child: Text("Notifications List is Empty"),
                          )
                              : ListView.builder(
                            itemBuilder: (context, int index) {

                              return snapshotData.data["data"]["result"][index]
                              ["userfrom"] ==
                                  null
                                  ? SizedBox()
                                  : notify(index,);
                            },
                            itemCount: snapshotData.data['data']['result'].length,
                          ),
                      );
                    } else {
                      return Expanded(
                          child: Center(child: CircularProgressIndicator()));
                    }
                  })
            ],
          ),
        ),
      ),
    );
  }



  Widget notify(int index,) {

    String createdAt = snapshotData.data["data"]["result"][index]['created_at'];
    DateFormat format = new DateFormat("yyyy-MM-dd HH:mm:ss");
    DateTime time = format.parse("${snapshotData.data["data"]["result"][index]['created_at']}");
    String notificatonTime = time.toString();
    return GestureDetector(
      onTap: () {

      },
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 5),
        color: snapshotData.data["data"]["result"][index]["is_read"] == 0
            ? Color(0xFFFA5657).withOpacity(.10)
            : Colors.white,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              margin: EdgeInsets.only(right: 10,),
              width: MediaQuery.of(context).size.width * .14,
              height: MediaQuery.of(context).size.height * .06,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                image: DecorationImage(
                    image: ExactAssetImage("assets/LaybullIcon.png"),
                    fit: BoxFit.cover
                ),
              ),
            ),
            Expanded(
              child: Container(
                margin: EdgeInsets.only(right: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                        "Notification"
                    ),
                    Text(
                      "$notificatonTime",
                      style: TextStyle(fontSize: 13,fontWeight: FontWeight.bold,color: Colors.grey,letterSpacing: 2,fontFamily: "MetropolisBold"),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<dynamic> apiCallMethod(
      String apiName, Map body,) async {
    var client = http.Client();
    try {
      print("calling API = $BaseUrl$apiName");
      http.Response response = await client.post(
        "$BaseUrl$apiName",
        body: body,
      );
      Map jsonOTPResult = json.decode(response.body);
      print("jsonOTPResult =  $jsonOTPResult");
        if (jsonOTPResult["status"] == "True") {
          // mark-read
          apiCallMethod(
              "api/mark-read",
              {
                "userId": "$myUserId",
              },
          );
          return jsonOTPResult;
        } else {
          Fluttertoast.showToast(
              msg: "Something went wrong",
              textColor: Colors.white,
              gravity: ToastGravity.CENTER,
              backgroundColor: Colors.black38,
          );

          return null;
        }
      } 
     on SocketException {
      Fluttertoast.showToast(
          msg: "No Internet connection ",
          textColor: Colors.white,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.black38
      );
      print('No Internet connection ');

      return null;
    } on HttpException catch (error) {
      print(error);
      Fluttertoast.showToast(
          msg: "$error",
          textColor: Colors.white,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.black38
      );

      return null;
    } on FormatException catch (error) {
      Fluttertoast.showToast(
          msg: "$error",
          textColor: Colors.white,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.black38
      );
      print(error);

      return null;
    } catch (value) {
      print(value);
      return null;
    }
  }
}