import 'package:flutter/material.dart';
import 'package:Laybull/API/API.dart';
import 'package:Laybull/API/BaseUrl.dart';
import 'package:Laybull/Model/Product.dart';
import 'package:Laybull/Model/favourite.dart';
import 'package:Laybull/ProductPages/categories.dart';
import 'package:Laybull/myCustomProgressDialog.dart';
import 'package:Laybull/searchonTap.dart';
import 'package:Laybull/shoe.dart';
import 'package:progress_dialog/progress_dialog.dart';

import 'API/GloabalVariable.dart';
import 'Addtochart.dart';
import 'constant.dart';
import 'lists.dart';
import 'main.dart';

class welcome extends StatefulWidget {
  @override
  _welcomeState createState() => _welcomeState();
}

class _welcomeState extends State<welcome> {
  @override
  void initState() {
    super.initState();
  }

  Future goToCategoryScreen(int index)async{
    MyProgressDialog pr;
    pr = MyProgressDialog(context);
    pr.show();
    try {
      await API.categoryProduct(categoryList[index].id);
      pr.hide();
    }catch(e){
      print("Error = $e");
      pr.hide();
    }
    pr.hide();
    Navigator.push(context, MaterialPageRoute(builder: (context) =>
        Shoe(id:categoryList[index].id,name:categoryList[index].categoryName,products: API.categoryProductsList,)),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(left: 10,right: 10),
          child: ListView(
            children: [
              Row(
                children: [
                  Container(
                    margin: const EdgeInsets.only(top:28.0),
                    child: Text(
                      'Welcome'.toUpperCase(),
                      style: headingStyle,
                    ),
                  ),
                  // Spacer(),
                  // Image.asset('assets/Vector.png'),
                ],
              ),
              SizedBox(
                height: 15,
              ),
              InkWell(
                onTap: () async{
                  var abc = await Navigator.of(context).push(
                    MaterialPageRoute(builder: (context) => search()),
                  );
                  setState(() {

                  });
                },
                child: Container(
                  height: MediaQuery.of(context).size.height*.06,
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.only(left: 2,right: 20),
                  decoration: BoxDecoration(
                    color: Color(0xffF4F4F4),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 10.0),
                    child: Row(
                      children: [
                        Icon(
                          Icons.search,
                          size: 15,
                          color: Colors.black,
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          'Search',
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              color: Colors.grey),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                'CATEGORIES',
                style: Textprimary,
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                height: 70,
                child:
                categoryList.length==0?Container(child: Text("No Data To Show"),):
                new ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: categoryList.length,
                    itemBuilder: (context, index) {
                      // print(BaseUrl+"uploads/categoryImages/"+categoryList[index].categoryImage);
                      dropdowncat.add(categoryList[index].categoryName);
                      return GestureDetector(
                        onTap: (){
                          goToCategoryScreen(index);
                        },
                        child: Padding(
                          padding: const EdgeInsets.only(
                              left: 5.0, top: 5, bottom: 6),
                          child: Container(
                            height: 60,
                            padding: EdgeInsets.symmetric(horizontal: 15),
                            decoration: BoxDecoration(
                              color: Color(0xffE5F3FD),
                              borderRadius: BorderRadius.circular(9),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                categoryList[index].categoryImage.isNotEmpty?
                                Container(
                                  child: Image.network(BaseUrl+"uploads/categoryImages/"+categoryList[index].categoryImage,fit: BoxFit.cover,
                                  ),
                                  margin: EdgeInsets.only(top: 5,left: 5,bottom: 5),
                                ):
                                Container(
                                  child: Image.asset("assets/bigshoe.png",fit: BoxFit.cover,),
                                  margin: EdgeInsets.only(top: 5,left: 5,bottom: 5),
                                )
                                ,
                                // Spacer(),
                                Container(
                                  child: Text(
                                    categoryList[index].categoryName.toUpperCase(),
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    //category[index].destination,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16,
                                        fontFamily: "MetropolisExtraBold"
                                    ),

                                  ),
                                  margin: EdgeInsets.only(left: 15),
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    }),
              ),
              SizedBox(
                height: 20,
              ),
              Text('POPULAR ITEMS', style: Textprimary),
              SizedBox(
                height: 13,
              ),
              productGridView(product: popularproduct,onFavoutireButtonTapped: (){setState(() {});}),
            ],
          ),
        ),
      ),
    );
  }
}
