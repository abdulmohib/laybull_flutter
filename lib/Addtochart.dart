import 'dart:io';

import 'package:Laybull/BottomNavigation/BottomNav.dart';
import 'package:Laybull/Cart/Purchase.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:Laybull/API/GloabalVariable.dart';
import 'package:Laybull/Model/bidModel.dart';
import 'package:Laybull/Model/countryModel.dart';
import 'package:Laybull/ProductPages/editproduct.dart';
import 'package:Laybull/ProductPages/sizelist.dart';
import 'package:Laybull/Model/Product.dart';
import 'package:Laybull/account.dart';
import 'package:Laybull/main.dart';
import 'package:Laybull/myCustomProgressDialog.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:url_launcher/url_launcher.dart';

import 'API/API.dart';
import 'API/BaseUrl.dart';
import 'constant.dart';

class AddToCart extends StatefulWidget {
  Product product;
  AddToCart({this.product ,});

  @override
  _AddToCartState createState() => _AddToCartState();
}

class _AddToCartState extends State<AddToCart> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _bidController = new TextEditingController();
  String paymnetLink;

  var images = [];

  @override
  void initState() {
    print("Images = ${widget.product.images}");
    if(widget.product.images==null)
        images.add(NetworkImage(BaseUrl+"upload/productImage/"+widget.product.feature_image,scale: 1),);
    else
      for(int i = 0; i< widget.product.images.length; i++ ){
        images.add(NetworkImage(BaseUrl+widget.product.images[i]["image"],scale: 1),);
      }
    super.initState();
  }

  // waittoPayment() async {
  //   await Future.delayed(const Duration(seconds: 2), (){
  //     if(API.complete == 'true'){
  //       pr.hide();
  //       Navigator.of(context).pop();
  //       paymnetLink = API.paymentlink;
  //       paymnetLink = paymnetLink.toString();
  //       print("huiujnnojkjnjnjion  "+ paymnetLink);
  //       // _launchInWebViewOrVC(paymnetLink);
  //
  //     }
  //     else{
  //       waittoPayment();
  //     }
  //   });
  // }


  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return Form(
          key: _formKey,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: AlertDialog(
              title: Text('MAKE OFFER',style: TextStyle(fontWeight: FontWeight.bold,letterSpacing: 2,fontFamily: "MetropolisExtraBold"),),
              content: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[
                    Text('Enter your price below'),
                    SizedBox(height: 20),
                    ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: TextFormField(
                          controller: _bidController,
                          inputFormatters: [
                            FilteringTextInputFormatter.digitsOnly,
                          ],
                          validator: (value){
                            if(value.isEmpty){
                              return 'Please enter Bid amount';
                            }
                            return null;
                          },
                          style: TextStyle(
                              fontSize: 15, fontFamily: 'Metropolis', letterSpacing: 2.2),
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              suffix: Text(
                                "AED",
                                style: TextStyle(fontWeight: FontWeight.bold,letterSpacing: 2,fontFamily: "MetropolisExtraBold"),
                              ),
                              // suffixText: "AED",
                              // suffixStyle:  TextStyle(fontWeight: FontWeight.bold,letterSpacing: 2,fontFamily: "MetropolisExtraBold"),
                              fillColor: Color(0xfff3f3f4),
                              filled: true)
                      ),
                    ),
                  ],
                ),
              ),
              actions: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    TextButton(
                      child: Text('CANCEL'),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                    TextButton(
                      child: Text('OFFER'),
                      onPressed: () async{
                        if(_formKey.currentState.validate()){
                          FocusScope.of(context).unfocus();
                          dlg.show();
                          try {
                            await API.Bid(widget.product.vendor_id.toString(),
                                widget.product.id.toString(),
                                _bidController.text.trim(),widget.product);
                          }catch(e){
                            dlg.hide();
                          }Navigator.of(context).pop();
                          dlg.hide();
                          if(API.complete == 'true'){
                            widget.product.bid_price = _bidController.text.trim();
                            Fluttertoast.showToast(
                                msg: "Offer Sent Successfully",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIosWeb: 1,
                                backgroundColor: Colors.black38,
                                textColor: Colors.white,
                                fontSize: 16.0
                            );
                          }
                          dlg.hide();

                        }
                      },
                    ),
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  Future<void> _showMyDialogPayment() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return Form(
          key: _formKey,
          child: AlertDialog(
            title: Text('Payment'),
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  Text('you want to purchase '+widget.product.name +" at AED"+widget.product.price+" Price"),
                ],
              ),
            ),
            actions: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  TextButton(
                    child: Text('NO'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),

                  TextButton(
                    child: Text('YES'),
                    onPressed: () {
                      Navigator.push(context, MaterialPageRoute(builder: (_)=>ConfirmAddress(product: widget.product,)));
                    },
                  ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }

  MyProgressDialog dlg;
  @override
  Widget build(BuildContext context) {
    int vendorId = int.parse(MyApp.sharedPreferences.get('userId').toString());


    dlg = MyProgressDialog(context);
    return Scaffold(
        body: SafeArea(
          child: SingleChildScrollView(
              child: new Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 20.0, left: 10, right: 15),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          InkWell(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Container(
                                padding: EdgeInsets.all(10),
                                child: Image.asset(
                                  'assets/ARROW.png',
                                )),

                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text('Product Detail',style: headingStyle,),
                          Spacer(),
                          widget.product.vendor_id == vendorId?
                          GestureDetector(
                            onTap: (){
                              Navigator.push(context, MaterialPageRoute(builder: (BuildContext)=>editProduct(product: widget.product,)));
                            },
                            child: Icon(Icons.edit),
                          )
                              :SizedBox(),
                          SizedBox(width: 10,),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child:Container(
                        height: MediaQuery.of(context).size.height*.4,
                        child: Carousel(
                          images: images,
                          dotSize: 4.0,
                          borderRadius: true,
                          autoplay: false,
                          dotPosition: DotPosition.bottomCenter,
                          dotBgColor: Colors.transparent,
                          dotColor: Colors.black,
                        ),
                      )
                    ),
                    SizedBox(height: 15,),
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            padding: const EdgeInsets.only(top: 1.0),
                            width: MediaQuery.of(context).size.width,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  widget.product.name,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                    fontFamily: "MetropolisBold"
                                  ),
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                Text(widget.product.price,
                                    style: TextStyle(
                                        fontSize: 15, fontWeight: FontWeight.w600,color: Colors.grey)),
                                SizedBox(
                                  height: 15,
                                ),
                                Text("${widget.product.condition}",
                                    style: TextStyle(
                                        fontSize: 12, fontWeight: FontWeight.w200,color: Colors.grey[500])),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 25,
                          ),
                          vendorId!=widget.product.vendor_id?
                          Padding(
                            padding: const EdgeInsets.only(right: 15, left: 15),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                InkWell(
                                  onTap: (){
                                    _showMyDialog();
                                  },
                                  child: Container(
                                      height: 45,
                                      width: MediaQuery.of(context).size.width/2.5,
                                      decoration: BoxDecoration(
                                          color: Colors.black,
                                          borderRadius: BorderRadius.circular(8)
                                      ),
                                      child: Center(child: Text("MAKE OFFER", style: TextStyle(color: Colors.white,letterSpacing: 1.1, fontWeight: FontWeight.w800, fontSize: 10),))
                                  ),
                                ),

                                InkWell(
                                  onTap: (){
                                    Navigator.push(context, MaterialPageRoute(builder: (BuildContext)=>ConfirmAddress(product: widget.product,)));
                                    // _showMyDialogPayment();
                                  },
                                  child: Container(
                                      height: 45,
                                      width: MediaQuery.of(context).size.width/2.5,
                                      decoration: BoxDecoration(
                                          color: Colors.black,
                                          borderRadius: BorderRadius.circular(8)
                                      ),
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: [
                                              Image.asset("assets/authentic.png", color: Colors.white,width: 15,height: 15,),
                                              SizedBox(width: 2,),
                                              Text("BUY NOW", style: TextStyle(color: Colors.white,letterSpacing: 1.12, fontWeight: FontWeight.bold, fontSize: 10),),
                                            ],
                                          ),
                                          SizedBox(height: 2,),
                                          Text("Verified Authentic",textAlign: TextAlign.center, style: TextStyle(color: Colors.white,letterSpacing: 0.12, fontSize: 10),),
                                        ],
                                      )
                                  ),
                                ),

                              ],
                            ),
                          ):SizedBox(),
                          SizedBox(
                            height: 20,
                          ),
                          Container(
                            height: 150,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(color: Color(0xff5DB3F5))
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(top: 10,left: 12),
                                  child: Row(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      Image.asset("assets/authentic.png", color: Color(0xff5DB3F5),width: 15,height: 15,),
                                      SizedBox(width: 5,),
                                      Text("Verified Authentic", style: TextStyle(color: Color(0xff5DB3F5),fontWeight: FontWeight.bold),),
                                    ],
                                  ),
                                ),
                                SizedBox(height: 20,),
                                Container(
                                    width: 300,
                                    child: Padding(
                                      padding: const EdgeInsets.only(left:12.0),
                                      child: Text("All items are sent to laybull's authentication team for in-hand verification before being shipped out to you." ,style: TextStyle(color: Color(0xff5DB3F5)),),
                                    )),
                                SizedBox(height: 3,),
                                GestureDetector(
                                  onTap: ()async{
                                    String url = "https://laybull.com/authentication-process";
                                    if (await canLaunch(url))
                                        await launch(url);
                                    else
                                      Fluttertoast.showToast(
                                          msg: "Failed to open Link",
                                          toastLength: Toast.LENGTH_SHORT,
                                          gravity: ToastGravity.CENTER,
                                          timeInSecForIosWeb: 1,
                                          backgroundColor: Colors.black38,
                                          textColor: Colors.white,
                                          fontSize: 16.0
                                      );
                                  },
                                  child: Padding(
                                    padding: const EdgeInsets.only(left:12.0),
                                    child: Text('Learn More',style: TextStyle(color: Color(0xff5DB3F5,),decoration: TextDecoration.underline)),
                                  ),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Text('SELLER', style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold,color: Colors.black,letterSpacing: 2,fontFamily: "MetropolisExtraBold")),
                          SizedBox(
                            height: 20,
                          ),
                          Row(
                            children: [
                              Text(
                                'NAME :',
                                style: TextStyle(fontSize: 13,fontWeight: FontWeight.bold,color: Colors.black,letterSpacing: 2,fontFamily: "MetropolisBold"),
                              ),
                              Spacer(),
                              GestureDetector(
                                onTap: (){
                                  print("Vendor Id = ${widget.product.vendor_id}");
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) => Account(userId: widget.product.vendor_id,isFromBottomNav: false,)),
                                  );
                                },
                                child: Container(
                                  padding: EdgeInsets.symmetric(horizontal: 3,vertical: 5),
                                  child: Text(
                                    widget.product.vendorName.isNotEmpty?widget.product.vendorName:"User name",
                                    style: TextStyle(color: Color(0xff5DB3F5),fontWeight: FontWeight.bold,fontSize: 14,fontFamily: "MetropolisBold"),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                'SPECIFICATION',
                                style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold,color: Colors.black,letterSpacing: 2,fontFamily: "MetropolisExtraBold"),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Row(
                            children: [
                              Text(
                                'SIZE:',
                                style: TextStyle(fontSize: 13,fontWeight: FontWeight.bold,color: Colors.black,letterSpacing: 2,fontFamily: "MetropolisBold"),
                              ),
                              Spacer(),
                              Text(
                                widget.product.size.toString(),
                                style: TextStyle(fontSize: 13,fontWeight: FontWeight.bold,color: Colors.grey,letterSpacing: 2,fontFamily: "MetropolisBold"),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Row(
                            children: [
                              Text(
                                'COLOR:',
                                style: TextStyle(fontSize: 13,fontWeight: FontWeight.bold,color: Colors.black,letterSpacing: 2,fontFamily: "MetropolisBold"),
                              ),
                              Spacer(),
                              Text(
                                widget.product.color.toString(),
                                style: TextStyle(fontSize: 13,fontWeight: FontWeight.bold,color: Colors.grey,letterSpacing: 2,fontFamily: "MetropolisBold"),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Row(
                            children: [
                              Text(
                                'CONDITION:',
                                style: TextStyle(fontSize: 13,fontWeight: FontWeight.bold,color: Colors.black,letterSpacing: 2,fontFamily: "MetropolisBold"),
                              ),
                              Spacer(),
                              Text(
                                "${widget.product.condition}",
                                style: TextStyle(fontSize: 13,fontWeight: FontWeight.bold,color: Colors.grey,letterSpacing: 2,fontFamily: "MetropolisBold"),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 25,
                          ),

                              GestureDetector(
                                onTap: (){
                                  showDialog(
                                      context: context,
                                      builder: (BuildContext context) => Dialog(
                                          backgroundColor: Colors.transparent,
                                          child: PictureDialog()
                                      )
                                  );
                                },
                                child: Container(
                                    height: MediaQuery.of(context).size.height / 13,
                                    width: MediaQuery.of(context).size.width / 1.1,
                                    decoration: BoxDecoration(
                                   border: Border.all(   color: Colors.black,),
                                      borderRadius: BorderRadius.circular(6),
                                    ),
                                      child: Center(
                                          child: Text(
                                            'Size Guide'.toUpperCase(),
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontWeight: FontWeight.w800,
                                                fontStyle: FontStyle.normal,
                                                fontFamily: 'MetropolisBold',
                                                letterSpacing: 1,
                                                fontSize: 15.0),
                                          )),
                          ),
                              ),
                          SizedBox(
                            height: 12,)
                          // Text('RELATED PRODUCTS', style: Textprimary),
                          // Container(
                          //   height: MediaQuery.of(context).size.height / 3.5,
                          //   width: MediaQuery.of(context).size.width,
                          //   //color: Colors.black,
                          //   child: GridView.builder(
                          //     gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                          //         maxCrossAxisExtent: 200,
                          //         // childAspectRatio: (itemWidth / itemHeight),
                          //         crossAxisSpacing: 20,
                          //         mainAxisSpacing: 20),
                          //     physics: NeverScrollableScrollPhysics(),
                          //     itemCount: slideList.length,
                          //     itemBuilder: (BuildContext ctx, index) {
                          //       return Container(
                          //         child: Column(
                          //           mainAxisAlignment: MainAxisAlignment.start,
                          //           crossAxisAlignment: CrossAxisAlignment.start,
                          //           children: [
                          //             Stack(
                          //               children: [
                          //                 Column(
                          //                   children: [
                          //                     Container(
                          //                       height: 100,
                          //                       width:
                          //                           MediaQuery.of(context).size.width /
                          //                               2.4,
                          //                       decoration: BoxDecoration(
                          //                         color: Greycolor,
                          //                         borderRadius:
                          //                             BorderRadius.circular(10),
                          //                       ),
                          //                       child:
                          //                           Image.asset(slideList[index].image),
                          //                     ),
                          //                   ],
                          //                 ),
                          //                 Padding(
                          //                   padding: const EdgeInsets.only(
                          //                       left: 120.0, top: 77),
                          //                   child: Container(
                          //                     height: 30,
                          //                     width: 30,
                          //                     decoration: BoxDecoration(
                          //                       color: Colors.white,
                          //                       borderRadius: BorderRadius.circular(20),
                          //                     ),
                          //                     child: Image.asset('assets/fire.png'),
                          //                   ),
                          //                 ),
                          //                 // Padding(
                          //                 //   padding: const EdgeInsets.only(top:123.0),
                          //                 //   child: Column(
                          //                 //     mainAxisAlignment: MainAxisAlignment.start,
                          //                 //     crossAxisAlignment: CrossAxisAlignment.start,
                          //                 //     children: [
                          //                 //       Text('Air Jordan 1 Dior',style: TextStyle(fontSize: 10),),
                          //                 //       Text('425 AED',style: TextStyle(fontSize: 10)),
                          //                 //       Text('Used',style: TextStyle(fontSize: 10)),
                          //                 //
                          //                 //     ],
                          //                 //   ),
                          //                 // )
                          //               ],
                          //             ),
                          //             Padding(
                          //               padding: const EdgeInsets.only(top: 1.0),
                          //               child: Column(
                          //                 mainAxisAlignment: MainAxisAlignment.start,
                          //                 crossAxisAlignment: CrossAxisAlignment.start,
                          //                 children: [
                          //                   Text(
                          //                     'Air Jordan 1 Dior',
                          //                     style: TextStyle(
                          //                         fontSize: 10,
                          //                         fontWeight: FontWeight.w500),
                          //                   ),
                          //                   Text('425 AED',
                          //                       style: TextStyle(
                          //                           fontSize: 10,
                          //                           fontWeight: FontWeight.bold)),
                          //                   Text('Used',
                          //                       style: TextStyle(
                          //                           fontSize: 10,
                          //                           fontWeight: FontWeight.w200)),
                          //                 ],
                          //               ),
                          //             )
                          //           ],
                          //         ),
                          //       );
                          //     },
                          //   ),
                          // ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
    ),
        ));
  }
}
// Image.asset("assets/images/color.png"),
// Row(
// children: [
// InkWell(
// onTap: () {
// Navigator.pop(context);
// },
// child: Container(
// height: 20,
// width: 20,
// child: Image.asset(
// 'assets/images/ARROW.png',
// )),
// ),
// SizedBox(
// width: 10,
// ),
// Spacer(),
// Container(
// height: 30,
// width: 30,
// decoration: BoxDecoration(
// color: Colors.white,
// borderRadius:
// BorderRadius.circular(20),
// ),
// child: Image.asset(
// 'assets/images/fire.png'),
// ),
// SizedBox(width: 3,),
// Image.asset('assets/images/Vector.png'),
// ],
// ),
// Container(
// height: 100,
// width: MediaQuery.of(context).size.width,
// child: Image.asset('assets/images/bigshoe.png'),
// ),

class ConfirmAddress extends StatefulWidget {
  ProductBid bidProduct;
  Product product;
  ConfirmAddress({this.bidProduct,this.product});
  @override
  _ConfirmAddressState createState() => _ConfirmAddressState();
}

class _ConfirmAddressState extends State<ConfirmAddress> {
  TextEditingController _fname = TextEditingController();
  TextEditingController _lname = TextEditingController();
  TextEditingController _phone = TextEditingController();
  TextEditingController _email = TextEditingController();
  TextEditingController _address1 = TextEditingController();
  TextEditingController _address2 = TextEditingController();
  CountryModel _selectedCountry;
  CountryModel country;
  String paymnetLink;
  bool isCityFetched = false;
  TextEditingController _city = TextEditingController();
  TextEditingController _discountCode = TextEditingController();
  double couponValue = 0.0;
  bool isDiscountVerified = false;
  double prodPrice = 0.0;
  Future fetchCountry()async{
    var url = BaseUrl + "api/list-of-countries";
    Dio dio = new Dio();
    // dio.options.headers["authorization"] = "Bearer " + MyApp.sharedPreferences.getString("access_token").toString();
    await dio
        .get(url,)
        .then((response) {
      if (response.statusCode==200){
        listCountry = List<CountryModel>.from(response.data["data"]["country"].map((prod) => CountryModel.fromJson(prod)).toList());
        setState(() {
            isCityFetched = true;
        });
      } else {
        success = "true";
        print("Exception in Api = $url");
        throw Exception('Failed to Fetch Vendors');
      }
    });
  }
  @override void initState() {
    prodPrice = widget.product==null?double.parse(widget.bidProduct.bidPrice):double.parse(widget.product.price);
    if(widget.product!=null){
      prodPrice = double.parse(widget.product.price);
    }else if(widget.bidProduct.counter==null){
      prodPrice = double.parse(widget.bidProduct.product.price);
    }else{
      prodPrice = double.parse(widget.bidProduct.counter);
    }
    fetchCountry();
    super.initState();
  }
  MyProgressDialog pr;
  @override
  Widget build(BuildContext context) {
    pr = MyProgressDialog(context);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.only(left: 30,right: 30, top: 30),
          child: ListView(
            children: [
              Row(
                children: [
                  GestureDetector(
                    onTap: (){
                      Navigator.pop(context);
                    },
                    child: Container(
                        padding:EdgeInsets.all(10),child: Image.asset(
                      'assets/ARROW.png',
                    )
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "SHIPPING",
                    style: headingStyle,
                  ),

                ],
              ),
              SizedBox(height: 20,),
              /// First Name
              Text(
                "First Name".toUpperCase(),
                style: Textprimary,
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                height: 50,
                width: MediaQuery.of(context).size.width/1.07,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    border: Border.all(color: Colors.grey[400])
                ),
                child: TextField(
                  controller: _fname,
                    style: TextStyle(
                      fontSize: 18,
                    ),
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      contentPadding: EdgeInsets.only(left: 10),
                      fillColor: Color(0xfff3f3f4),
                    )),
              ),
              SizedBox(
                height: 15,
              ),
              /// Last Name
              Text(
                "Last Name".toUpperCase(),
                style: Textprimary,
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                height: 50,
                width: MediaQuery.of(context).size.width/1.07,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    border: Border.all(color: Colors.grey[400])
                ),
                child: TextField(
                  controller: _lname,
                    style: TextStyle(
                      fontSize: 18,
                    ),
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      contentPadding: EdgeInsets.only(left: 10),
                      fillColor: Color(0xfff3f3f4),
                    )),
              ),
              SizedBox(
                height: 15,
              ),
              /// Email
              Text(
                "Email".toUpperCase(),
                style: Textprimary
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                height: 50,
                width: MediaQuery.of(context).size.width/1.07,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    border: Border.all(color: Colors.grey[400])
                ),
                child: TextField(
                  controller: _email,
                    style: TextStyle(
                      fontSize: 18,
                    ),
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      contentPadding: EdgeInsets.only(left: 10),
                      fillColor: Color(0xfff3f3f4),
                    )),
              ),
              SizedBox(
                height: 15,
              ),
              /// Phone Number
              Text(
                "Phone Number".toUpperCase(),
                style: Textprimary
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                height: 50,
                width: MediaQuery.of(context).size.width/1.07,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    border: Border.all(color: Colors.grey[400])
                ),
                child: TextField(
                  controller: _phone,
                    style: TextStyle(
                      fontSize: 18,
                    ),
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      contentPadding: EdgeInsets.only(left: 10),
                      fillColor: Color(0xfff3f3f4),
                    )),
              ),
              SizedBox(
                height: 15,
              ),
              /// Address 1
              Text(
                "Address 1".toUpperCase(),
                style: Textprimary
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                height: 50,
                width: MediaQuery.of(context).size.width/1.07,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    border: Border.all(color: Colors.grey[400])
                ),
                child: TextField(
                  controller: _address1,
                    style: TextStyle(
                      fontSize: 18,
                    ),
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      contentPadding: EdgeInsets.only(left: 10),
                      fillColor: Color(0xfff3f3f4),
                    )),
              ),
              SizedBox(
                height: 15,
              ),
              /// Address 2
              Text(
                "Address 2".toUpperCase(),
                style: Textprimary
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                height: 50,
                width: MediaQuery.of(context).size.width/1.07,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    border: Border.all(color: Colors.grey[400])
                ),
                child: TextField(
                  controller: _address2,
                    style: TextStyle(
                      fontSize: 18,
                    ),
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      contentPadding: EdgeInsets.only(left: 10),
                      fillColor: Color(0xfff3f3f4),
                    )),
              ),
              SizedBox(
                height: 15,
              ),
              /// Country
              isCityFetched?
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Country".toUpperCase(),
                    style: Textprimary
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 10, right: 10),
                    decoration: BoxDecoration(
                      border: Border.all(color:Colors.grey[400]),
                      borderRadius: BorderRadius.circular(5.0),),
                    child: DropdownButton(
                      isExpanded: true,
                      hint: Text("Country"),
                      dropdownColor: Colors.grey[100],
                      icon: Icon(
                        Icons.keyboard_arrow_down_outlined,
                        size: 20,
                        color: Colors.black,
                      ),
                      iconSize: 12,
                      underline: SizedBox(),
                      style: TextStyle(color: Colors.black, fontSize: 15),
                      value: _selectedCountry,
                      onChanged: (newValue) {
                        setState(() {
                          _selectedCountry = newValue;
                          print("value = ${_selectedCountry.countryISOCode}");
                        });
                      },
                      items: listCountry.map((valueItem) {
                        return DropdownMenuItem(
                          value: valueItem,
                          child: new Row(
                            children: <Widget>[
                              Text(valueItem.countryName),
                            ],
                          ),
                        );
                      }).toList(),
                    ),
                  ),
                ],
              ):SizedBox(),
              SizedBox(
                height: 15,
              ),
              /// City
              Text(
                "City".toUpperCase(),
                style: Textprimary
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                height: 50,
                width: MediaQuery.of(context).size.width/1.07,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    border: Border.all(color: Colors.grey[400])
                ),
                child: TextField(
                  controller: _city,
                    style: TextStyle(
                      fontSize: 18,
                    ),
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      contentPadding: EdgeInsets.only(left: 10),
                      fillColor: Color(0xfff3f3f4),
                    )),
              ),
              SizedBox(
                height: 15,
              ),
              isDiscountVerified?SizedBox()
                  :
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  /// Discount Code
                  Text(
                      "Discount Code".toUpperCase(),
                      style: Textprimary
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Expanded(
                        child: Container(
                          height: 50,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(color: Colors.grey[400])
                          ),
                          child: TextField(
                              controller: _discountCode,
                              style: TextStyle(
                                fontSize: 18,
                              ),
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                contentPadding: EdgeInsets.only(left: 10),
                                fillColor: Color(0xfff3f3f4),
                              )),
                        ),
                      ),
                      SizedBox(width: 5,),
                      GestureDetector(
                        onTap: ()async{
                          if(isDiscountVerified)
                            return;
                          if(_discountCode.text.trim().isEmpty){
                            Fluttertoast.showToast(
                                msg: "Enter Discount Code",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIosWeb: 1,
                                backgroundColor: Colors.black38,
                                textColor: Colors.white,
                                fontSize: 16.0
                            );
                          }
                          FocusScope.of(context).unfocus();
                          pr.show();
                          try {
                           await verifyCoupon(_discountCode.text.trim());
                            setState(() {
                              print("Dooo = $couponValue");
                            });
                          }catch(e){
                            pr.hide();
                          }
                          pr.hide();
                          if(API.complete == 'true'&& success=="true"){
                            Fluttertoast.showToast(
                                msg: "Coupon Verified",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIosWeb: 1,
                                backgroundColor: Colors.black38,
                                textColor: Colors.white,
                                fontSize: 16.0
                            );
                            setState(() {
                              isDiscountVerified = true;
                            });
                          }else{
                            Fluttertoast.showToast(
                                msg: "Coupon Not Verified",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIosWeb: 1,
                                backgroundColor: Colors.black38,
                                textColor: Colors.white,
                                fontSize: 16.0
                            );
                          }
                          pr.hide();
                        },
                        child: Container(
                          height: 40,
                          decoration: BoxDecoration(color: Colors.black,borderRadius: BorderRadius.circular(5)),
                          padding: EdgeInsets.symmetric(horizontal: 10,),
                          child: Center(
                            child: Text(isDiscountVerified?"Verified":"Verify",style:TextStyle(fontSize: 14,fontWeight: FontWeight.bold,color: Colors.white,letterSpacing: 2,fontFamily: "MetropolisExtraBold"),),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              SizedBox(
                height: 15,
              ),
              /// Shipping Cost
              isCityFetched?
              Column(
                children: [
                  isDiscountVerified?
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("DISCOUNT:",style: Textprimary,),
                      Text(
                        "$couponValue%",
                        style: TextStyle(fontSize: 10,fontWeight: FontWeight.w600,color: Colors.grey,letterSpacing: 2,fontFamily: "MetropolisExtraBold"),),
                    ],
                  )
                      :SizedBox(),
                  SizedBox(height: 15,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("SUBTOTAL:",style: Textprimary,),
                      Text("${widget.product!=null?widget.product.price:widget.bidProduct.counter==null?widget.bidProduct.bidPrice:widget.bidProduct.counter} AED",style: TextStyle(fontSize: 10,fontWeight: FontWeight.w600,color: Colors.grey,letterSpacing: 2,fontFamily: "MetropolisExtraBold"),),
                    ],
                  ),
                  SizedBox(height: 15,),
                  _selectedCountry!=null?
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Delivery Charges:".toUpperCase(),style: Textprimary,),
                      Text("${_selectedCountry.shippingPrice} AED",style: TextStyle(fontSize: 10,fontWeight: FontWeight.w600,color: Colors.grey,letterSpacing: 2,fontFamily: "MetropolisExtraBold"),),
                    ],
                  )
                      :SizedBox(),
                  SizedBox(height: 15,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("TOTAL:",style: Textprimary,),
                      Text("${(double.parse(widget.product!=null?widget.product.price:widget.bidProduct.counter==null?widget.bidProduct.bidPrice:widget.bidProduct.counter))+(_selectedCountry!=null?_selectedCountry.shippingPrice:0.0)-double.parse(widget.product!=null?widget.product.price:widget.bidProduct.counter==null?widget.bidProduct.bidPrice:widget.bidProduct.counter)*(couponValue/100)} AED",style: TextStyle(fontSize: 10,fontWeight: FontWeight.w600,color: Colors.grey,letterSpacing: 2,fontFamily: "MetropolisExtraBold"),),
                    ],
                  ),
                ],
              )
              :SizedBox(),
              SizedBox(height: 30,),
              InkWell(
                onTap:!isCityFetched?null:()async{
                  if(_fname.text.trim().isEmpty||_lname.text.trim().isEmpty||_email.text.trim().isEmpty||_phone.text.trim().isEmpty||_address1.text.trim().isEmpty||_selectedCountry==null||_city.text.trim().isEmpty){
                    Fluttertoast.showToast(
                        msg: "Please fill required fields",
                        toastLength: Toast.LENGTH_SHORT,
                        gravity: ToastGravity.CENTER,
                        timeInSecForIosWeb: 1,
                        backgroundColor: Colors.black38,
                        textColor: Colors.white,
                        fontSize: 16.0
                    );
                    return;
                  }else{
                    print(((double.parse(widget.product!=null?widget.product.price:widget.bidProduct.counter==null?widget.bidProduct.bidPrice:widget.bidProduct.counter)-double.parse(widget.product!=null?widget.product.price:widget.bidProduct.counter==null?widget.bidProduct.bidPrice:widget.bidProduct.counter)*(couponValue/100))+(_selectedCountry!=null?_selectedCountry.shippingPrice:0.0)));
                    pr.show();
                    double buyingAmount = ((double.parse(widget.product!=null?widget.product.price:widget.bidProduct.counter==null?widget.bidProduct.bidPrice:widget.bidProduct.counter)-double.parse(widget.product!=null?widget.product.price:widget.bidProduct.counter==null?widget.bidProduct.bidPrice:widget.bidProduct.counter)*(couponValue/100))+(_selectedCountry!=null?_selectedCountry.shippingPrice:0.0));
                    try{
                      await API.payment(
                        "test@gmail.com",
                        _fname.text.trim(),
                        _lname.text.trim(),
                        _email.text.trim(),
                        _phone.text.trim(),
                        _address1.text.trim(),
                        _address2.text.trim(),
                        _city.text.trim(),
                        _selectedCountry.countryName,
                        _selectedCountry.countryCurrencyLabel,
                        couponValue,
                        int.parse(MyApp.sharedPreferences.getString("userId")),
                        widget.product!=null?widget.product.id:widget.bidProduct.productId,
                        _selectedCountry.shippingPrice,
                        buyingAmount,
                        widget.product!=null?widget.product.vendor_id:widget.bidProduct.vendorId,
                        _selectedCountry.countryISOCode,
                      );
                      pr.hide();
                      if(API.complete == 'true' && API.error == "no Error" ){
                        pr.hide();
                        // Navigator.push(context, MaterialPageRoute(builder: (_)=>PaymentGateWAy(productId: widget.bidProduct!=null?widget.bidProduct.productId: widget.product.productId,amount: widget.bidProduct!=null?widget.bidProduct.counter==null? widget.bidProduct.bidPrice:widget.bidProduct.counter:widget.product.price,contryISOCode: _selectedCountry.countryISOCode,)));
                        Navigator.push(context, MaterialPageRoute(builder: (_)=>PaymentGateWAy(productId: widget.bidProduct!=null?widget.bidProduct.productId: widget.product.id,amount: buyingAmount.toString(),contryISOCode: _selectedCountry.countryISOCode,)));
                        // Navigator.of(context).pop();
                        // paymnetLink = API.paymentlink;
                        // paymnetLink = paymnetLink.toString();
                        // _launchInWebViewOrVC(paymnetLink);

                      }else if(API.complete=="true" && API.error=="City Invalid"){
                        Fluttertoast.showToast(
                            msg: "City invalid",
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.CENTER,
                            timeInSecForIosWeb: 1,
                            backgroundColor: Colors.black38,
                            textColor: Colors.white,
                            fontSize: 16.0
                        );
                      }
                    }catch(e){
                      print("Exception = $e");
                      pr.hide();
                    }
                    // Navigator.pop(context);
                  }

                },
                child: Container(
                  width:  MediaQuery.of(context).size.width/1.07,
                  height: 45,
                  margin: EdgeInsets.only(bottom: 5),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: isCityFetched?Colors.black:Colors.grey,
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  child: Text(
                    isCityFetched?'PROCEED TO CHECKOUT':"FETCHING COUNTRIES",
                    style:TextStyle(fontSize: 14,fontWeight: FontWeight.bold,color: Colors.white,letterSpacing: 2,fontFamily: "MetropolisExtraBold"),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 10,bottom: 30),
                child: Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.all(10),
                  child: Center(
                      child: Text.rich(
                          TextSpan(
                              text: 'By proceeding, I confirm that I have read and agree to the ', style: TextStyle(
                              fontSize: 12, color: Colors.grey
                          ),
                              children: <TextSpan>[
                                TextSpan(
                                    text: 'Purchase & Return', style: TextStyle(
                                  fontSize: 12, color: Colors.grey,
                                  decoration: TextDecoration.underline,
                                ),
                                    recognizer: TapGestureRecognizer()
                                      ..onTap = () async {
                                      print("Tapped");
                                        String url = "https://laybull.com/payment-terms";
                                        if (await canLaunch(url))
                                        await launch(url);
                                        else
                                        Fluttertoast.showToast(
                                        msg: "Failed to open Link",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.CENTER,
                                        timeInSecForIosWeb: 1,
                                        backgroundColor: Colors.black38,
                                        textColor: Colors.white,
                                        fontSize: 16.0
                                        );
                                      }
                                ),
                                TextSpan(
                                    text: ' Policy and my shipping address is correct.', style: TextStyle(
                                    fontSize: 12, color: Colors.grey
                                ),
                                )
                              ]
                          )
                      )
                  ),
                )
              ),
            ],
          ),
        ),
      ),
    );
  }
  Future verifyCoupon(String coupon) async {
    var url = BaseUrl + "api/verify_coupon";
    print("USer of Bid = $url");
    API.complete = "false";
    Dio dio = new Dio();
    FormData formData = new FormData.fromMap({
      'coupon_name': coupon,
    });
    await dio.post(url, data: formData,)
        .then((response) {
      if (response.data["status"] == "True") {
        print("Response of Biding = ${response.data}");
        couponValue = double.parse(response.data['coupon']["discount_percent"].toString());
        print("data = $couponValue");
        API.complete = "true";
        success = "true";
      }
    }).catchError((e) {
      API.complete = "true";
      print(url);
      print("errorss $e");
      if (e.error is SocketException) {
        success = "error";
        print(e.error);
        print(e);
      } else {
        success = "error";
      }
    });
  }
  Future<void> _launchInWebViewOrVC(String url) async {
    bool a = await launch(
      url,
      forceSafariVC: true,
      forceWebView: true,
      enableJavaScript: true,
      //headers: <String, String>{'my_header_key': 'my_header_value'},
    );
    print("Payment APU result = $a");
    // } else {
    //   throw 'Could not launch $url';
    // }
  }
}

class PictureDialog extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return _PictureDialog();
  }
}
class _PictureDialog extends State<PictureDialog> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: [
          Image.asset('assets/sizeguide.jpg',fit: BoxFit.contain,),
          Align(
            alignment: Alignment.topRight,
            child: IconButton(
              icon: Icon(Icons.cancel,color: Colors.black,),
              onPressed: (){
                Navigator.pop(context);
              },
            ),
          ),
        ],
      ),
    );
  }
}

class PaymentGateWAy extends StatefulWidget {
  final int productId;
  final String amount;
  final String contryISOCode;
  PaymentGateWAy({@required this.productId, @required this.amount, @required this.contryISOCode});
  @override
  _PaymentGateWAyState createState() => _PaymentGateWAyState();
}
class _PaymentGateWAyState extends State<PaymentGateWAy> {
  MyProgressDialog pr;
  TextEditingController _cardNumber = TextEditingController();
  TextEditingController _expiryMonth = TextEditingController();
  TextEditingController _expiryYear = TextEditingController();
  TextEditingController _cvvNumber = TextEditingController();
  TextEditingController _accountHolderName = TextEditingController();
  bool isPaymentSuccess = false;
  bool isShipmentSuccess = false;
  @override
  Widget build(BuildContext context) {
    pr = MyProgressDialog(context);
    print("paument Amount = ${widget.amount}");
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.only(left: 30,right: 30, top: 30),
          child: ListView(
            children: [
              Row(
                children: [
                  GestureDetector(
                    onTap: (){
                      Navigator.pop(context);
                    },
                    child: Container(
                        padding:EdgeInsets.all(10),child: Image.asset(
                      'assets/ARROW.png',
                    )
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "Payment".toUpperCase(),
                    style: headingStyle,
                  ),

                ],
              ),
              SizedBox(height: 20,),
              /// First Name
              Text(
                "Card Number".toUpperCase(),
                style: Textprimary,
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                height: 50,
                width: MediaQuery.of(context).size.width/1.07,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    border: Border.all(color: Colors.grey[400])
                ),
                child: TextField(
                    controller: _cardNumber,
                    inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                    style: TextStyle(
                      fontSize: 18,
                    ),
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      contentPadding: EdgeInsets.only(left: 10),
                      fillColor: Color(0xfff3f3f4),
                    )),
              ),
              SizedBox(
                height: 15,
              ),
              /// Expiry Month
              Text(
                "Expiry Month".toUpperCase(),
                style: Textprimary,
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                height: 50,
                width: MediaQuery.of(context).size.width/1.07,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    border: Border.all(color: Colors.grey[400])
                ),
                child: TextField(
                    controller: _expiryMonth,
                    inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                    style: TextStyle(
                      fontSize: 18,
                    ),
                    maxLength: 2,
                    maxLengthEnforcement: MaxLengthEnforcement.enforced,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      contentPadding: EdgeInsets.only(left: 10,right: 5,),
                      fillColor: Color(0xfff3f3f4),
                    )),
              ),
              SizedBox(
                height: 15,
              ),
              /// Email
              Text(
                  "Expiry Year".toUpperCase(),
                  style: Textprimary
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                height: 50,
                width: MediaQuery.of(context).size.width/1.07,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    border: Border.all(color: Colors.grey[400])
                ),
                child: TextField(
                    inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                    controller: _expiryYear,
                    maxLengthEnforced: true,
                    maxLength: 4,
                    style: TextStyle(
                      fontSize: 18,
                    ),
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      contentPadding: EdgeInsets.only(left: 10,right: 5),
                      fillColor: Color(0xfff3f3f4),
                    )),
              ),
              SizedBox(
                height: 15,
              ),
              /// Phone Number
              Text(
                  "CVV".toUpperCase(),
                  style: Textprimary
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                height: 50,
                width: MediaQuery.of(context).size.width/1.07,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    border: Border.all(color: Colors.grey[400])
                ),
                child: TextField(
                    controller: _cvvNumber,
                    maxLengthEnforcement: MaxLengthEnforcement.enforced,
                    maxLength: 3,
                    inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                    style: TextStyle(
                      fontSize: 18,
                    ),
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      contentPadding: EdgeInsets.only(left: 10,right: 5),
                      fillColor: Color(0xfff3f3f4),
                    )),
              ),
              SizedBox(
                height: 15,
              ),
              /// Address 1
              Text(
                  "Account Holder Name".toUpperCase(),
                  style: Textprimary
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                height: 50,
                width: MediaQuery.of(context).size.width/1.07,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    border: Border.all(color: Colors.grey[400])
                ),
                child: TextField(
                    controller: _accountHolderName,
                    style: TextStyle(
                      fontSize: 18,
                    ),
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      contentPadding: EdgeInsets.only(left: 10,right: 5),
                      fillColor: Color(0xfff3f3f4),
                    )),
              ),
              SizedBox(
                height: 15,
              ),
              SizedBox(height: 30,),
              InkWell(
                onTap:()async{
                  if(_accountHolderName.text.trim().isEmpty||_cvvNumber.text.trim().isEmpty||_expiryYear.text.trim().isEmpty||_expiryMonth.text.trim().isEmpty||_cardNumber.text.trim().isEmpty){
                    Fluttertoast.showToast(
                        msg: "Please fill missing fields",
                        toastLength: Toast.LENGTH_SHORT,
                        gravity: ToastGravity.CENTER,
                        timeInSecForIosWeb: 1,
                        backgroundColor: Colors.black38,
                        textColor: Colors.white,
                        fontSize: 16.0
                    );
                    return;
                  }
                  else if(_expiryMonth.text.trim().length!=2){
                    Fluttertoast.showToast(
                        msg: "Expiry month invalid",
                        toastLength: Toast.LENGTH_SHORT,
                        gravity: ToastGravity.CENTER,
                        timeInSecForIosWeb: 1,
                        backgroundColor: Colors.black38,
                        textColor: Colors.white,
                        fontSize: 16.0
                    );
                    return;
                  }else if(_expiryYear.text.trim().length!=4){
                    Fluttertoast.showToast(
                        msg: "Expiry year invalid",
                        toastLength: Toast.LENGTH_SHORT,
                        gravity: ToastGravity.CENTER,
                        timeInSecForIosWeb: 1,
                        backgroundColor: Colors.black38,
                        textColor: Colors.white,
                        fontSize: 16.0
                    );
                    return;
                  }else if(_cvvNumber.text.trim().length!=3){
                    Fluttertoast.showToast(
                        msg: "CVV number invalid",
                        toastLength: Toast.LENGTH_SHORT,
                        gravity: ToastGravity.CENTER,
                        timeInSecForIosWeb: 1,
                        backgroundColor: Colors.black38,
                        textColor: Colors.white,
                        fontSize: 16.0
                    );
                    return;
                  }
                  else {
                    pr.show();
                    print(isPaymentSuccess);
                    print(isShipmentSuccess);
                    if(!isPaymentSuccess&&!isShipmentSuccess){
                      try{
                        await API.doPayment(
                          _cardNumber.text.trim(),
                          _expiryMonth.text.trim(),
                          _expiryYear.text.trim(),
                          _cvvNumber.text.trim(),
                          _accountHolderName.text.trim(),
                          widget.amount,
                          widget.productId,
                        );
                        if(API.complete == 'true' && API.error == "payment success" ){
                          setState(() {
                            isPaymentSuccess = true;
                          });
                          await API.confirmCheckout(
                            widget.productId,
                            widget.contryISOCode,
                          );
                          pr.hide();
                          print("Tracking Number = ${API.TrackingNumber}");
                          if(API.complete == 'true' && API.error == "order success" ){
                            isShipmentSuccess = true;
                            pr.hide();
                            Navigator.push(context, MaterialPageRoute(builder: (_)=>cart3()));
                          }else if(API.complete=="true" && API.error=="invalid"){

                            pr.hide();
                            Fluttertoast.showToast(
                                msg: "Shipment Failed please try again",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIosWeb: 1,
                                backgroundColor: Colors.black38,
                                textColor: Colors.white,
                                fontSize: 16.0
                            );
                          }
                          // pr.hide();
                          // Navigator.push(context, MaterialPageRoute(builder: (_)=>ConfirmCheckout(productId: widget.productId)));
                        }
                        else if(API.complete=="true" && API.error=="invalid"){
                          pr.hide();
                          Fluttertoast.showToast(
                              msg: "Account information invalid",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.CENTER,
                              timeInSecForIosWeb: 1,
                              backgroundColor: Colors.black38,
                              textColor: Colors.white,
                              fontSize: 16.0
                          );
                        }
                      }catch(e){
                        pr.hide();
                        return;
                      }
                    }else if(isPaymentSuccess&&!isShipmentSuccess){
                      try{
                        await API.confirmCheckout(
                          widget.productId,
                          widget.contryISOCode,
                        );
                        pr.hide();
                        print("Tracking Number = ${API.TrackingNumber}");
                        if(API.complete == 'true' && API.error == "order success" ){
                          isShipmentSuccess = true;
                          pr.hide();
                          Navigator.push(context, MaterialPageRoute(builder: (_)=>cart3()));
                        }else if(API.complete=="true" && API.error=="invalid"){
                          pr.hide();
                          Fluttertoast.showToast(
                              msg: "Shipment Failed please try again",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.CENTER,
                              timeInSecForIosWeb: 1,
                              backgroundColor: Colors.black38,
                              textColor: Colors.white,
                              fontSize: 16.0
                          );
                      }
                      }catch(e){
                        pr.hide();
                      }
                    }
                    return;
                  }
                },
                child: Container(
                  width:  MediaQuery.of(context).size.width/1.07,
                  height: 45,
                  margin: EdgeInsets.only(bottom: 5),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: Colors.black,
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  child: Text(
                    'CONFIRM PAYMENT',
                    style:TextStyle(fontSize: 14,fontWeight: FontWeight.bold,color: Colors.white,letterSpacing: 2,fontFamily: "MetropolisExtraBold"),
                  ),
                ),
              ),
              SizedBox(height: 20,),
            ],
          ),
        ),
      ),
    );
  }
}