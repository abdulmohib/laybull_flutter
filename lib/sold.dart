import 'package:flutter/material.dart';
import 'package:Laybull/API/BaseUrl.dart';
import 'package:Laybull/Model/Product.dart';
import 'API/GloabalVariable.dart';
import 'constant.dart';
class SoldProducts extends StatefulWidget {
  final List<Product>listSold;
  final bool isSold;
  SoldProducts({this.listSold,this.isSold});
  @override
  _SoldProductsState createState() => _SoldProductsState();
}

class _SoldProductsState extends State<SoldProducts> {
  @override
  Widget build(BuildContext context) {
    List<Product> listOfSold = widget.listSold;
    return Scaffold(
      body: SafeArea(
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.only(left: 10,right: 10, top: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Row(
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Container(
                        // height: 20,
                        // width: 20,
                        padding: EdgeInsets.all(15),
                        child: Image.asset(
                          'assets/ARROW.png',
                        )),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    widget.isSold==null||widget.isSold==true?"SOLD":'ORDERED',
                    style: headingStyle,
                  ),
                  // Spacer(),
                  // Image.asset('assets/Vector.png'),
                ],
              ),
              Expanded(
                child: ListView.builder(
                  itemCount: listOfSold.length,
                  shrinkWrap: true,
                  physics: BouncingScrollPhysics(),
                  itemBuilder: (context,index){
                    return Container(
                      height: 170,
                      // width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.symmetric(vertical: 3),
                      decoration: BoxDecoration(
                          color: Color(0xffF4F4F4),
                          borderRadius: BorderRadius.circular(10)
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Flexible(
                            child: Container(
                              margin: EdgeInsets.symmetric(vertical: 20),
                              width: 120,
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(10),
                                child:listOfSold.length!=0 &&listOfSold[index].feature_image!=null?
                                  Image.network(BaseUrl+listOfSold[index].feature_image, fit: BoxFit.fill,):Image.asset('assets/bigshoe.png'),
                              ),
                            ),
                          ),
                          SizedBox(width: 15,),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(height: MediaQuery.of(context).size.height*.01,),
                              Row(
                                children: [
                                  Text(
                                    '${listOfSold[index].updated_at.toString().substring(0,10)}',
                                    style: TextStyle(color: Colors.grey),
                                    textAlign: TextAlign.end,
                                  ),
                                ],
                                mainAxisAlignment: MainAxisAlignment.end,
                              ),
                              SizedBox(height: 10,),
                              Container(
                                  child: Text(listOfSold.length!=0?listOfSold[index].name??"Product":'Product', style: TextStyle(fontWeight: FontWeight.w700, fontSize: 16),),
                                width: MediaQuery.of(context).size.width*.6,
                              ),
                              SizedBox(height: 8,),
                              Text(listOfSold.length!=0?listOfSold[index].condition + " | " + listOfSold[index].size:"New",
                                style: TextStyle(color: Colors.grey),),
                              SizedBox(height: 8,),
                              Text(listOfSold.length!=0?"AED "+listOfSold[index].price:"AED 3000", style: TextStyle(fontWeight: FontWeight.w600, fontSize: 14),),
                              SizedBox(height: 10,),
                              Container(
                                height: 30,
                                width: 100,
                                decoration: BoxDecoration(
                                    border: Border.all(color: Colors.green),
                                    borderRadius: BorderRadius.circular(3)
                                ),
                                child:  Center(child: Text("SOLD", style: TextStyle(color: Colors.green , fontSize: 10),)),
                              )
                            ],
                          ),
                        ],
                      ),
                    );
                  },
                ),
              ),
              SizedBox(height: 10,),
            ],
          ),
        ),
      ),

    );
  }
}
