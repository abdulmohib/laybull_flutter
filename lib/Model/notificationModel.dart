import 'package:Laybull/BottomNavigation/BottomNav.dart';
import 'package:Laybull/notificationScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class NotificationType{
  static const String OFFER_ACCEPTED =  "Accept";
  static const String OFFER_REJECTED =  "Reject";
  static const String OFFER_COUNTER =  "CounterOffer";
  static const String OFFER_BID =  "Bid";
  static const String PRODUCT_APPROVED =  "ProductApprove";
}

class LaybullRoute{
  static String initialRoute = "/";
  static String MainScreenRoute = "/mainScreen";
  static String NotificationScreenRoute = "/notificationScreen";


}


final routes = {
  LaybullRoute.MainScreenRoute: (context,{Map<String,int>arguments}) => BottomNav(argument: arguments,),
  LaybullRoute.NotificationScreenRoute: (context,{Map<String,bool>arguments}) => notification(),


};

var onGenerateRoute=(RouteSettings settings){
  //Unified treatment
  final String name = settings.name;
  final Function pageContentBuilder = routes[name];
  if (pageContentBuilder != null) {
    if (settings.arguments != null) {

      final Route route = MaterialPageRoute(
        builder: (context) =>
            pageContentBuilder(context, arguments: settings.arguments),
      );
      return route;
    } else {
      final Route route = MaterialPageRoute(
        builder: (context) => pageContentBuilder(context),
      );
      return route;
    }
  }
};

class NotificationProvider with ChangeNotifier{
  int _totalNotifications = 0;
  void setCount(int count){
    _totalNotifications = count;
    notifyListeners();
  }
  void incCount(){
    _totalNotifications++;
    notifyListeners();
  }
  void reSetCount(){
    _totalNotifications = 0;
    notifyListeners();
  }
  int getCount(){
    return _totalNotifications>0?_totalNotifications:0;
  }
}