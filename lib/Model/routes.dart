import 'package:Laybull/Model/notificationModel.dart';
import 'package:flutter/material.dart';

class AppRoutes {
  static Future<dynamic> push(BuildContext context, Widget page) {
    return Navigator.of(context).push(
      MaterialPageRoute(builder: (context) => page),
    );
  }

  static void replace(BuildContext context, Widget page) {
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(builder: (context) => page),
    );
  }
  static void makeFirst(BuildContext context, Widget page,int MainScreenNavIndex) {
    Navigator.of(context).popUntil((predicate){
      print("Predicate = ${predicate.settings.name}\t\t${predicate.isFirst}");
      return predicate.settings.name=="/";
    });
    Navigator.pushNamed(context, LaybullRoute.MainScreenRoute,arguments: {"nav_index":MainScreenNavIndex});
    // Navigator.of(context).pushReplacement(
    //   MaterialPageRoute(builder: (context) => page),
    // );
  }

  static void pop(BuildContext context) {
    Navigator.of(context).pop();
  }

  static void dismissAlert(context) {
    Navigator.of(context).pop();
  }
}
