import 'dart:convert';

import 'package:Laybull/API/GloabalVariable.dart';

class Product
{
  int category_id;
  int id;
  String feature_image;
  int vendor_id;
  List<dynamic>images;
  String type;
  int brand_id;
  String status;
  String popular;
  String name;
  String price;
  String color;
  String size;
  String condition;
  String discount;
  String original_price;
  String tags;
  String pusblish;
  String payment;
  String warrenty;
  String short_desc;
  String fulldesc;
  bool isFavourite;
  String bid_price;
  String updated_at;
  String vendorName;

  Product(
      {
       this.category_id,
      this.id,
      this.brand_id,
      this.vendor_id,
      this.type,
      this.status,
      this.popular,
      this.name,
      this.price,
      this.feature_image,
      this.discount,
      this.original_price,
      this.images,
      this.tags,
      this.pusblish,
      this.short_desc,
      this.fulldesc,
      this.payment,
      this.warrenty,
      this.color,
      this.isFavourite = false,
          this.size,
        this.condition,
        this.bid_price,
        this.updated_at,
        this.vendorName,
  });

  factory Product.fromJson(Map<String, dynamic> jsonData) {
    return Product(
          id: jsonData['id'],
          category_id: jsonData['category_id'],
          brand_id: jsonData['brand_id'],
          vendor_id: jsonData['vendor_id'],
          type: jsonData["type"],
          status: jsonData['status'],
          popular: jsonData['popular'],
          name: jsonData['name'],
          price: jsonData['price'],
          feature_image: jsonData["vendorName"]!=null?jsonData['feature_image']:"uploads/productImages/${jsonData['feature_image']}",
          images: jsonData['images']!=null?jsonData['images']:[""],
          color: jsonData['color'],
          size: jsonData["size"],
          condition: jsonData["condition"],
          discount: jsonData['discount'],
          original_price: jsonData['original_price'],
          tags: jsonData['tags'],
          pusblish: jsonData['pusblish'],
          short_desc: jsonData['short_desc']??"",
          fulldesc: jsonData['fulldesc']??"",
          payment: jsonData['payment'],
          warrenty: jsonData['warrenty'],
          isFavourite: jsonData["likes"]==null?false:jsonData["likes"]["is_like"]==1?true:false,
        bid_price: jsonData["bid_price"]??"",
        updated_at: jsonData["updated_at"]??"",
        vendorName: jsonData["vendorName"]!=null?jsonData["vendorName"]:jsonData["user"]!=null?jsonData["user"]["name"]:"",
      );
  }

  static Map<String, dynamic> toMap(Product cart) => {
      'category_id': cart.category_id,
      'id': cart.id,
      'brand_id': cart.brand_id,
      'vendor_id': cart.vendor_id,
      'type': cart.type,
      'status': cart.status,
      'popular': cart.popular,
      'name': cart.name,
      'price': cart.price,
      'feature_image': cart.feature_image,
      'discount': cart.discount,
      "images": cart.images,
      'original_price': cart.original_price,
      'tags': cart.tags,
      'pusblish': cart.pusblish,
      'short_desc': cart.short_desc,
      'fulldesc': cart.fulldesc,
      'payment': cart.payment,
      'warrenty': cart.warrenty,
      'color': cart.color,
      'isFavourite': cart.isFavourite,
      "updated_at": cart.updated_at??"",
      "vendorName": cart.vendorName??"",
      "condition": cart.condition,
      "size":cart.size,
  };
  static String encodeMusics(List<Product> cart) {
    return json.encode(
      cart
          .map<Map<dynamic, dynamic>>((music) => Product.toMap(music))
          .toList(),
    );
  }

  static List<Product> decodeMusics(String musics){
      if(musics != null){
          return (json.decode(musics) as List<dynamic>)
              .map<Product>((item) => Product.fromJson(item))
              .toList();
      }else{
          return new List();
      }

  }

}