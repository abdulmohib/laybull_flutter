import 'package:Laybull/Model/Product.dart';

class ProductBid{
  int bidId;
  int productId;
  int vendorId;
  String bidPrice;
  String status;
  String counter;
  Product product;

  ProductBid({this.bidPrice,this.productId,this.vendorId,this.status,this.counter,this.bidId,this.product});

  factory ProductBid.fromJson(Map<String,dynamic>json){
    return ProductBid(
        bidId: json["id"],
        productId: json["product_id"],
        vendorId: json["vendor_id"],
        bidPrice: json["bid_price"],
      status: json["status"],
      counter: json["counter"],
      product:json["product"]==null?null:Product.fromJson(json["product"])
    );
  }
}