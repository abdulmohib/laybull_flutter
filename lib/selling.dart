import 'package:flutter/material.dart';
import 'package:Laybull/API/BaseUrl.dart';
import 'package:Laybull/Model/Product.dart';

import 'API/GloabalVariable.dart';
import 'Addtochart.dart';
import 'constant.dart';
import 'lists.dart';

class Selling extends StatefulWidget {
  final List<Product>sellingProducts;
  Selling({this.sellingProducts});
  @override
  _SellingState createState() => _SellingState();
}

class _SellingState extends State<Selling> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(18.0),
            child: Container(
              // height: MediaQuery.of(context).size.height*1.1,
              // width: MediaQuery.of(context).size.width,
              child: ListView(
                shrinkWrap: true,
                // crossAxisAlignment: CrossAxisAlignment.start,
                // mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Container(
                            padding: EdgeInsets.all(15),
                            child: Image.asset(
                              'assets/ARROW.png',
                            )),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        'SELLING',
                        style: headingStyle,
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top:12.0),
                    child: productGridView(product: widget.sellingProducts,onFavoutireButtonTapped: (){setState(() {});}),
                  )
                ],
              ),
            ),
          ),
        ),

    );
  }
}

