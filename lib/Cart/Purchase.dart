import 'package:Laybull/BottomNavigation/BottomNav.dart';
import 'package:flutter/material.dart';

class cart3 extends StatefulWidget {

  @override
  _cart3State createState() => _cart3State();
}

class _cart3State extends State<cart3> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //resizeToAvoidBottomPadding: false,
      resizeToAvoidBottomInset: false,
      body: WillPopScope(
        onWillPop: (){
          Navigator.of(context).popUntil((predicate) => predicate.isFirst);
          return Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => BottomNav()),
          );
        },
        child: Column(
          children: [
            SizedBox(height: MediaQuery.of(context).size.height*.2,),
            // Row(
            //   children: [
            //     Container(
            //       padding:EdgeInsets.all(10),
            //       child: GestureDetector(
            //         onTap: (){
            //
            //           Navigator.of(context).popUntil((predicate) => predicate.isFirst);
            //           Navigator.of(context).pushReplacement(
            //             MaterialPageRoute(builder: (context) => BottomNav()),
            //           );
            //         },
            //         child: Image.asset(
            //         'assets/ARROW.png',
            //       ),
            //       ),
            //     ),
            //   ],
            // ),
            // SizedBox(height: MediaQuery.of(context).size.height*.1,),
            Column(
              children: [
                SizedBox(height: 20,),
                Center(child: Icon(Icons.check_circle, size: 42, color: Colors.green,)),
                SizedBox(height: 20,),
                Center(
                  child: Text(
                    "THANK YOU FOR YOUR  ",
                    style: TextStyle(fontSize: 17,fontWeight: FontWeight.w500, letterSpacing: 0.11),
                  ),
                ),
                Center(
                  child: Text(
                    "PURCHASE ",
                    style: TextStyle(fontSize: 37,fontWeight: FontWeight.w800, letterSpacing: 0.11),
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
              ],
            ),
          ],
        ),
      ),
      bottomNavigationBar: Container(
        margin: EdgeInsets.only(bottom: 20,left: 20,right: 20),
        child: GestureDetector(
          onTap:(){
            Navigator.of(context).popUntil((predicate) => predicate.isFirst);
                      Navigator.of(context).pushReplacement(
                        MaterialPageRoute(builder: (context) => BottomNav()),
                      );
          },
          child: Container(
            width:  MediaQuery.of(context).size.width/1.07,
            height: 45,
            margin: EdgeInsets.only(bottom: 5),
            alignment: Alignment.center,
            decoration: BoxDecoration(
              color: Colors.black,
              borderRadius: BorderRadius.circular(5.0),
            ),
            child: Text(
              'CONTINUE SHOPPING',
              style:TextStyle(fontSize: 14,fontWeight: FontWeight.bold,color: Colors.white,letterSpacing: 2,fontFamily: "MetropolisExtraBold"),
            ),
          ),
        ),
      ),
    );
  }
}
